/**
 * Project Carina - EVE Fitting Engine Android Library
 *
 * Copyright (c) 2015 Aureolin
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package co.aureolin.labs.carina.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.Map;

import co.aureolin.labs.carina.model.sde.SdeAttribute;
import co.aureolin.labs.carina.util.Helper;

/**
 * Created by Akinwale on 20/09/2015.
 */
public class Implant implements IModel, Comparable<Implant> {
    private static final String SLOT_ATTRIB_NAME = "implantness";

    private long id;

    public String name;

    private long jumpCloneId; // For jump clone implants

    private Map<String, SdeAttribute> attributes;

    private boolean placeholder; // Placeholder flag for "No implants installed" message

    public Implant() {
        this.placeholder = false;
    }

    public Implant(boolean placeholder) {
        this.placeholder = placeholder;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getJumpCloneId() {
        return jumpCloneId;
    }

    public void setJumpCloneId(long jumpCloneId) {
        this.jumpCloneId = jumpCloneId;
    }

    @JsonIgnore
    public boolean isPlaceholder() {
        return placeholder;
    }

    @JsonIgnore
    public void setPlaceholder(boolean placeholder) {
        this.placeholder = placeholder;
    }

    @JsonIgnore
    public String getSlot() {
        if (attributes == null || !attributes.containsKey(SLOT_ATTRIB_NAME)) {
            return "";
        }
        return String.valueOf(Double.valueOf(attributes.get(SLOT_ATTRIB_NAME).getValue()).intValue());
    }

    @JsonIgnore
    public Map<String, SdeAttribute> getAttributes() {
        return attributes;
    }

    @JsonIgnore
    public void setAttributes(Map<String, SdeAttribute> attributes) {
        this.attributes = attributes;
    }

    @JsonIgnore
    public SdeAttribute getAttribute(String name) {
        return (attributes.containsKey(name) ? attributes.get(name) : null);
    }

    public int compareTo(Implant implant) {
        int thisSlot = Helper.parseInt(getSlot(), 0);
        int implantSlot = Helper.parseInt(implant.getSlot(), 0);
        if (thisSlot > implantSlot) {
            return 1;
        }
        if (thisSlot < implantSlot) {
            return -1;
        }

        return 0;
    }
}
