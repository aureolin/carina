/**
 * Project Carina - EVE Fitting Engine Android Library
 *
 * Copyright (c) 2015 Aureolin
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package co.aureolin.labs.carina.model.fitting;

import android.util.Log;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import co.aureolin.labs.carina.fitting.Dogma;
import co.aureolin.labs.carina.model.sde.SdeAttribute;
import co.aureolin.labs.carina.util.FittingHelper;

/**
 * Created by Akinwale on 11/10/2015.
 */
public class Ship extends FittingModel {

    private FittingCharacter currentCharacter;

    /**
     * Map to store penalised attribute IDs related to the number of fitted modules
     * that affect the specific attribute.
     */
    private Map<Long, Integer> currentFittedGroupCountMap;

    private Map<Long, Integer> penalisedAttributeCountMap;

    private List<Module> highSlotModules;

    private List<Module> mediumSlotModules;

    private List<Module> lowSlotModules;

    private List<Module> rigSlotModules;

    private List<Module> subsystemModules;

    public Ship() {
        highSlotModules = new ArrayList<Module>();
        mediumSlotModules = new ArrayList<Module>();
        lowSlotModules = new ArrayList<Module>();
        rigSlotModules = new ArrayList<Module>();
        subsystemModules = new ArrayList<Module>();
        currentFittedGroupCountMap = new HashMap<Long, Integer>();
        penalisedAttributeCountMap = new HashMap<Long, Integer>();
    }

    public int getPenalisedAttributeCount(long attributeId) {
        int count = (!penalisedAttributeCountMap.containsKey(attributeId)) ? 0
                : penalisedAttributeCountMap.get(attributeId);
        return (count < 0) ? 0 : count;
    }

    public void decrementPenalisedAttributeCount(long attributeId) {
        int count = 0;
        if (!penalisedAttributeCountMap.containsKey(attributeId)) {
            count = 0;
        } else {
            count = penalisedAttributeCountMap.get(attributeId) - 1;
            if (count < 0) {
                count = 0;
            }
        }
        penalisedAttributeCountMap.put(attributeId, count);
    }

    public boolean moduleHasReachedMaxFitting(Module module) {
        if (!module.hasMaxFitting()) {
            return false;
        }
        long groupId = module.getGroupId();
        if (Module.MaxFittedGroups.containsKey(groupId)
            && currentFittedGroupCountMap.containsKey(groupId)) {
            int max = Module.MaxFittedGroups.get(groupId);
            int current = currentFittedGroupCountMap.get(groupId);
            if (current >= max) {
                return true;
            }
        }

        return false;
    }

    public void incrementPenalisedAttributeCount(long attributeId) {
        int count = 0;
        if (!penalisedAttributeCountMap.containsKey(attributeId)) {
            count = 1;
        } else {
            count = penalisedAttributeCountMap.get(attributeId) + 1;
        }
        penalisedAttributeCountMap.put(attributeId, count);
    }

    public void decrementModuleMaxFittingCount(Module module) {
        if (!module.hasMaxFitting()) {
            return;
        }
        long groupId = module.getGroupId();
        if (!currentFittedGroupCountMap.containsKey(groupId)) {
            currentFittedGroupCountMap.put(groupId, 0);
        } else {
            currentFittedGroupCountMap.put(groupId, currentFittedGroupCountMap.get(groupId) - 1);
        }
    }

    public void incrementModuleMaxFittingCount(Module module) {
        if (!module.hasMaxFitting()) {
            return;
        }

        long groupId = module.getGroupId();
        if (!currentFittedGroupCountMap.containsKey(groupId)) {
            currentFittedGroupCountMap.put(groupId, 0);
        }
        currentFittedGroupCountMap.put(groupId, currentFittedGroupCountMap.get(groupId) + 1);
    }

    public void addHighSlotModule(Module module) {
        highSlotModules.add(module);
        incrementModuleMaxFittingCount(module);
    }

    public void addMediumSlotModule(Module module) {
        mediumSlotModules.add(module);
        incrementModuleMaxFittingCount(module);
    }

    public void addLowSlotModule(Module module) {
        lowSlotModules.add(module);
        incrementModuleMaxFittingCount(module);
    }

    public void addRigSlotModule(Module module) {
        rigSlotModules.add(module);
        updateCalibration();
        incrementModuleMaxFittingCount(module);

    }

    private void updateCalibration() {
        int totalRigUpgradeCost = 0;
        for (int i = 0; i < rigSlotModules.size(); i++) {
            Module module = rigSlotModules.get(i);
            totalRigUpgradeCost +=
                FittingHelper.toIntValue(module.getAttributeValueByName("upgradeCost"));
        }
        setAffectedValue("upgradeCapacity", getBaseCalibration() - totalRigUpgradeCost);
    }

    public void addLauncherSlotModule(Module module) {
        // Create separate list for launcher slots or no?
        highSlotModules.add(module);
        incrementModuleMaxFittingCount(module);
    }

    public boolean canFitSubsystem(Module module) {
        boolean canFit = true;
        for (int i = 0; i < subsystemModules.size(); i++) {
            if (subsystemModules.get(i).getAttributeValueByName("subSystemSlot")
                == module.getAttributeValueByName("subSystemSlot")) {
                // A subsystem that fits in the same slot already exists;
                canFit = false;
                break;
            }
        }

        return canFit;
    }

    public void addSubsystemSlotModule(Module module) {
        subsystemModules.add(module);

        // Recalculate based on subsystems
        updateShipSlotsBasedOnSubsystems();
    }

    public void updateShipSlotsBasedOnSubsystems() {
        int numHighSlots = 0;
        int numLauncherSlots = 0;
        int numTurretSlots = 0;
        int numMedSlots = 0;
        int numLowSlots = 0;

        for (int i = 0; i < subsystemModules.size(); i++) {
            Module subsystem = subsystemModules.get(i);
            if (subsystem.hasAttribute("hiSlotModifier")) {
                numHighSlots += FittingHelper.toIntValue(subsystem.getAttributeValueByName("hiSlotModifier"));
            }
            if (subsystem.hasAttribute("turretHardPointModifier")) {
                numTurretSlots += FittingHelper.toIntValue(
                    subsystem.getAttributeValueByName("turretHardPointModifier"));
            }
            if (subsystem.hasAttribute("launcherHardPointModifier")) {
                numLauncherSlots += FittingHelper.toIntValue(
                    subsystem.getAttributeValueByName("launcherHardPointModifier"));
            }
            if (subsystem.hasAttribute("medSlotModifier")) {
                numMedSlots += FittingHelper.toIntValue(subsystem.getAttributeValueByName("medSlotModifier"));
            }
            if (subsystem.hasAttribute("lowSlotModifier")) {
                numLowSlots += FittingHelper.toIntValue(subsystem.getAttributeValueByName("lowSlotModifier"));
            }
        }

        insertValue("hiSlots", numHighSlots);
        insertValue("launcherSlotsLeft", numLauncherSlots);
        insertValue("turretSlotsLeft", numTurretSlots);
        insertValue("medSlots", numMedSlots);
        insertValue("lowSlots", numLowSlots);
    }

    public void addTurretSlotModule(Module module) {
        highSlotModules.add(module);
        incrementModuleMaxFittingCount(module);
    }

    public int getModuleIndexToRemove(Module module, List<Module> moduleList) {
        int idxToRemove = -1;
        for (int i = 0; i < moduleList.size(); i++) {
            if (moduleList.get(i).getUniqueId() == module.getUniqueId()) {
                idxToRemove = i;
                break;
            }
        }

        return idxToRemove;
    }

    public boolean removeModule(Module module) {
        if (module == null) {
            return false;
        }

        int idxToRemove = -1;
        boolean removed = false;
        int slotType = module.getSlotType();
        switch (slotType) {
            case Dogma.ModuleSlot.High:
            case Dogma.ModuleSlot.Turret:
            case Dogma.ModuleSlot.Launcher:
                idxToRemove = getModuleIndexToRemove(module, highSlotModules);
                if (idxToRemove > -1) {
                    removed = (highSlotModules.remove(idxToRemove) != null);
                }
                break;

            case Dogma.ModuleSlot.Medium:
                idxToRemove = getModuleIndexToRemove(module, mediumSlotModules);
                if (idxToRemove > -1) {
                    removed = (mediumSlotModules.remove(idxToRemove) != null);
                }
                break;

            case Dogma.ModuleSlot.Low:
                idxToRemove = getModuleIndexToRemove(module, lowSlotModules);
                if (idxToRemove > -1) {
                    removed = (lowSlotModules.remove(idxToRemove) != null);
                }
                break;

            case Dogma.ModuleSlot.Rig:
                idxToRemove = getModuleIndexToRemove(module, rigSlotModules);
                if (idxToRemove > -1) {
                    removed = (rigSlotModules.remove(idxToRemove) != null);
                }
                break;

            case Dogma.ModuleSlot.Subsystem:
                idxToRemove = getModuleIndexToRemove(module, subsystemModules);
                if (idxToRemove > -1) {
                    removed = (subsystemModules.remove(idxToRemove) != null);
                }
                break;
        }

        if (removed) {
            decrementModuleMaxFittingCount(module);
            if (slotType == Dogma.ModuleSlot.Rig) {
                updateCalibration();
            }
            if (slotType == Dogma.ModuleSlot.Subsystem) {
                updateShipSlotsBasedOnSubsystems();
            }
        }

        return removed;
    }

    @JsonIgnore
    public int getRigSize() {
        return FittingHelper.toIntValue(getDisplayAttributeValueByName("rigSize"));
    }

    /************************
     *   BASE SHIP VALUES   *
     ************************/
    /*
     * CPU
     */
    @JsonIgnore
    public double getBaseCpuOutput() {
        return getDisplayAttributeValueByName("cpuOutput");
    }

    /*
     * Powergrid
     */
    @JsonIgnore
    public double getBasePowergridOutput() {
        return getDisplayAttributeValueByName("powerOutput");
    }

    /*
     * High slots / Launchers / Turrets
     */
    @JsonIgnore
    public int getBaseHighSlots() {
        return FittingHelper.toIntValue(getDisplayAttributeValueByName("hiSlots"));
    }

    @JsonIgnore
    public int getBaseLauncherHardpoints() {
        return FittingHelper.toIntValue(getDisplayAttributeValueByName("launcherSlotsLeft"));
    }

    @JsonIgnore
    public int getBaseTurretHardpoints() {
        return FittingHelper.toIntValue(getDisplayAttributeValueByName("turretSlotsLeft"));
    }

    @JsonIgnore
    public int getBaseMediumSlots() {
        return FittingHelper.toIntValue(getDisplayAttributeValueByName("medSlots"));
    }

    @JsonIgnore
    public int getBaseLowSlots() {
        return FittingHelper.toIntValue(getDisplayAttributeValueByName("lowSlots"));
    }

    /*
     * Rigs
     */
    @JsonIgnore
    public int getBaseRigSlots() {
        return FittingHelper.toIntValue(getDisplayAttributeValueByName("rigSlots"));
    }

    @JsonIgnore
    public int getBaseCalibration() {
        return FittingHelper.toIntValue(getDisplayAttributeValueByName("upgradeCapacity"));
    }

    /*
     * Drones
     */
    @JsonIgnore
    public int getBaseDroneBayCapacity() {
        return FittingHelper.toIntValue(getDisplayAttributeValueByName("droneCapacity"));
    }

    @JsonIgnore
    public double getBaseDroneBandwidth() {
        return FittingHelper.toIntValue(getDisplayAttributeValueByName("droneBandwidth"));
    }

    /**
     * Shield hitpoints + resistance
     */
    @JsonIgnore
    public int getBaseShieldHitpoints() {
        return FittingHelper.toIntValue(getDisplayAttributeValueByName("shieldCapacity"));
    }

    @JsonIgnore
    public int getBaseShieldRechargeTime() {
        return FittingHelper.toIntValue(getDisplayAttributeValueByName("rechargeRate"));
    }

    @JsonIgnore
    public int getBaseShieldEMDamageResistance() {
        return FittingHelper.toIntValue(getDisplayAttributeValueByName("shieldEmDamageResonance"));
    }

    @JsonIgnore
    public int getBaseShieldThermalDamageResistance() {
        return FittingHelper.toIntValue(getDisplayAttributeValueByName("shieldThermalDamageResonance"));
    }

    @JsonIgnore
    public int getBaseShieldKineticDamageResistance() {
        return FittingHelper.toIntValue(getDisplayAttributeValueByName("shieldKineticDamageResonance"));
    }

    @JsonIgnore
    public int getBaseShieldExplosiveDamageResistance() {
        return FittingHelper.toIntValue(getDisplayAttributeValueByName("shieldExplosiveDamageResonance"));
    }

    @JsonIgnore
    public int getBaseArmorHitpoints() {
        return FittingHelper.toIntValue(getDisplayAttributeValueByName("armorHP"));
    }

    @JsonIgnore
    public int getBaseArmorEMDamageResistance() {
        return FittingHelper.toIntValue(getDisplayAttributeValueByName("armorEmDamageResonance"));
    }

    @JsonIgnore
    public int getBaseArmorThermalDamageResistance() {
        return FittingHelper.toIntValue(getDisplayAttributeValueByName("armorThermalDamageResonance"));
    }

    @JsonIgnore
    public int getBaseArmorKineticDamageResistance() {
        return FittingHelper.toIntValue(getDisplayAttributeValueByName("armorKineticDamageResonance"));
    }

    @JsonIgnore
    public int getBaseArmorExplosiveDamageResistance() {
        return FittingHelper.toIntValue(getDisplayAttributeValueByName("armorExplosiveDamageResonance"));
    }

    @JsonIgnore
    public int getBaseStructureHitpoints() {
        return FittingHelper.toIntValue(getDisplayAttributeValueByName("hp"));
    }

    @JsonIgnore
    public int getBaseStructureEMDamageResistance() {
        return FittingHelper.toIntValue(getDisplayAttributeValueByName("emDamageResonance"));
    }

    @JsonIgnore
    public int getBaseStructureThermalDamageResistance() {
        return FittingHelper.toIntValue(getDisplayAttributeValueByName("thermalDamageResonance"));
    }

    @JsonIgnore
    public int getBaseStructureKineticDamageResistance() {
        return FittingHelper.toIntValue(getDisplayAttributeValueByName("kineticDamageResonance"));
    }

    @JsonIgnore
    public int getBaseStructureExplosiveDamageResistance() {
        return FittingHelper.toIntValue(getDisplayAttributeValueByName("explosiveDamageResonance"));
    }

    @JsonIgnore
    public int getBaseTargetingRange() {
        return FittingHelper.toIntValue(getDisplayAttributeValueByName("maxTargetRange"));
    }

    @JsonIgnore
    public int getBaseRadarSensorStrength() {
        return FittingHelper.toIntValue(getDisplayAttributeValueByName("scanRadarStrength"));
    }

    @JsonIgnore
    public int getBaseLadarSensorStrength() {
        return FittingHelper.toIntValue(getDisplayAttributeValueByName("scanLadarStrength"));
    }

    @JsonIgnore
    public int getBaseMagnetometricSensorStrength() {
        return FittingHelper.toIntValue(getDisplayAttributeValueByName("scanMagnetometricStrength"));
    }

    @JsonIgnore
    public int getBaseGravimetricSensorStrength() {
        return FittingHelper.toIntValue(getDisplayAttributeValueByName("scanGravimetricStrength"));
    }

    @JsonIgnore
    public int getBaseScanResolution() {
        return FittingHelper.toIntValue(getDisplayAttributeValueByName("scanResolution"));
    }

    @JsonIgnore
    public int getBaseSignatureRadius() {
        return FittingHelper.toIntValue(getDisplayAttributeValueByName("signatureRadius"));
    }

    @JsonIgnore
    public int getBaseMaximumLockedTargets() {
        return FittingHelper.toIntValue(getDisplayAttributeValueByName("maxLockedTargets"));
    }

    @JsonIgnore
    public int getBaseMaximumVelocity() {
        return FittingHelper.toIntValue(getDisplayAttributeValueByName("maxVelocity"));
    }

    @JsonIgnore
    public double getBaseMass() {
        // Where to get this? DB table
        return 0;
    }

    @JsonIgnore
    public double getBaseInertiaModifier() {
        return getDisplayAttributeValueByName("agility");
    }

    @JsonIgnore
    public double getBaseWarpSpeed() {
        return getDisplayAttributeValueByName("warpSpeedMultiplier");
    }


    /***************************
     *     AFFECTED VALUES     *
     ***************************/
    /*
     * CPU
     */
    @JsonIgnore
    public double getCpuOutput() {
        return getDisplayAffectedValueByName("cpuOutput");
    }

    @JsonIgnore
    public double getCpuLoad() {
        return getDisplayAffectedValueByName("cpuLoad");
    }

    /*
     * Powergrid
     */
    @JsonIgnore
    public double getPowergridOutput() {
        return getDisplayAffectedValueByName("powerOutput");
    }

    @JsonIgnore
    public double getPowerLoad() {
        return getDisplayAffectedValueByName("powerLoad");
    }

    /*
     * High slots / Launchers / Turrets
     */
    @JsonIgnore
    public int getFittedHighSlotCount() {
        return highSlotModules.size();
    }

    @JsonIgnore
    public int getRemainingHighSlots() {
        return FittingHelper.toIntValue(getDisplayAffectedValueByName("hiSlots"))
                - highSlotModules.size();
    }

    @JsonIgnore
    public int getLauncherHardpoints() {
        return getBaseLauncherHardpoints();
    }

    @JsonIgnore
    public int getRemainingLauncherHardpoints() {
        return getLauncherHardpoints() - getFittedLauncherCount();
    }

    @JsonIgnore
    public int getFittedLauncherCount() {
        int count = 0;
        for (int i = 0; i < highSlotModules.size(); i++) {
            if (highSlotModules.get(i).canFitLauncherSlot()) {
                count++;
            }
        }
        return count;
    }

    @JsonIgnore
    public int getTurretHardpoints() {
        return getBaseTurretHardpoints();
    }

    @JsonIgnore
    public int getRemainingTurretHardpoints() {
        return getTurretHardpoints() - getFittedTurretCount();
    }

    @JsonIgnore
    public int getFittedTurretCount() {
        int count = 0;
        for (int i = 0; i < highSlotModules.size(); i++) {
            if (highSlotModules.get(i).canFitTurretSlot()) {
                count++;
            }
        }
        return count;
    }

    @JsonIgnore
    public int getMediumSlots() {
        return FittingHelper.toIntValue(getDisplayAffectedValueByName("medSlots"));
    }

    @JsonIgnore
    public int getLowSlots() {
        return FittingHelper.toIntValue(getDisplayAffectedValueByName("lowSlots"));
    }

    /*
     * Rigs
     */
    @JsonIgnore
    public int getRigSlots() {
        return FittingHelper.toIntValue(getDisplayAffectedValueByName("rigSlots"));
    }

    @JsonIgnore
    public int getCalibration() {
        return FittingHelper.toIntValue(getDisplayAffectedValueByName("upgradeCapacity"));
    }

    /*
     * Drones
     */
    @JsonIgnore
    public int getDroneBayCapacity() {
        return FittingHelper.toIntValue(getDisplayAffectedValueByName("droneCapacity"));
    }

    @JsonIgnore
    public double getDroneBandwidth() {
        return FittingHelper.toIntValue(getDisplayAffectedValueByName("droneBandwidth"));
    }

    /**
     * Shield hitpoints + resistance
     */
    @JsonIgnore
    public int getShieldHitpoints() {
        return FittingHelper.toIntValue(getDisplayAffectedValueByName("shieldCapacity"));
    }

    @JsonIgnore
    public int getShieldRechargeTime() {
        return FittingHelper.toIntValue(getDisplayAffectedValueByName("rechargeRate"));
    }

    @JsonIgnore
    public double getShieldEMDamageResistance() {
        return getDisplayAffectedValueByName("shieldEmDamageResonance");
    }

    @JsonIgnore
    public double getShieldThermalDamageResistance() {
        return getDisplayAffectedValueByName("shieldThermalDamageResonance");
    }

    @JsonIgnore
    public double getShieldKineticDamageResistance() {
        return getDisplayAffectedValueByName("shieldKineticDamageResonance");
    }

    @JsonIgnore
    public double getShieldExplosiveDamageResistance() {
        return getDisplayAffectedValueByName("shieldExplosiveDamageResonance");
    }

    @JsonIgnore
    public int getArmorHitpoints() {
        return FittingHelper.toIntValue(getDisplayAffectedValueByName("armorHP"));
    }

    @JsonIgnore
    public double getArmorEMDamageResistance() {
        return getDisplayAffectedValueByName("armorEmDamageResonance");
    }

    @JsonIgnore
    public double getArmorThermalDamageResistance() {
        return getDisplayAffectedValueByName("armorThermalDamageResonance");
    }

    @JsonIgnore
    public double getArmorKineticDamageResistance() {
        return getDisplayAffectedValueByName("armorKineticDamageResonance");
    }

    @JsonIgnore
    public double getArmorExplosiveDamageResistance() {
        return getDisplayAffectedValueByName("armorExplosiveDamageResonance");
    }

    @JsonIgnore
    public int getStructureHitpoints() {
        return FittingHelper.toIntValue(getDisplayAffectedValueByName("hp"));
    }

    @JsonIgnore
    public double getStructureEMDamageResistance() {
        return getDisplayAffectedValueByName("emDamageResonance");
    }

    @JsonIgnore
    public double getStructureThermalDamageResistance() {
        return getDisplayAffectedValueByName("thermalDamageResonance");
    }

    @JsonIgnore
    public double getStructureKineticDamageResistance() {
        return getDisplayAffectedValueByName("kineticDamageResonance");
    }

    @JsonIgnore
    public double getStructureExplosiveDamageResistance() {
        return getDisplayAffectedValueByName("explosiveDamageResonance");
    }

    @JsonIgnore
    public int getTargetingRange() {
        return FittingHelper.toIntValue(getDisplayAffectedValueByName("maxTargetRange"));
    }

    @JsonIgnore
    public int getRadarSensorStrength() {
        return FittingHelper.toIntValue(getDisplayAffectedValueByName("scanRadarStrength"));
    }

    @JsonIgnore
    public int getLadarSensorStrength() {
        return FittingHelper.toIntValue(getDisplayAffectedValueByName("scanLadarStrength"));
    }

    @JsonIgnore
    public int getMagnetometricSensorStrength() {
        return FittingHelper.toIntValue(getDisplayAffectedValueByName("scanMagnetometricStrength"));
    }

    @JsonIgnore
    public int getGravimetricSensorStrength() {
        return FittingHelper.toIntValue(getDisplayAffectedValueByName("scanGravimetricStrength"));
    }

    @JsonIgnore
    public int getScanResolution() {
        return FittingHelper.toIntValue(getDisplayAffectedValueByName("scanResolution"));
    }

    @JsonIgnore
    public int getSignatureRadius() {
        return FittingHelper.toIntValue(getDisplayAffectedValueByName("signatureRadius"));
    }

    @JsonIgnore
    public int getMaximumLockedTargets() {
        return FittingHelper.toIntValue(getDisplayAffectedValueByName("maxLockedTargets"));
    }

    @JsonIgnore
    public int getMaximumVelocity() {
        return FittingHelper.toIntValue(getDisplayAffectedValueByName("maxVelocity"));
    }

    @JsonIgnore
    public double getMass() {
        // Where to get this? DB table
        return 0;
    }

    @JsonIgnore
    public double getInertiaModifier() {
        return getDisplayAffectedValueByName("agility");
    }

    @JsonIgnore
    public double getWarpSpeed() {
        return getDisplayAffectedValueByName("warpSpeedMultiplier");
    }

    /**
     * Fitting Stats
     */
    @JsonIgnore
    public int getHighSlotCount() {
        return FittingHelper.toIntValue(getAttributeValueByName("hiSlots"));
    }

    @JsonIgnore
    public int getMediumSlotCount() {
        return FittingHelper.toIntValue(getAttributeValueByName("medSlots"));
    }

    @JsonIgnore
    public int getRemainingMediumSlots() {
        return getMediumSlotCount() - mediumSlotModules.size();
    }

    @JsonIgnore
    public int getLowSlotCount() {
        return FittingHelper.toIntValue(getAttributeValueByName("lowSlots"));
    }

    @JsonIgnore
    public int getRemainingLowSlots() {
        return getMediumSlotCount() - lowSlotModules.size();
    }

    @JsonIgnore
    public int getRigSlotCount() {
        return FittingHelper.toIntValue(getAttributeValueByName("rigSlots"));
    }

    @JsonIgnore
    public int getRemainingRigSlots() {
        return getMediumSlotCount() - rigSlotModules.size();
    }

    @JsonIgnore
    public int getSubsystemSlotCount() {
        return FittingHelper.toIntValue(getAttributeValueByName("maxSubSystems"));
    }

    @JsonIgnore
    public int getRemainingSubsystemSlots() {
        return getSubsystemSlotCount() - subsystemModules.size();
    }

    @JsonIgnore
    public int getDps() {
        return 0;
    }

    @JsonIgnore
    public int getCpuLoadPercent() {
        int percent = FittingHelper.toIntValue((getCpuLoad() / getCpuOutput()) * 100.0);
        if (percent > 100) {
            percent = 100;
        }
        return percent;
    }

    @JsonIgnore
    public int getPowergridLoadPercent() {
        int percent = FittingHelper.toIntValue((getPowerLoad() / getPowergridOutput()) * 100.0);
        if (percent > 100) {
            percent = 100;
        }
        return percent;
    }
}
