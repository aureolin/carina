/**
 * Project Carina - EVE Fitting Engine Android Library
 *
 * Copyright (c) 2015 Aureolin
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package co.aureolin.labs.carina.model;

import android.util.Log;

import com.fasterxml.jackson.annotation.JsonIgnore;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import co.aureolin.labs.carina.model.sde.SdeAttribute;
import co.aureolin.labs.carina.util.Helper;
import co.aureolin.labs.carina.util.CarinaSdeHelper;
import co.aureolin.labs.carina.util.XmlModelHelper;

public class Character implements IModel {
    private static final String[] characterInfoTags = {
            "race",
            "bloodline",
            "ancestry",
            "accountBalance",
            "skillPoints",
            "shipName",
            "shipTypeID",
            "shipTypeName",
            "corporationID",
            "corporation",
            "corporationDate",
            "lastKnownLocation",
            "securityStatus"
    };

    private static final String[] characterSheetTags = {
            "balance",
            "gender",
            "DoB",
            "cloneSkillPoints",
            "freeSkillPoints",
            "freeRespecs",
            "cloneJumpDate",
            "lastRespecDate",
            "lastTimedRespec",
            "remoteStationDate",
            "jumpActivation",
            "jumpFatigue",
            "jumpLastUpdate",
            "intelligence",
            "memory",
            "charisma",
            "perception",
            "willpower"
    };

    private static final List<String> characterSheetDateTags = new ArrayList<String>();
    static {
        characterSheetDateTags.addAll(Arrays.asList(new String[]{
                characterSheetTags[0],
                characterSheetTags[4],
                characterSheetTags[5],
                characterSheetTags[6],
                characterSheetTags[7],
                characterSheetTags[8],
                characterSheetTags[9],
                characterSheetTags[10]
        }));
    }

    /**
     * Basic API key info
     */
    private long id;

    private long apiKeyId;

    private String name;

    private long corporationId;

    private String corporationName;

    private Date corporationDate;

    private long allianceId;

    private String allianceName;

    private long factionId;

    private String factionName;

    /**
     * +Character Info
     */
    private String race;

    private String bloodline;

    private String ancestry;

    private String gender;

    private double accountBalance;

    private int skillPoints;

    private String shipName;

    private int shipTypeId;

    private String shipTypeName;

    private String lastKnownLocation;

    private double securityStatus;

    /**
     * +Character Sheet
     */
    private int homeStationId;

    private Date dob;

    private int cloneTypeId;

    private String cloneName;

    private int cloneSkillPoints;

    private int freeSkillPoints;

    private int freeRespecs;

    private Date cloneJumpDate;

    private Date lastRespecDate;

    private Date lastTimedRespec;

    private Date remoteStationDate;

    private Date jumpActivation;

    private Date jumpFatigue;

    private Date jumpLastUpdate;

    /**
     * Attributes
     */
    private int intelligence;

    private int memory;

    private int charisma;

    private int perception;

    private int willpower;

    private List<EmploymentHistoryItem> employmentHistory;

    private List<Implant> implants;

    private List<CharacterSkill> skills;

    private List<Certificate> certificates;

    private List<JumpClone> jumpClones;

    // Caching
    private Date characterInfoCachedUntil;

    private Date characterSheetCachedUntil;

    public Character() {

    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getApiKeyId() {
        return apiKeyId;
    }

    public void setApiKeyId(long apiKeyId) {
        this.apiKeyId = apiKeyId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getCorporationId() {
        return corporationId;
    }

    public void setCorporationId(long corporationId) {
        this.corporationId = corporationId;
    }

    public String getCorporationName() {
        return corporationName;
    }

    public void setCorporationName(String corporationName) {
        this.corporationName = corporationName;
    }

    public long getAllianceId() {
        return allianceId;
    }

    public void setAllianceId(long allianceId) {
        this.allianceId = allianceId;
    }

    public String getAllianceName() {
        return allianceName;
    }

    public void setAllianceName(String allianceName) {
        this.allianceName = allianceName;
    }

    public long getFactionId() {
        return factionId;
    }

    public void setFactionId(long factionId) {
        this.factionId = factionId;
    }

    public String getFactionName() {
        return factionName;
    }

    public void setFactionName(String factionName) {
        this.factionName = factionName;
    }

    public Date getCorporationDate() {
        return corporationDate;
    }

    public void setCorporationDate(Date corporationDate) {
        this.corporationDate = corporationDate;
    }

    public String getRace() {
        return race;
    }

    public void setRace(String race) {
        this.race = race;
    }

    public String getBloodline() {
        return bloodline;
    }

    public void setBloodline(String bloodline) {
        this.bloodline = bloodline;
    }

    public String getAncestry() {
        return ancestry;
    }

    public void setAncestry(String ancestry) {
        this.ancestry = ancestry;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public double getAccountBalance() {
        return accountBalance;
    }

    public void setAccountBalance(double accountBalance) {
        this.accountBalance = accountBalance;
    }

    public int getSkillPoints() {
        return skillPoints;
    }

    public void setSkillPoints(int skillPoints) {
        this.skillPoints = skillPoints;
    }

    public String getShipName() {
        return shipName;
    }

    public void setShipName(String shipName) {
        this.shipName = shipName;
    }

    public int getShipTypeId() {
        return shipTypeId;
    }

    public void setShipTypeId(int shipTypeId) {
        this.shipTypeId = shipTypeId;
    }

    public String getShipTypeName() {
        return shipTypeName;
    }

    public void setShipTypeName(String shipTypeName) {
        this.shipTypeName = shipTypeName;
    }

    public String getLastKnownLocation() {
        return lastKnownLocation;
    }

    public void setLastKnownLocation(String lastKnownLocation) {
        this.lastKnownLocation = lastKnownLocation;
    }

    public double getSecurityStatus() {
        return securityStatus;
    }

    public void setSecurityStatus(double securityStatus) {
        this.securityStatus = securityStatus;
    }

    public int getHomeStationId() {
        return homeStationId;
    }

    public void setHomeStationId(int homeStationId) {
        this.homeStationId = homeStationId;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public int getCloneTypeId() {
        return cloneTypeId;
    }

    public void setCloneTypeId(int cloneTypeId) {
        this.cloneTypeId = cloneTypeId;
    }

    public String getCloneName() {
        return cloneName;
    }

    public void setCloneName(String cloneName) {
        this.cloneName = cloneName;
    }

    public int getCloneSkillPoints() {
        return cloneSkillPoints;
    }

    public void setCloneSkillPoints(int cloneSkillPoints) {
        this.cloneSkillPoints = cloneSkillPoints;
    }

    public int getFreeSkillPoints() {
        return freeSkillPoints;
    }

    public void setFreeSkillPoints(int freeSkillPoints) {
        this.freeSkillPoints = freeSkillPoints;
    }

    public int getFreeRespecs() {
        return freeRespecs;
    }

    public void setFreeRespecs(int freeRespecs) {
        this.freeRespecs = freeRespecs;
    }

    public Date getCloneJumpDate() {
        return cloneJumpDate;
    }

    public void setCloneJumpDate(Date cloneJumpDate) {
        this.cloneJumpDate = cloneJumpDate;
    }

    public Date getLastRespecDate() {
        return lastRespecDate;
    }

    public void setLastRespecDate(Date lastRespecDate) {
        this.lastRespecDate = lastRespecDate;
    }

    public Date getLastTimedRespec() {
        return lastTimedRespec;
    }

    public void setLastTimedRespec(Date lastTimedRespec) {
        this.lastTimedRespec = lastTimedRespec;
    }

    public Date getRemoteStationDate() {
        return remoteStationDate;
    }

    public void setRemoteStationDate(Date remoteStationDate) {
        this.remoteStationDate = remoteStationDate;
    }

    public Date getJumpActivation() {
        return jumpActivation;
    }

    public void setJumpActivation(Date jumpActivation) {
        this.jumpActivation = jumpActivation;
    }

    public Date getJumpFatigue() {
        return jumpFatigue;
    }

    public void setJumpFatigue(Date jumpFatigue) {
        this.jumpFatigue = jumpFatigue;
    }

    public Date getJumpLastUpdate() {
        return jumpLastUpdate;
    }

    public void setJumpLastUpdate(Date jumpLastUpdate) {
        this.jumpLastUpdate = jumpLastUpdate;
    }

    public int getIntelligence() {
        return intelligence;
    }

    public void setIntelligence(int intelligence) {
        this.intelligence = intelligence;
    }

    public int getMemory() {
        return memory;
    }

    public void setMemory(int memory) {
        this.memory = memory;
    }

    public int getCharisma() {
        return charisma;
    }

    public void setCharisma(int charisma) {
        this.charisma = charisma;
    }

    public int getPerception() {
        return perception;
    }

    public void setPerception(int perception) {
        this.perception = perception;
    }

    public int getWillpower() {
        return willpower;
    }

    public void setWillpower(int willpower) {
        this.willpower = willpower;
    }

    public List<EmploymentHistoryItem> getEmploymentHistory() {
        return employmentHistory;
    }

    public void setEmploymentHistory(List<EmploymentHistoryItem> employmentHistory) {
        this.employmentHistory = employmentHistory;
    }

    public List<Implant> getImplants() {
        return implants;
    }

    public void setImplants(List<Implant> implants) {
        this.implants = implants;
    }

    public List<CharacterSkill> getSkills() {
        return skills;
    }

    public void setSkills(List<CharacterSkill> skills) {
        this.skills = skills;
    }

    public List<Certificate> getCertificates() {
        return certificates;
    }

    public void setCertificates(List<Certificate> certificates) {
        this.certificates = certificates;
    }

    public List<JumpClone> getJumpClones() {
        return jumpClones;
    }

    public void setJumpClones(List<JumpClone> jumpClones) {
        this.jumpClones = jumpClones;
    }

    public Date getCharacterInfoCachedUntil() {
        return characterInfoCachedUntil;
    }

    public void setCharacterInfoCachedUntil(Date characterInfoCachedUntil) {
        this.characterInfoCachedUntil = characterInfoCachedUntil;
    }

    public Date getCharacterSheetCachedUntil() {
        return characterSheetCachedUntil;
    }

    public void setCharacterSheetCachedUntil(Date characterSheetCachedUntil) {
        this.characterSheetCachedUntil = characterSheetCachedUntil;
    }

    public boolean hasAttributes() {
        return (charisma > 0 && intelligence > 0 && memory > 0 && perception > 0 && willpower > 0);
    }

    @JsonIgnore
    public int getAttributeValue(SdeAttribute.CharacterAttribute attribute) {
        if (attribute == SdeAttribute.CharacterAttribute.Charisma) {
            return charisma;
        }
        if (attribute == SdeAttribute.CharacterAttribute.Memory) {
            return memory;
        }
        if (attribute == SdeAttribute.CharacterAttribute.Intelligence) {
            return intelligence;
        }
        if (attribute == SdeAttribute.CharacterAttribute.Perception) {
            return perception;
        }
        if (attribute == SdeAttribute.CharacterAttribute.Willpower) {
            return willpower;
        }

        return 0;
    }

    public boolean updateFromCharacterInfo(XmlPullParser parser) {
        try {
            boolean lastPropertyLoaded = false;
            while (parser.next() != XmlPullParser.END_DOCUMENT) {
                if (parser.getEventType() != XmlPullParser.START_TAG) {
                    continue;
                }
                String name = parser.getName();
                for (int i = 0; i < characterInfoTags.length; i++) {
                    if (characterInfoTags[i].equals(name)) {
                        String value = XmlModelHelper.readXmlValue(name, parser);
                        setXmlProperty(name, value);
                        if (i == (characterInfoTags.length - 1)) {
                            lastPropertyLoaded = true;
                        }
                    }
                }

                if (lastPropertyLoaded) {
                    break;
                }
            }

            if (employmentHistory != null) {
                employmentHistory.clear();
            }

            boolean rowsetFound = false;
            while (parser.next() != XmlPullParser.END_DOCUMENT) {
                String name = parser.getName();
                if (parser.getEventType() == XmlPullParser.END_TAG
                        && "rowset".equals(name)) {
                    break;
                } else if (parser.getEventType() != XmlPullParser.START_TAG) {
                    continue;
                }

                if (!rowsetFound && "rowset".equals(name)) {
                    rowsetFound = true;
                }

                if (!rowsetFound) {
                    break;
                }

                if ("row".equals(name)) {
                    if (employmentHistory == null) {
                        employmentHistory = new ArrayList<EmploymentHistoryItem>();
                    }
                    EmploymentHistoryItem historyItem =
                            employmentHistoryItemFromParser(parser);
                    if (!employmentHistory.contains(historyItem)) {
                        employmentHistory.add(historyItem);
                    }
                }
            }

            // Get value of cachedUntil and store it for CharacterInfo
            while (parser.next() != XmlPullParser.END_DOCUMENT) {
                if (parser.getEventType() != XmlPullParser.START_TAG) {
                    continue;
                }

                String name = parser.getName();
                if ("cachedUntil".equals(name)) {
                    String cachedUntilStr = XmlModelHelper.readXmlValue("cachedUntil", parser);
                    characterInfoCachedUntil = Helper.parseIsoDate(cachedUntilStr, new Date(0));
                    break;
                }
            }
        } catch (XmlPullParserException | IOException e) {
            Log.e("#UpdateCharFailed", e.getMessage(), e);
            return false;
        }

        // update the history item end dates
        if (employmentHistory != null) {
            for (int i = 0, j = 1; i < employmentHistory.size() && j < employmentHistory.size(); i++, j++) {
                employmentHistory.get(j).setEndDate(employmentHistory.get(i).getStartDate());
            }
        }

        return true;
    }

    public boolean updateFromCharacterSheet(XmlPullParser parser) {
        jumpClones = new ArrayList<JumpClone>();
        implants = new ArrayList<Implant>();
        skills = new ArrayList<CharacterSkill>();
        certificates = new ArrayList<Certificate>();

        try {
            String currentRowset = null;
            while (parser.next() != XmlPullParser.END_DOCUMENT) {
                if (parser.getEventType() != XmlPullParser.START_TAG) {
                    continue;
                }
                boolean isSheetTag = false;
                String name = parser.getName();
                for (int i = 0; i < characterSheetTags.length; i++) {
                    if (characterSheetTags[i].equals(name)) {
                        String value = XmlModelHelper.readXmlValue(name, parser);
                        setXmlProperty(name, value);
                        isSheetTag = true;
                    }
                }

                if (!isSheetTag && "rowset".equals(name)) {
                    currentRowset = XmlModelHelper.getStringAttribute("name", parser);
                }

                if ("row".equals(name)) {
                    if ("jumpClones".equals(currentRowset)) {
                        JumpClone jumpClone = jumpCloneFromParser(parser);
                        if (jumpClone != null) {
                            jumpClones.add(jumpClone);
                        }
                    } else if ("jumpCloneImplants".equals(currentRowset)) {
                        Implant jumpCloneImplant = jumpCloneImplantFromParser(parser);
                        if (jumpCloneImplant != null) {
                            addJumpCloneImplant(jumpCloneImplant);
                        }
                    } else if ("implants".equals(currentRowset)) {
                        Implant implant = implantFromParser(parser);
                        if (implant != null) {
                            implants.add(implant);
                        }
                    } else if ("skills".equals(currentRowset)) {
                        CharacterSkill skill = characterSkillFromParser(parser);
                        if (skill != null) {
                            skills.add(skill);
                        }
                    } else if ("certificates".equals(currentRowset)) {
                        Certificate certificate = certificateFromParser(parser);
                        if (certificate != null) {
                            certificates.add(certificate);
                        }
                    }
                }

                if ("cachedUntil".equals(name)) {
                    String cachedUntilStr = XmlModelHelper.readXmlValue("cachedUntil", parser);
                    characterSheetCachedUntil = Helper.parseIsoDate(cachedUntilStr, new Date(0));
                    break;
                }
            }
        } catch (XmlPullParserException | IOException e) {
            Log.e("#UpdateCharFailed", e.getMessage(), e);
            return false;
        }

        return true;
    }

    private void addJumpCloneImplant(Implant implant) {
        if (jumpClones != null && jumpClones.size() > 0) {
            for (int i = 0; i < jumpClones.size(); i++) {
                JumpClone jumpClone = jumpClones.get(i);
                if (jumpClone.getId() == implant.getJumpCloneId()) {
                    if (jumpClone.getImplants() == null) {
                        jumpClone.setImplants(new ArrayList<Implant>());
                    }
                    jumpClone.getImplants().add(implant);
                    break;
                }
            }
        }
    }

    private static EmploymentHistoryItem employmentHistoryItemFromParser(XmlPullParser parser)
        throws XmlPullParserException, IOException {
        EmploymentHistoryItem item = new EmploymentHistoryItem();
        item.setId(XmlModelHelper.getLongAttribute("recordID", parser));
        item.setCorporationId(XmlModelHelper.getLongAttribute("corporationID", parser));
        item.setCorporationName(XmlModelHelper.getStringAttribute("corporationName", parser));
        item.setStartDate(XmlModelHelper.getDateAttribute("startDate", parser));

        return item;
    }

    private static JumpClone jumpCloneFromParser(XmlPullParser parser)
        throws XmlPullParserException, IOException {
        JumpClone jumpClone = new JumpClone();
        jumpClone.setId(XmlModelHelper.getLongAttribute("jumpCloneID", parser));
        jumpClone.setTypeId(XmlModelHelper.getLongAttribute("typeID", parser));
        jumpClone.setLocationId(XmlModelHelper.getLongAttribute("locationID", parser));
        jumpClone.setName(XmlModelHelper.getStringAttribute("cloneName", parser));
        return jumpClone;
    }

    private static Implant jumpCloneImplantFromParser(XmlPullParser parser)
        throws XmlPullParserException, IOException {
        Implant implant = implantFromParser(parser);
        implant.setJumpCloneId(XmlModelHelper.getLongAttribute("jumpCloneID", parser));
        return implant;
    }

    private static Implant implantFromParser(XmlPullParser parser)
            throws XmlPullParserException, IOException {
        Implant implant = new Implant();
        implant.setId(XmlModelHelper.getLongAttribute("typeID", parser));
        implant.setName(XmlModelHelper.getStringAttribute("typeName", parser));
        return implant;
    }

    private static CharacterSkill characterSkillFromParser(XmlPullParser parser)
            throws XmlPullParserException, IOException {
        CharacterSkill skill = new CharacterSkill();
        skill.setId(XmlModelHelper.getLongAttribute("typeID", parser));
        skill.setName(CarinaSdeHelper.getSkillName(skill.getId()));
        skill.setLevel(XmlModelHelper.getIntAttribute("level", parser));
        skill.setSkillPoints(XmlModelHelper.getIntAttribute("skillpoints", parser));
        skill.setPublished(XmlModelHelper.getIntAttribute("published", parser) == 1);

        return skill;
    }

    // TODO: Certificate parsing
    private static Certificate certificateFromParser(XmlPullParser parser)
            throws XmlPullParserException, IOException {
        return null;
    }

    private void setXmlProperty(String name, String value) {
        // TODO: Use field name map and reflection to set values?
        if ("race".equals(name)) {
            race = value;
        } else if ("bloodline".equals(name)) {
            bloodline = value;
        } else if ("ancestry".equals(name)) {
            ancestry = value;
        } else if ("accountBalance".equals(name)) {
            accountBalance = Helper.parseDouble(value, 0);
        } else if ("skillPoints".equals(name)) {
            skillPoints = Helper.parseInt(value, -1);
        } else if ("shipName".equals(name)) {
            shipName = value;
        } else if ("shipTypeID".equals(name)) {
            shipTypeId = Helper.parseInt(value, -1);
        } else if ("shipTypeName".equals(name)) {
            shipTypeName = value;
        } else if ("corporationID".equals(name)) {
            int id = Helper.parseInt(value, -1);
            corporationId = (id > -1) ? id : corporationId;
        } else if ("corporation".equals(name)) {
            corporationName = (value != null && value.trim().length() > 0) ? value : corporationName;
        } else if ("corporationDate".equals(name)) {
            corporationDate = Helper.parseIsoDate(value, new Date(0));
        } else if ("lastKnownLocation".equals(name)) {
            lastKnownLocation = value;
        } else if ("securityStatus".equals(name)) {
            securityStatus = Helper.parseDouble(value, -1);
        }

        /*"gender",
                "DoB",
                "cloneSkillPoints",
                "freeSkillPoints",
                "freeRespecs",
                "cloneJumpDate",
                "lastRespecDate",
                "lastTimedRespec",
                "remoteStationDate",
                "jumpActivation",
                "jumpFatigue",
                "jumpLastUpdate",*/

        // Character Sheet
        else if ("charisma".equals(name)) {
            charisma = Helper.parseInt(value, -1);
        } else if ("intelligence".equals(name)) {
            intelligence = Helper.parseInt(value, -1);
        } else if ("memory".equals(name)) {
            memory = Helper.parseInt(value, -1);
        } else if ("perception".equals(name)) {
            perception = Helper.parseInt(value, -1);
        } else if ("willpower".equals(name)) {
            willpower = Helper.parseInt(value, -1);
        } else if ("balance".equals(name)) {
            accountBalance = Helper.parseDouble(value, 0);
        }
    }

    public static Character fromXmlAttributes(XmlPullParser parser) {
        Character character = null;

        try {
            if (parser.getEventType() == XmlPullParser.START_TAG) {
                String name = parser.getName();
                if ("row".equals(name)) {
                    character = new Character();
                    character.setId(XmlModelHelper.getLongAttribute("characterID", parser));
                    character.setName(XmlModelHelper.getStringAttribute("characterName", parser));
                    character.setCorporationId(XmlModelHelper.getLongAttribute("corporationID", parser));
                    character.setCorporationName(XmlModelHelper.getStringAttribute("corporationName", parser));
                    character.setAllianceId(XmlModelHelper.getLongAttribute("allianceID", parser));
                    character.setAllianceName(XmlModelHelper.getStringAttribute("allianceName", parser));
                    character.setFactionId(XmlModelHelper.getLongAttribute("factionID", parser));
                    character.setFactionName(XmlModelHelper.getStringAttribute("factionName", parser));
                }
            }
        } catch (XmlPullParserException e) {
            // Null will be returned
        }

        return character;
    }

    public static Map<SdeAttribute.CharacterAttribute, Integer> AttributeMapForCharacter(Character character) {
        Map<SdeAttribute.CharacterAttribute, Integer> attributeMap =
                new HashMap<SdeAttribute.CharacterAttribute, Integer>();
        if (character != null) {
            attributeMap.put(SdeAttribute.CharacterAttribute.Charisma, character.getCharisma());
            attributeMap.put(SdeAttribute.CharacterAttribute.Intelligence, character.getIntelligence());
            attributeMap.put(SdeAttribute.CharacterAttribute.Memory, character.getMemory());
            attributeMap.put(SdeAttribute.CharacterAttribute.Perception, character.getPerception());
            attributeMap.put(SdeAttribute.CharacterAttribute.Willpower, character.getWillpower());
        }

        return attributeMap;
    }
}
