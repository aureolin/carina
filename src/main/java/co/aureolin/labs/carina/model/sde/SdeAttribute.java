/**
 * Project Carina - EVE Fitting Engine Android Library
 *
 * Copyright (c) 2015 Aureolin
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package co.aureolin.labs.carina.model.sde;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.text.NumberFormat;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import co.aureolin.labs.carina.util.Helper;

/**
 * Created by Akinwale on 25/09/2015.
 */
public class SdeAttribute {
    public enum CharacterAttribute {
        Charisma(164),
        Intelligence(165),
        Memory(166),
        Perception(167),
        Willpower(168);

        private int id;

        CharacterAttribute(int id) {
            this.id = id;
        }

        public final int getId() {
            return this.id;
        }

        public static CharacterAttribute fromId(int id) {
            switch (id) {
                case 164:
                    return Charisma;
                case 165:
                    return Intelligence;
                case 166:
                    return Memory;
                case 167:
                    return Perception;
                case 168:
                    return Willpower;
            }
            return null;
        }
    }

    private static List<String> PrefixUnits = Arrays.asList(new String[] {
            "Slot",
            "Level"
    });

    // dgmAttributeTypes for Charisma, Willpower, etc.
    public static Map<Integer, String> CharacterAttributesMap = new HashMap<Integer, String>();
    static {
        CharacterAttributesMap.put(164, "Charisma");
        CharacterAttributesMap.put(165, "Intelligence");
        CharacterAttributesMap.put(166, "Memory");
        CharacterAttributesMap.put(167, "Perception");
        CharacterAttributesMap.put(168, "Willpower");
    }

    public static Map<String, String> AttributeNameStringMap = new HashMap<String, String>();
    static {
        AttributeNameStringMap.put("AI_TankingModifierDrone", "AI_TankingModifierDrone");
        AttributeNameStringMap.put("WarpSBonus", "WarpSBonus");
        AttributeNameStringMap.put("accessDifficultyBonus", "accessDifficultyBonus");
        AttributeNameStringMap.put("accessDifficultyBonusAbsolutePercent", "accessDifficultyBonusAbsolutePercent");
        AttributeNameStringMap.put("accessDifficultyBonusModifier", "accessDifficultyBonusModifier");
        AttributeNameStringMap.put("advancedIndustrySkillIndustryJobTimeBonus", "advancedIndustrySkillIndustryJobTimeBonus");
        AttributeNameStringMap.put("agility", "agility");
        AttributeNameStringMap.put("agilityBonus", "agilityBonus");
        AttributeNameStringMap.put("agilityMultiplier", "agilityMultiplier");
        AttributeNameStringMap.put("anchoringDelay", "anchoringDelay");
        AttributeNameStringMap.put("anchoringRequiresSovUpgrade1", "anchoringRequiresSovUpgrade1");
        AttributeNameStringMap.put("anchoringSecurityLevelMax", "anchoringSecurityLevelMax");
        AttributeNameStringMap.put("anchoringSecurityLevelMin", "anchoringSecurityLevelMin");
        AttributeNameStringMap.put("aoeCloudSize", "aoeCloudSize");
        AttributeNameStringMap.put("aoeCloudSizeBonus", "aoeCloudSizeBonus");
        AttributeNameStringMap.put("aoeCloudSizeBonusBonus", "aoeCloudSizeBonusBonus");
        AttributeNameStringMap.put("aoeCloudSizeMultiplier", "aoeCloudSizeMultiplier");
        AttributeNameStringMap.put("aoeVelocity", "aoeVelocity");
        AttributeNameStringMap.put("aoeVelocityBonus", "aoeVelocityBonus");
        AttributeNameStringMap.put("aoeVelocityBonusBonus", "aoeVelocityBonusBonus");
        AttributeNameStringMap.put("aoeVelocityMultiplier", "aoeVelocityMultiplier");
        AttributeNameStringMap.put("armorDamage", "armorDamage");
        AttributeNameStringMap.put("armorDamageAmount", "armorDamageAmount");
        AttributeNameStringMap.put("armorDamageAmountBonus", "armorDamageAmountBonus");
        AttributeNameStringMap.put("armorDamageAmountMultiplier", "armorDamageAmountMultiplier");
        AttributeNameStringMap.put("armorDamageAmountMultiplierRemote", "armorDamageAmountMultiplierRemote");
        AttributeNameStringMap.put("armorDamageDurationBonus", "armorDamageDurationBonus");
        AttributeNameStringMap.put("armorEmDamageResistanceBonus", "armorEmDamageResistanceBonus");
        AttributeNameStringMap.put("armorEmDamageResonance", "armorEmDamageResonance");
        AttributeNameStringMap.put("armorExplosiveDamageResistanceBonus", "armorExplosiveDamageResistanceBonus");
        AttributeNameStringMap.put("armorExplosiveDamageResonance", "armorExplosiveDamageResonance");
        AttributeNameStringMap.put("armorHP", "Armor Hitpoints");
        AttributeNameStringMap.put("armorHPBonusAdd", "armorHPBonusAdd");
        AttributeNameStringMap.put("armorHPMultiplier", "armorHPMultiplier");
        AttributeNameStringMap.put("armorHpBonus", "armorHpBonus");
        AttributeNameStringMap.put("armorHpBonus2", "armorHpBonus2");
        AttributeNameStringMap.put("armorKineticDamageResistanceBonus", "armorKineticDamageResistanceBonus");
        AttributeNameStringMap.put("armorKineticDamageResonance", "armorKineticDamageResonance");
        AttributeNameStringMap.put("armorThermalDamageResistanceBonus", "armorThermalDamageResistanceBonus");
        AttributeNameStringMap.put("armorThermalDamageResonance", "armorThermalDamageResonance");
        AttributeNameStringMap.put("baseArmorDamage", "baseArmorDamage");
        AttributeNameStringMap.put("baseMaxScanDeviation", "baseMaxScanDeviation");
        AttributeNameStringMap.put("baseScanRange", "baseScanRange");
        AttributeNameStringMap.put("baseSensorStrength", "baseSensorStrength");
        AttributeNameStringMap.put("baseShieldDamage", "baseShieldDamage");
        AttributeNameStringMap.put("blueprintmanufactureTimeBonus", "blueprintmanufactureTimeBonus");
        AttributeNameStringMap.put("boosterAOEVelocityPenalty", "boosterAOEVelocityPenalty");
        AttributeNameStringMap.put("boosterArmorHPPenalty", "boosterArmorHPPenalty");
        AttributeNameStringMap.put("boosterArmorRepairAmountPenalty", "boosterArmorRepairAmountPenalty");
        AttributeNameStringMap.put("boosterAttributeModifier", "boosterAttributeModifier");
        AttributeNameStringMap.put("boosterCapacitorCapacityPenalty", "boosterCapacitorCapacityPenalty");
        AttributeNameStringMap.put("boosterChanceBonus", "boosterChanceBonus");
        AttributeNameStringMap.put("boosterDuration", "boosterDuration");
        AttributeNameStringMap.put("boosterEffectChance1", "boosterEffectChance1");
        AttributeNameStringMap.put("boosterMaxCharAgeHours", "boosterMaxCharAgeHours");
        AttributeNameStringMap.put("boosterMaxVelocityPenalty", "boosterMaxVelocityPenalty");
        AttributeNameStringMap.put("boosterMissileAOECloudPenalty", "boosterMissileAOECloudPenalty");
        AttributeNameStringMap.put("boosterMissileVelocityPenalty", "boosterMissileVelocityPenalty");
        AttributeNameStringMap.put("boosterShieldBoostAmountPenalty", "boosterShieldBoostAmountPenalty");
        AttributeNameStringMap.put("boosterShieldCapacityPenalty", "boosterShieldCapacityPenalty");
        AttributeNameStringMap.put("boosterTurretFalloffPenalty", "boosterTurretFalloffPenalty");
        AttributeNameStringMap.put("boosterTurretOptimalRange", "boosterTurretOptimalRange");
        AttributeNameStringMap.put("boosterTurretTrackingPenalty", "boosterTurretTrackingPenalty");
        AttributeNameStringMap.put("boosterness", "boosterness");
        AttributeNameStringMap.put("canFitShipGroup1", "canFitShipGroup1");
        AttributeNameStringMap.put("canFitShipGroup2", "canFitShipGroup2");
        AttributeNameStringMap.put("canFitShipGroup3", "canFitShipGroup3");
        AttributeNameStringMap.put("canFitShipGroup4", "canFitShipGroup4");
        AttributeNameStringMap.put("canFitShipGroup5", "canFitShipGroup5");
        AttributeNameStringMap.put("canFitShipGroup6", "canFitShipGroup6");
        AttributeNameStringMap.put("canFitShipGroup7", "canFitShipGroup7");
        AttributeNameStringMap.put("canFitShipGroup8", "canFitShipGroup8");
        AttributeNameStringMap.put("canFitShipType1", "canFitShipType1");
        AttributeNameStringMap.put("canFitShipType2", "canFitShipType2");
        AttributeNameStringMap.put("canFitShipType3", "canFitShipType3");
        AttributeNameStringMap.put("canFitShipType4", "canFitShipType4");
        AttributeNameStringMap.put("canFitShipType5", "canFitShipType5");
        AttributeNameStringMap.put("capAttackReflector", "capAttackReflector");
        AttributeNameStringMap.put("capNeedBonus", "capNeedBonus");
        AttributeNameStringMap.put("capRechargeBonus", "capRechargeBonus");
        AttributeNameStringMap.put("capacitorBonus", "capacitorBonus");
        AttributeNameStringMap.put("capacitorCapacity", "Capacitor Capacity");
        AttributeNameStringMap.put("capacitorCapacityBonus", "capacitorCapacityBonus");
        AttributeNameStringMap.put("capacitorCapacityMultiplier", "capacitorCapacityMultiplier");
        AttributeNameStringMap.put("capacitorCapacityMultiplierSystem", "capacitorCapacityMultiplierSystem");
        AttributeNameStringMap.put("capacitorNeed", "capacitorNeed");
        AttributeNameStringMap.put("capacitorNeedMultiplier", "capacitorNeedMultiplier");
        AttributeNameStringMap.put("capacitorRechargeRateMultiplier", "capacitorRechargeRateMultiplier");
        AttributeNameStringMap.put("capacityBonus", "capacityBonus");
        AttributeNameStringMap.put("capacitySecondary", "capacitySecondary");
        AttributeNameStringMap.put("cargoCapacityBonus", "cargoCapacityBonus");
        AttributeNameStringMap.put("cargoCapacityMultiplier", "cargoCapacityMultiplier");
        AttributeNameStringMap.put("cargoScanRange", "cargoScanRange");
        AttributeNameStringMap.put("chargeGroup1", "chargeGroup1");
        AttributeNameStringMap.put("chargeGroup2", "chargeGroup2");
        AttributeNameStringMap.put("chargeGroup3", "chargeGroup3");
        AttributeNameStringMap.put("chargeGroup4", "chargeGroup4");
        AttributeNameStringMap.put("chargeRate", "chargeRate");
        AttributeNameStringMap.put("chargeSize", "chargeSize");
        AttributeNameStringMap.put("chargedArmorDamageMultiplier", "chargedArmorDamageMultiplier");
        AttributeNameStringMap.put("charismaBonus", "Charisma Modifier");
        AttributeNameStringMap.put("cloakingTargetingDelay", "cloakingTargetingDelay");
        AttributeNameStringMap.put("cloakingTargetingDelayBonus", "cloakingTargetingDelayBonus");
        AttributeNameStringMap.put("commandBonus", "commandBonus");
        AttributeNameStringMap.put("commandBonusECM", "commandBonusECM");
        AttributeNameStringMap.put("commandBonusRSD", "commandBonusRSD");
        AttributeNameStringMap.put("commandBonusTD", "commandBonusTD");
        AttributeNameStringMap.put("commandBonusTP", "commandBonusTP");
        AttributeNameStringMap.put("compressionQuantityNeeded", "compressionQuantityNeeded");
        AttributeNameStringMap.put("compressionTypeID", "compressionTypeID");
        AttributeNameStringMap.put("consumptionQuantity", "consumptionQuantity");
        AttributeNameStringMap.put("consumptionQuantityBonus", "consumptionQuantityBonus");
        AttributeNameStringMap.put("consumptionQuantityBonusPercent", "consumptionQuantityBonusPercent");
        AttributeNameStringMap.put("consumptionQuantityBonusPercentage", "consumptionQuantityBonusPercentage");
        AttributeNameStringMap.put("consumptionType", "consumptionType");
        AttributeNameStringMap.put("controlTowerMinimumDistance", "controlTowerMinimumDistance");
        AttributeNameStringMap.put("controlTowerSize", "controlTowerSize");
        AttributeNameStringMap.put("copySpeedBonus", "copySpeedBonus");
        AttributeNameStringMap.put("corporationMemberBonus", "corporationMemberBonus");
        AttributeNameStringMap.put("covertCloakCPUAdd", "covertCloakCPUAdd");
        AttributeNameStringMap.put("covertOpsAndReconOpsCloakModuleDelay", "covertOpsAndReconOpsCloakModuleDelay");
        AttributeNameStringMap.put("cpu", "cpu");
        AttributeNameStringMap.put("cpuLoad", "cpuLoad");
        AttributeNameStringMap.put("cpuLoadLevelModifier", "cpuLoadLevelModifier");
        AttributeNameStringMap.put("cpuLoadPerKm", "cpuLoadPerKm");
        AttributeNameStringMap.put("cpuMultiplier", "cpuMultiplier");
        AttributeNameStringMap.put("cpuNeedBonus", "cpuNeedBonus");
        AttributeNameStringMap.put("cpuOutput", "CPU Output");
        AttributeNameStringMap.put("cpuOutputBonus2", "cpuOutputBonus2");
        AttributeNameStringMap.put("cpuPenaltyPercent", "cpuPenaltyPercent");
        AttributeNameStringMap.put("criminalConnectionsMutator", "criminalConnectionsMutator");
        AttributeNameStringMap.put("crystalVolatilityChance", "crystalVolatilityChance");
        AttributeNameStringMap.put("crystalVolatilityDamage", "crystalVolatilityDamage");
        AttributeNameStringMap.put("crystalsGetDamaged", "crystalsGetDamaged");
        AttributeNameStringMap.put("damage", "damage");
        AttributeNameStringMap.put("damageHP", "damageHP");
        AttributeNameStringMap.put("damageMultiplier", "damageMultiplier");
        AttributeNameStringMap.put("damageMultiplierBonus", "Damage Multiplier Bonus");
        AttributeNameStringMap.put("damageMultiplierMultiplier", "damageMultiplierMultiplier");
        AttributeNameStringMap.put("devIndexIndustrial", "devIndexIndustrial");
        AttributeNameStringMap.put("devIndexMilitary", "devIndexMilitary");
        AttributeNameStringMap.put("devIndexSovereignty", "devIndexSovereignty");
        AttributeNameStringMap.put("diplomacyMutator", "diplomacyMutator");
        AttributeNameStringMap.put("disallowActivateInForcefield", "disallowActivateInForcefield");
        AttributeNameStringMap.put("disallowActivateOnWarp", "disallowActivateOnWarp");
        AttributeNameStringMap.put("disallowAssistance", "disallowAssistance");
        AttributeNameStringMap.put("disallowInEmpireSpace", "disallowInEmpireSpace");
        AttributeNameStringMap.put("disallowInHighSec", "disallowInHighSec");
        AttributeNameStringMap.put("disallowRepeatingActivation", "disallowRepeatingActivation");
        AttributeNameStringMap.put("drawback", "drawback");
        AttributeNameStringMap.put("droneBandwidth", "Drone Bandwidth");
        AttributeNameStringMap.put("droneBandwidthUsed", "droneBandwidthUsed");
        AttributeNameStringMap.put("droneCapacity", "Drone Capacity");
        AttributeNameStringMap.put("droneDamageBonus", "droneDamageBonus");
        AttributeNameStringMap.put("droneMaxVelocityBonus", "droneMaxVelocityBonus");
        AttributeNameStringMap.put("droneRangeBonus", "droneRangeBonus");
        AttributeNameStringMap.put("dscanImmune", "dscanImmune");
        AttributeNameStringMap.put("duration", "duration");
        AttributeNameStringMap.put("durationBonus", "durationBonus");
        AttributeNameStringMap.put("durationSkillBonus", "durationSkillBonus");
        AttributeNameStringMap.put("ecmBurstRange", "ecmBurstRange");
        AttributeNameStringMap.put("ecmRangeBonus", "ecmRangeBonus");
        AttributeNameStringMap.put("ecmStrengthBonusPercent", "ecmStrengthBonusPercent");
        AttributeNameStringMap.put("ecuExtractorHeadCPU", "ecuExtractorHeadCPU");
        AttributeNameStringMap.put("ecuExtractorHeadPower", "ecuExtractorHeadPower");
        AttributeNameStringMap.put("emDamage", "emDamage");
        AttributeNameStringMap.put("emDamageResistanceBonus", "emDamageResistanceBonus");
        AttributeNameStringMap.put("emDamageResonance", "emDamageResonance");
        AttributeNameStringMap.put("emDamageResonanceMultiplier", "emDamageResonanceMultiplier");
        AttributeNameStringMap.put("empFieldRange", "empFieldRange");
        AttributeNameStringMap.put("empFieldRangeMultiplier", "empFieldRangeMultiplier");
        AttributeNameStringMap.put("energyDestabilizationAmount", "energyDestabilizationAmount");
        AttributeNameStringMap.put("energyDestabilizationRange", "energyDestabilizationRange");
        AttributeNameStringMap.put("energyWarfareStrengthMultiplier", "energyWarfareStrengthMultiplier");
        AttributeNameStringMap.put("entityCruiseSpeed", "entityCruiseSpeed");
        AttributeNameStringMap.put("ewCapacitorNeedBonus", "ewCapacitorNeedBonus");
        AttributeNameStringMap.put("explosionDelay", "explosionDelay");
        AttributeNameStringMap.put("explosionDelayBonus", "explosionDelayBonus");
        AttributeNameStringMap.put("explosionDelayBonusBonus", "explosionDelayBonusBonus");
        AttributeNameStringMap.put("explosiveDamage", "explosiveDamage");
        AttributeNameStringMap.put("explosiveDamageResistanceBonus", "explosiveDamageResistanceBonus");
        AttributeNameStringMap.put("explosiveDamageResonance", "explosiveDamageResonance");
        AttributeNameStringMap.put("explosiveDamageResonanceMultiplier", "explosiveDamageResonanceMultiplier");
        AttributeNameStringMap.put("exportTax", "exportTax");
        AttributeNameStringMap.put("fallofMultiplier", "fallofMultiplier");
        AttributeNameStringMap.put("falloff", "falloff");
        AttributeNameStringMap.put("falloffBonus", "falloffBonus");
        AttributeNameStringMap.put("falloffBonusBonus", "falloffBonusBonus");
        AttributeNameStringMap.put("fitsToShipType", "fitsToShipType");
        AttributeNameStringMap.put("fleetHangarCapacity", "fleetHangarCapacity");
        AttributeNameStringMap.put("freighterBonusO1", "freighterBonusO1");
        AttributeNameStringMap.put("freighterBonusO2", "freighterBonusO2");
        AttributeNameStringMap.put("gateScrambleStatus", "gateScrambleStatus");
        AttributeNameStringMap.put("gender", "gender");
        AttributeNameStringMap.put("hardeningBonus", "hardeningBonus");
        AttributeNameStringMap.put("harvesterType", "harvesterType");
        AttributeNameStringMap.put("heatDamage", "heatDamage");
        AttributeNameStringMap.put("heatDamageBonus", "heatDamageBonus");
        AttributeNameStringMap.put("heatDamageMultiplier", "heatDamageMultiplier");
        AttributeNameStringMap.put("hiSlotModifier", "hiSlotModifier");
        AttributeNameStringMap.put("hiSlots", "High Power Slots");
        AttributeNameStringMap.put("hp", "Structure Hitpoints");
        AttributeNameStringMap.put("hullEmDamageResonance", "hullEmDamageResonance");
        AttributeNameStringMap.put("hullExplosiveDamageResonance", "hullExplosiveDamageResonance");
        AttributeNameStringMap.put("hullHpBonus", "hullHpBonus");
        AttributeNameStringMap.put("hullKineticDamageResonance", "hullKineticDamageResonance");
        AttributeNameStringMap.put("hullThermalDamageResonance", "hullThermalDamageResonance");
        AttributeNameStringMap.put("iceHarvestCycleBonus", "iceHarvestCycleBonus");
        AttributeNameStringMap.put("implantBonusVelocity", "implantBonusVelocity");
        AttributeNameStringMap.put("implantSetAngel", "implantSetAngel");
        AttributeNameStringMap.put("implantSetBloodraider", "implantSetBloodraider");
        AttributeNameStringMap.put("implantSetChristmas", "implantSetChristmas");
        AttributeNameStringMap.put("implantSetGuristas", "implantSetGuristas");
        AttributeNameStringMap.put("implantSetMordus", "implantSetMordus");
        AttributeNameStringMap.put("implantSetORE", "implantSetORE");
        AttributeNameStringMap.put("implantSetSansha", "implantSetSansha");
        AttributeNameStringMap.put("implantSetSerpentis", "implantSetSerpentis");
        AttributeNameStringMap.put("implantSetSisters", "implantSetSisters");
        AttributeNameStringMap.put("implantSetSyndicate", "implantSetSyndicate");
        AttributeNameStringMap.put("implantSetThukker", "implantSetThukker");
        AttributeNameStringMap.put("implantSetWarpSpeed", "implantSetWarpSpeed");
        AttributeNameStringMap.put("implantness", "Implant Slot");
        AttributeNameStringMap.put("importTax", "importTax");
        AttributeNameStringMap.put("intelligenceBonus", "Intelligence Modifier");
        AttributeNameStringMap.put("isCovert", "isCovert");
        AttributeNameStringMap.put("jumpDelayDuration", "jumpDelayDuration");
        AttributeNameStringMap.put("jumpDriveCapacitorNeed", "jumpDriveCapacitorNeed");
        AttributeNameStringMap.put("jumpDriveCapacitorNeedBonus", "jumpDriveCapacitorNeedBonus");
        AttributeNameStringMap.put("jumpDriveConsumptionAmount", "jumpDriveConsumptionAmount");
        AttributeNameStringMap.put("jumpDriveConsumptionType", "jumpDriveConsumptionType");
        AttributeNameStringMap.put("jumpDriveRange", "jumpDriveRange");
        AttributeNameStringMap.put("jumpDriveRangeBonus", "jumpDriveRangeBonus");
        AttributeNameStringMap.put("jumpFatigueMultiplier", "jumpFatigueMultiplier");
        AttributeNameStringMap.put("jumpPortalCapacitorNeed", "jumpPortalCapacitorNeed");
        AttributeNameStringMap.put("jumpThroughFatigueMultiplier", "jumpThroughFatigueMultiplier");
        AttributeNameStringMap.put("kineticDamage", "kineticDamage");
        AttributeNameStringMap.put("kineticDamageResistanceBonus", "kineticDamageResistanceBonus");
        AttributeNameStringMap.put("kineticDamageResonance", "kineticDamageResonance");
        AttributeNameStringMap.put("kineticDamageResonanceMultiplier", "kineticDamageResonanceMultiplier");
        AttributeNameStringMap.put("launcherGroup", "launcherGroup");
        AttributeNameStringMap.put("launcherGroup2", "launcherGroup2");
        AttributeNameStringMap.put("launcherGroup3", "launcherGroup3");
        AttributeNameStringMap.put("launcherHardPointModifier", "launcherHardPointModifier");
        AttributeNameStringMap.put("launcherSlotsLeft", "Launcher Hardpoints");
        AttributeNameStringMap.put("logisticalCapacity", "logisticalCapacity");
        AttributeNameStringMap.put("lowSlotModifier", "lowSlotModifier");
        AttributeNameStringMap.put("lowSlots", "Low Power Slots");
        AttributeNameStringMap.put("manufactureTimePerLevel", "manufactureTimePerLevel");
        AttributeNameStringMap.put("manufacturingSlotBonus", "manufacturingSlotBonus");
        AttributeNameStringMap.put("manufacturingTimeBonus", "manufacturingTimeBonus");
        AttributeNameStringMap.put("massAddition", "massAddition");
        AttributeNameStringMap.put("massBonusPercentage", "massBonusPercentage");
        AttributeNameStringMap.put("massBonusPercentageBonus", "massBonusPercentageBonus");
        AttributeNameStringMap.put("massMultiplier", "massMultiplier");
        AttributeNameStringMap.put("massPenaltyReduction", "massPenaltyReduction");
        AttributeNameStringMap.put("maxActiveDroneBonus", "maxActiveDroneBonus");
        AttributeNameStringMap.put("maxDronePercentageBonus", "maxDronePercentageBonus");
        AttributeNameStringMap.put("maxFlightTimeBonus", "maxFlightTimeBonus");
        AttributeNameStringMap.put("maxGangModules", "maxGangModules");
        AttributeNameStringMap.put("maxGroupFitted", "maxGroupFitted");
        AttributeNameStringMap.put("maxJumpClones", "maxJumpClones");
        AttributeNameStringMap.put("maxJumpClonesBonus", "maxJumpClonesBonus");
        AttributeNameStringMap.put("maxLockedTargets", "Maximum Locked Targets");
        AttributeNameStringMap.put("maxLockedTargetsBonus", "maxLockedTargetsBonus");
        AttributeNameStringMap.put("maxOperationalDistance", "maxOperationalDistance");
        AttributeNameStringMap.put("maxOperationalUsers", "maxOperationalUsers");
        AttributeNameStringMap.put("maxRange", "maxRange");
        AttributeNameStringMap.put("maxRangeBonus", "maxRangeBonus");
        AttributeNameStringMap.put("maxRangeBonus2", "maxRangeBonus2");
        AttributeNameStringMap.put("maxRangeBonusBonus", "maxRangeBonusBonus");
        AttributeNameStringMap.put("maxRangeMultiplier", "maxRangeMultiplier");
        AttributeNameStringMap.put("maxScanDeviation", "maxScanDeviation");
        AttributeNameStringMap.put("maxScanDeviationModifier", "maxScanDeviationModifier");
        AttributeNameStringMap.put("maxScanDeviationModifierModule", "maxScanDeviationModifierModule");
        AttributeNameStringMap.put("maxStructureDistance", "maxStructureDistance");
        AttributeNameStringMap.put("maxTargetBonus", "maxTargetBonus");
        AttributeNameStringMap.put("maxTargetRange", "Maximum Targeting Range");
        AttributeNameStringMap.put("maxTargetRangeBonus", "maxTargetRangeBonus");
        AttributeNameStringMap.put("maxTargetRangeBonusBonus", "maxTargetRangeBonusBonus");
        AttributeNameStringMap.put("maxTargetRangeMultiplier", "maxTargetRangeMultiplier");
        AttributeNameStringMap.put("maxTractorVelocity", "maxTractorVelocity");
        AttributeNameStringMap.put("maxVelocity", "Maximum Velocity");
        AttributeNameStringMap.put("maxVelocityActivationLimit", "maxVelocityActivationLimit");
        AttributeNameStringMap.put("maxVelocityBonus", "maxVelocityBonus");
        AttributeNameStringMap.put("medSlotModifier", "medSlotModifier");
        AttributeNameStringMap.put("medSlots", "Medium Power Slots");
        AttributeNameStringMap.put("memoryBonus", "Memory Modifier");
        AttributeNameStringMap.put("metaLevel", "Meta Level");
        AttributeNameStringMap.put("minScanDeviation", "minScanDeviation");
        AttributeNameStringMap.put("mindlinkBonus", "mindlinkBonus");
        AttributeNameStringMap.put("mineralNeedResearchBonus", "mineralNeedResearchBonus");
        AttributeNameStringMap.put("miningAmount", "miningAmount");
        AttributeNameStringMap.put("miningAmountBonus", "miningAmountBonus");
        AttributeNameStringMap.put("missileDamageMultiplier", "missileDamageMultiplier");
        AttributeNameStringMap.put("missileDamageMultiplierBonus", "missileDamageMultiplierBonus");
        AttributeNameStringMap.put("missileEntityAoeCloudSizeMultiplier", "missileEntityAoeCloudSizeMultiplier");
        AttributeNameStringMap.put("missileEntityAoeVelocityMultiplier", "missileEntityAoeVelocityMultiplier");
        AttributeNameStringMap.put("missileEntityFlightTimeMultiplier", "missileEntityFlightTimeMultiplier");
        AttributeNameStringMap.put("missileEntityVelocityMultiplier", "missileEntityVelocityMultiplier");
        AttributeNameStringMap.put("missileLaunchDuration", "missileLaunchDuration");
        AttributeNameStringMap.put("missileVelocityBonus", "missileVelocityBonus");
        AttributeNameStringMap.put("missileVelocityBonusBonus", "missileVelocityBonusBonus");
        AttributeNameStringMap.put("missileVelocityMultiplier", "missileVelocityMultiplier");
        AttributeNameStringMap.put("moduleReactivationDelay", "moduleReactivationDelay");
        AttributeNameStringMap.put("moduleRepairRateBonus", "moduleRepairRateBonus");
        AttributeNameStringMap.put("moonAnchorDistance", "moonAnchorDistance");
        AttributeNameStringMap.put("moonMiningAmount", "moonMiningAmount");
        AttributeNameStringMap.put("negotiationBonus", "negotiationBonus");
        AttributeNameStringMap.put("neutReflectAmountBonus", "neutReflectAmountBonus");
        AttributeNameStringMap.put("nosOverride", "nosOverride");
        AttributeNameStringMap.put("nosReflectAmountBonus", "nosReflectAmountBonus");
        AttributeNameStringMap.put("numDays", "numDays");
        AttributeNameStringMap.put("onliningDelay", "onliningDelay");
        AttributeNameStringMap.put("onliningRequiresSovereigntyLevel", "onliningRequiresSovereigntyLevel");
        AttributeNameStringMap.put("operationalDuration", "operationalDuration");
        AttributeNameStringMap.put("optimalSigRadius", "optimalSigRadius");
        AttributeNameStringMap.put("overloadArmorDamageAmount", "overloadArmorDamageAmount");
        AttributeNameStringMap.put("overloadBonusMultiplier", "overloadBonusMultiplier");
        AttributeNameStringMap.put("overloadDamageModifier", "overloadDamageModifier");
        AttributeNameStringMap.put("overloadECCMStrenghtBonus", "overloadECCMStrenghtBonus");
        AttributeNameStringMap.put("overloadECMStrengthBonus", "overloadECMStrengthBonus");
        AttributeNameStringMap.put("overloadHardeningBonus", "overloadHardeningBonus");
        AttributeNameStringMap.put("overloadPainterStrengthBonus", "overloadPainterStrengthBonus");
        AttributeNameStringMap.put("overloadRangeBonus", "overloadRangeBonus");
        AttributeNameStringMap.put("overloadRofBonus", "overloadRofBonus");
        AttributeNameStringMap.put("overloadSelfDurationBonus", "overloadSelfDurationBonus");
        AttributeNameStringMap.put("overloadSensorModuleStrengthBonus", "overloadSensorModuleStrengthBonus");
        AttributeNameStringMap.put("overloadShieldBonus", "overloadShieldBonus");
        AttributeNameStringMap.put("overloadSpeedFactorBonus", "overloadSpeedFactorBonus");
        AttributeNameStringMap.put("overloadTrackingModuleStrengthBonus", "overloadTrackingModuleStrengthBonus");
        AttributeNameStringMap.put("passiveArmorEmDamageResonance", "passiveArmorEmDamageResonance");
        AttributeNameStringMap.put("passiveArmorExplosiveDamageResonance", "passiveArmorExplosiveDamageResonance");
        AttributeNameStringMap.put("passiveArmorKineticDamageResonance", "passiveArmorKineticDamageResonance");
        AttributeNameStringMap.put("passiveArmorThermalDamageResonance", "passiveArmorThermalDamageResonance");
        AttributeNameStringMap.put("passiveShieldEmDamageResonance", "passiveShieldEmDamageResonance");
        AttributeNameStringMap.put("passiveShieldExplosiveDamageResonance", "passiveShieldExplosiveDamageResonance");
        AttributeNameStringMap.put("passiveShieldKineticDamageResonance", "passiveShieldKineticDamageResonance");
        AttributeNameStringMap.put("passiveShieldThermalDamageResonance", "passiveShieldThermalDamageResonance");
        AttributeNameStringMap.put("perceptionBonus", "Perception Modifier");
        AttributeNameStringMap.put("piTaxReductionModifer", "piTaxReductionModifer");
        AttributeNameStringMap.put("pinCycleTime", "pinCycleTime");
        AttributeNameStringMap.put("pinExtractionQuantity", "pinExtractionQuantity");
        AttributeNameStringMap.put("planetRestriction", "planetRestriction");
        AttributeNameStringMap.put("posAnchoredPerSolarSystemAmount", "posAnchoredPerSolarSystemAmount");
        AttributeNameStringMap.put("posCargobayAcceptType", "posCargobayAcceptType");
        AttributeNameStringMap.put("posControlTowerPeriod", "posControlTowerPeriod");
        AttributeNameStringMap.put("posPlayerControlStructure", "posPlayerControlStructure");
        AttributeNameStringMap.put("posStructureControlAmount", "posStructureControlAmount");
        AttributeNameStringMap.put("posStructureControlDistanceMax", "posStructureControlDistanceMax");
        AttributeNameStringMap.put("power", "power");
        AttributeNameStringMap.put("powerEngineeringOutputBonus", "powerEngineeringOutputBonus");
        AttributeNameStringMap.put("powerIncrease", "powerIncrease");
        AttributeNameStringMap.put("powerLoad", "powerLoad");
        AttributeNameStringMap.put("powerLoadLevelModifier", "powerLoadLevelModifier");
        AttributeNameStringMap.put("powerLoadPerKm", "powerLoadPerKm");
        AttributeNameStringMap.put("powerNeedBonus", "powerNeedBonus");
        AttributeNameStringMap.put("powerNeedMultiplier", "powerNeedMultiplier");
        AttributeNameStringMap.put("powerOutput", "Powergrid Output");
        AttributeNameStringMap.put("powerOutputMultiplier", "powerOutputMultiplier");
        AttributeNameStringMap.put("powerTransferAmount", "powerTransferAmount");
        AttributeNameStringMap.put("powerTransferAmountBonus", "powerTransferAmountBonus");
        AttributeNameStringMap.put("powerTransferDurationBonus", "powerTransferDurationBonus");
        AttributeNameStringMap.put("powerTransferRange", "powerTransferRange");
        AttributeNameStringMap.put("primaryAttribute", "Primary attribute");
        AttributeNameStringMap.put("projECMDurationBonus", "projECMDurationBonus");
        AttributeNameStringMap.put("proximityRange", "proximityRange");
        AttributeNameStringMap.put("rangeFactor", "rangeFactor");
        AttributeNameStringMap.put("rangeSkillBonus", "rangeSkillBonus");
        AttributeNameStringMap.put("reactionGroup1", "reactionGroup1");
        AttributeNameStringMap.put("reactionGroup2", "reactionGroup2");
        AttributeNameStringMap.put("rechargeRate", "Capacitor Recharge time");
        AttributeNameStringMap.put("rechargeRateMultiplier", "rechargeRateMultiplier");
        AttributeNameStringMap.put("rechargeratebonus", "rechargeratebonus");
        AttributeNameStringMap.put("refiningYieldMultiplier", "refiningYieldMultiplier");
        AttributeNameStringMap.put("refiningYieldMutator", "refiningYieldMutator");
        AttributeNameStringMap.put("reloadTime", "reloadTime");
        AttributeNameStringMap.put("remoteArmorDamageAmountBonus", "remoteArmorDamageAmountBonus");
        AttributeNameStringMap.put("remoteArmorDamageDurationBonus", "remoteArmorDamageDurationBonus");
        AttributeNameStringMap.put("remoteHullDamageAmountBonus", "remoteHullDamageAmountBonus");
        AttributeNameStringMap.put("remoteHullDamageDurationBonus", "remoteHullDamageDurationBonus");
        AttributeNameStringMap.put("repairBonus", "repairBonus");
        AttributeNameStringMap.put("reprocessingSkillType", "reprocessingSkillType");
        AttributeNameStringMap.put("requiredSkill1", "requiredSkill1");
        AttributeNameStringMap.put("requiredSkill2", "requiredSkill2");
        AttributeNameStringMap.put("requiredSkill3", "requiredSkill3");
        AttributeNameStringMap.put("requiredSkill4", "requiredSkill4");
        AttributeNameStringMap.put("requiredSkill5", "requiredSkill5");
        AttributeNameStringMap.put("requiredSkill6", "requiredSkill6");
        AttributeNameStringMap.put("requiredThermoDynamicsSkill", "requiredThermoDynamicsSkill");
        AttributeNameStringMap.put("requiresSovereigntyDisplayOnly", "requiresSovereigntyDisplayOnly");
        AttributeNameStringMap.put("resistanceBonus", "resistanceBonus");
        AttributeNameStringMap.put("resistanceKiller", "resistanceKiller");
        AttributeNameStringMap.put("rigDrawbackBonus", "rigDrawbackBonus");
        AttributeNameStringMap.put("rigSize", "Rig Size");
        AttributeNameStringMap.put("rofBonus", "rofBonus");
        AttributeNameStringMap.put("scanDurationBonus", "scanDurationBonus");
        AttributeNameStringMap.put("scanGravimetricStrength", "Gravimetric Sensor Strength");
        AttributeNameStringMap.put("scanGravimetricStrengthBonus", "scanGravimetricStrengthBonus");
        AttributeNameStringMap.put("scanGravimetricStrengthModifier", "scanGravimetricStrengthModifier");
        AttributeNameStringMap.put("scanGravimetricStrengthPercent", "scanGravimetricStrengthPercent");
        AttributeNameStringMap.put("scanLadarStrength", "scanLadarStrength");
        AttributeNameStringMap.put("scanLadarStrengthBonus", "scanLadarStrengthBonus");
        AttributeNameStringMap.put("scanLadarStrengthModifier", "scanLadarStrengthModifier");
        AttributeNameStringMap.put("scanLadarStrengthPercent", "scanLadarStrengthPercent");
        AttributeNameStringMap.put("scanMagnetometricStrength", "scanMagnetometricStrength");
        AttributeNameStringMap.put("scanMagnetometricStrengthBonus", "scanMagnetometricStrengthBonus");
        AttributeNameStringMap.put("scanMagnetometricStrengthModifier", "scanMagnetometricStrengthModifier");
        AttributeNameStringMap.put("scanMagnetometricStrengthPercent", "scanMagnetometricStrengthPercent");
        AttributeNameStringMap.put("scanRadarStrength", "scanRadarStrength");
        AttributeNameStringMap.put("scanRadarStrengthBonus", "scanRadarStrengthBonus");
        AttributeNameStringMap.put("scanRadarStrengthModifier", "scanRadarStrengthModifier");
        AttributeNameStringMap.put("scanRadarStrengthPercent", "scanRadarStrengthPercent");
        AttributeNameStringMap.put("scanResolution", "Scan Resolution");
        AttributeNameStringMap.put("scanResolutionBonus", "scanResolutionBonus");
        AttributeNameStringMap.put("scanResolutionBonusBonus", "scanResolutionBonusBonus");
        AttributeNameStringMap.put("scanResolutionMultiplier", "scanResolutionMultiplier");
        AttributeNameStringMap.put("scanSkillEwStrengthBonus", "scanSkillEwStrengthBonus");
        AttributeNameStringMap.put("scanSkillTargetPaintStrengthBonus", "scanSkillTargetPaintStrengthBonus");
        AttributeNameStringMap.put("scanStrengthBonus", "scanStrengthBonus");
        AttributeNameStringMap.put("scanStrengthBonusModule", "scanStrengthBonusModule");
        AttributeNameStringMap.put("scanspeedBonus", "scanspeedBonus");
        AttributeNameStringMap.put("secondaryAttribute", "Secondary attribute");
        AttributeNameStringMap.put("securityProcessingFee", "securityProcessingFee");
        AttributeNameStringMap.put("sensorStrengthBonus", "sensorStrengthBonus");
        AttributeNameStringMap.put("shieldBonus", "shieldBonus");
        AttributeNameStringMap.put("shieldBonusDurationBonus", "shieldBonusDurationBonus");
        AttributeNameStringMap.put("shieldBonusMultiplier", "shieldBonusMultiplier");
        AttributeNameStringMap.put("shieldBonusMultiplierRemote", "shieldBonusMultiplierRemote");
        AttributeNameStringMap.put("shieldBoostCapacitorBonus", "shieldBoostCapacitorBonus");
        AttributeNameStringMap.put("shieldBoostMultiplier", "shieldBoostMultiplier");
        AttributeNameStringMap.put("shieldCapacity", "Shield Capacity");
        AttributeNameStringMap.put("shieldCapacityBonus", "shieldCapacityBonus");
        AttributeNameStringMap.put("shieldCapacityMultiplier", "shieldCapacityMultiplier");
        AttributeNameStringMap.put("shieldEmDamageResistanceBonus", "shieldEmDamageResistanceBonus");
        AttributeNameStringMap.put("shieldEmDamageResonance", "shieldEmDamageResonance");
        AttributeNameStringMap.put("shieldExplosiveDamageResistanceBonus", "shieldExplosiveDamageResistanceBonus");
        AttributeNameStringMap.put("shieldExplosiveDamageResonance", "shieldExplosiveDamageResonance");
        AttributeNameStringMap.put("shieldKineticDamageResistanceBonus", "shieldKineticDamageResistanceBonus");
        AttributeNameStringMap.put("shieldKineticDamageResonance", "shieldKineticDamageResonance");
        AttributeNameStringMap.put("shieldRadius", "shieldRadius");
        AttributeNameStringMap.put("shieldRechargeRate", "Shield recharge time");
        AttributeNameStringMap.put("shieldRechargeRateMultiplier", "shieldRechargeRateMultiplier");
        AttributeNameStringMap.put("shieldThermalDamageResistanceBonus", "shieldThermalDamageResistanceBonus");
        AttributeNameStringMap.put("shieldThermalDamageResonance", "shieldThermalDamageResonance");
        AttributeNameStringMap.put("shieldTransferRange", "shieldTransferRange");
        AttributeNameStringMap.put("shieldTransportAmountBonus", "shieldTransportAmountBonus");
        AttributeNameStringMap.put("shieldTransportDurationBonus", "shieldTransportDurationBonus");
        AttributeNameStringMap.put("shipBrokenRepairCostMultiplierBonus", "shipBrokenRepairCostMultiplierBonus");
        AttributeNameStringMap.put("shipMaintenanceBayCapacity", "shipMaintenanceBayCapacity");
        AttributeNameStringMap.put("shipScanFalloff", "shipScanFalloff");
        AttributeNameStringMap.put("shipScanRange", "shipScanRange");
        AttributeNameStringMap.put("signatureRadius", "Signature Radius");
        AttributeNameStringMap.put("signatureRadiusAdd", "signatureRadiusAdd");
        AttributeNameStringMap.put("signatureRadiusBonus", "signatureRadiusBonus");
        AttributeNameStringMap.put("signatureRadiusBonusBonus", "signatureRadiusBonusBonus");
        AttributeNameStringMap.put("signatureRadiusBonusPercent", "signatureRadiusBonusPercent");
        AttributeNameStringMap.put("signatureRadiusMultiplier", "signatureRadiusMultiplier");
        AttributeNameStringMap.put("siphonPolyMaterial", "siphonPolyMaterial");
        AttributeNameStringMap.put("siphonProMaterial", "siphonProMaterial");
        AttributeNameStringMap.put("siphonRawMaterial", "siphonRawMaterial");
        AttributeNameStringMap.put("siphonWasteAmount", "siphonWasteAmount");
        AttributeNameStringMap.put("skillAllyCostModifierBonus", "skillAllyCostModifierBonus");
        AttributeNameStringMap.put("skillLevel", "skillLevel");
        AttributeNameStringMap.put("skillTimeConstant", "Training time multiplier");
        AttributeNameStringMap.put("smallWeaponDamageMultiplier", "smallWeaponDamageMultiplier");
        AttributeNameStringMap.put("smartbombDamageMultiplier", "smartbombDamageMultiplier");
        AttributeNameStringMap.put("socialMutator", "socialMutator");
        AttributeNameStringMap.put("sovBillSystemCost", "sovBillSystemCost");
        AttributeNameStringMap.put("sovUpgradeRequiredUpgradeID", "sovUpgradeRequiredUpgradeID");
        AttributeNameStringMap.put("specialAmmoHoldCapacity", "specialAmmoHoldCapacity");
        AttributeNameStringMap.put("specialCommandCenterHoldCapacity", "specialCommandCenterHoldCapacity");
        AttributeNameStringMap.put("specialFuelBayCapacity", "specialFuelBayCapacity");
        AttributeNameStringMap.put("specialMaterialBayCapacity", "specialMaterialBayCapacity");
        AttributeNameStringMap.put("specialMineralHoldCapacity", "specialMineralHoldCapacity");
        AttributeNameStringMap.put("specialOreHoldCapacity", "specialOreHoldCapacity");
        AttributeNameStringMap.put("specialPlanetaryCommoditiesHoldCapacity", "specialPlanetaryCommoditiesHoldCapacity");
        AttributeNameStringMap.put("specialisationAsteroidGroup", "specialisationAsteroidGroup");
        AttributeNameStringMap.put("specialisationAsteroidYieldMultiplier", "specialisationAsteroidYieldMultiplier");
        AttributeNameStringMap.put("specialtyMiningAmount", "specialtyMiningAmount");
        AttributeNameStringMap.put("speed", "speed");
        AttributeNameStringMap.put("speedBonus", "speedBonus");
        AttributeNameStringMap.put("speedBoostFactor", "speedBoostFactor");
        AttributeNameStringMap.put("speedBoostFactorBonus", "speedBoostFactorBonus");
        AttributeNameStringMap.put("speedBoostFactorBonusBonus", "speedBoostFactorBonusBonus");
        AttributeNameStringMap.put("speedFBonus", "speedFBonus");
        AttributeNameStringMap.put("speedFactor", "speedFactor");
        AttributeNameStringMap.put("speedFactorBonus", "speedFactorBonus");
        AttributeNameStringMap.put("speedFactorBonusBonus", "speedFactorBonusBonus");
        AttributeNameStringMap.put("speedMultiplier", "speedMultiplier");
        AttributeNameStringMap.put("squadronCommandBonus", "squadronCommandBonus");
        AttributeNameStringMap.put("stanceSwitchTime", "stanceSwitchTime");
        AttributeNameStringMap.put("stasisWebStrengthMultiplier", "stasisWebStrengthMultiplier");
        AttributeNameStringMap.put("stationOreRefiningBonus", "stationOreRefiningBonus");
        AttributeNameStringMap.put("stationTypeID", "stationTypeID");
        AttributeNameStringMap.put("structureDamageAmount", "structureDamageAmount");
        AttributeNameStringMap.put("structureHPMultiplier", "structureHPMultiplier");
        AttributeNameStringMap.put("surveyScanRange", "surveyScanRange");
        AttributeNameStringMap.put("systemEffectDamageReduction", "systemEffectDamageReduction");
        AttributeNameStringMap.put("targetHostileRange", "targetHostileRange");
        AttributeNameStringMap.put("targetPainterStrengthMultiplier", "targetPainterStrengthMultiplier");
        AttributeNameStringMap.put("techLevel", "Tech Level");
        AttributeNameStringMap.put("thermalDamage", "thermalDamage");
        AttributeNameStringMap.put("thermalDamageResistanceBonus", "thermalDamageResistanceBonus");
        AttributeNameStringMap.put("thermalDamageResonance", "thermalDamageResonance");
        AttributeNameStringMap.put("thermalDamageResonanceMultiplier", "thermalDamageResonanceMultiplier");
        AttributeNameStringMap.put("thermodynamicsHeatDamage", "thermodynamicsHeatDamage");
        AttributeNameStringMap.put("trackingSpeed", "trackingSpeed");
        AttributeNameStringMap.put("trackingSpeedBonus", "trackingSpeedBonus");
        AttributeNameStringMap.put("trackingSpeedBonusBonus", "trackingSpeedBonusBonus");
        AttributeNameStringMap.put("trackingSpeedMultiplier", "trackingSpeedMultiplier");
        AttributeNameStringMap.put("triageRemoteModuleCapNeed", "triageRemoteModuleCapNeed");
        AttributeNameStringMap.put("turretHardPointModifier", "turretHardPointModifier");
        AttributeNameStringMap.put("turretSlotsLeft", "turretSlotsLeft");
        AttributeNameStringMap.put("turretSpeeBonus", "turretSpeeBonus");
        AttributeNameStringMap.put("unanchoringDelay", "unanchoringDelay");
        AttributeNameStringMap.put("unfitCapCost", "unfitCapCost");
        AttributeNameStringMap.put("uniformityBonus", "Uniformity Bonus");
        AttributeNameStringMap.put("upgradeCapacity", "Calibration");
        AttributeNameStringMap.put("upgradeCost", "upgradeCost");
        AttributeNameStringMap.put("upgradeSlotsLeft", "Rig Slots");
        AttributeNameStringMap.put("velocityBonus", "velocityBonus");
        AttributeNameStringMap.put("velocityBonus2", "velocityBonus2");
        AttributeNameStringMap.put("virusCoherence", "virusCoherence");
        AttributeNameStringMap.put("virusCoherenceBonus", "virusCoherenceBonus");
        AttributeNameStringMap.put("virusElementSlots", "virusElementSlots");
        AttributeNameStringMap.put("virusStrength", "virusStrength");
        AttributeNameStringMap.put("virusStrengthBonus", "virusStrengthBonus");
        AttributeNameStringMap.put("warfareLinkCPUAdd", "warfareLinkCPUAdd");
        AttributeNameStringMap.put("warpCapacitorNeedBonus", "warpCapacitorNeedBonus");
        AttributeNameStringMap.put("warpScrambleDuration", "warpScrambleDuration");
        AttributeNameStringMap.put("warpScrambleRange", "warpScrambleRange");
        AttributeNameStringMap.put("warpScrambleRangeBonus", "warpScrambleRangeBonus");
        AttributeNameStringMap.put("warpScrambleStatus", "warpScrambleStatus");
        AttributeNameStringMap.put("warpScrambleStrength", "warpScrambleStrength");
        AttributeNameStringMap.put("warpSpeedAdd", "warpSpeedAdd");
        AttributeNameStringMap.put("warpSpeedMultiplier", "Ship Warp Speed");
        AttributeNameStringMap.put("weaponRangeMultiplier", "weaponRangeMultiplier");
        AttributeNameStringMap.put("webSpeedFactorBonus", "webSpeedFactorBonus");
        AttributeNameStringMap.put("willpowerBonus", "Willpower Modifier");
    }

    public long id;

    public String name;

    public double value;

    public String displayUnit;

    public String iconFile;

    public SdeAttribute() {

    }

    public SdeAttribute(long id, String name, double value, String displayUnit, String iconFile) {
        this.id = id;
        this.name = name;
        this.value = value;
        this.displayUnit = displayUnit;
        this.iconFile = iconFile;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public String getIconFile() {
        return iconFile;
    }

    public void setIconFile(String iconFile) {
        this.iconFile = iconFile;
    }

    @JsonIgnore
    public String getFormattedValue() {
        double displayValue = getConvertedValueForDisplayUnit(value, displayUnit);
        String actualDisplayUnit = getConvertedDisplayUnit(name, value, displayUnit);

        String valueStr = String.valueOf(displayValue);
        int intValue = Double.valueOf(displayValue).intValue();
        if (valueStr.indexOf(".") > -1 && valueStr.substring(valueStr.indexOf(".") + 1).equals("0")) {
            valueStr = NumberFormat.getInstance().format(intValue);
        } else {
            valueStr = Helper.formatDecimal(displayValue);
        }

        if (("primaryAttribute".equals(name) || "secondaryAttribute".equals(name))
                && CharacterAttributesMap.containsKey(Double.valueOf(value).intValue())) {
            return CharacterAttributesMap.get(intValue);
        }

        if (displayUnit == null) {
            return valueStr;
        }

        if (displayUnit.contains("=")) {
            String[] unitParts = displayUnit.split(" ");
            for (int i = 0; i < unitParts.length; i++) {
                String[] subParts = unitParts[i].split("=");
                if (subParts.length > 1
                        && subParts[0].equals(String.valueOf(intValue))) {
                    actualDisplayUnit = "";
                    valueStr = subParts[1];
                }
            }
        }

        return String.format("%s %s",
                (PrefixUnits.contains(actualDisplayUnit)) ? actualDisplayUnit : valueStr,
                (PrefixUnits.contains(actualDisplayUnit)) ? valueStr : actualDisplayUnit).trim();
    }

    private static double getConvertedValueForDisplayUnit(double value, String displayUnit) {
        if ("s".equals(displayUnit) // Seconds (values stored as milliseconds
            || "m".equals(displayUnit) && value > 1000) {
            return value / 1000;
        }

        return value;
    }

    private static String getConvertedDisplayUnit(String attributeName, double value, String displayUnit) {
        if ("maxTargetRange".equals(attributeName) && value > 1000) {
            return "km"; // Kilometres
        }

        if ("warpSpeedMultiplier".equals(attributeName)) {
            return "AU/s";
        }

        return displayUnit;
    }

    public String getDisplayUnit() {
        return displayUnit;
    }

    public void setDisplayUnit(String displayUnit) {
        this.displayUnit = displayUnit;
    }
}
