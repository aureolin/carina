/**
 * Project Carina - EVE Fitting Engine Android Library
 *
 * Copyright (c) 2015 Aureolin
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package co.aureolin.labs.carina.model.fitting;

import android.util.Log;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import co.aureolin.labs.carina.fitting.Effect;
import co.aureolin.labs.carina.model.sde.SdeType;
import co.aureolin.labs.carina.util.FittingHelper;

/**
 * Created by Akinwale on 11/10/2015.
 */
public class FittingModel extends SdeType {
    private static final String TAG = FittingModel.class.getName();

    public static final int TYPE_SHIP = 1;

    public static final int TYPE_MODULE = 2;

    public static final int TYPE_RIG = 3;

    public static final int TYPE_SUBSYSTEM = 4;

    public static final int TYPE_DRONE = 5;

    public static final int TYPE_CHARGE = 6;

    public static final int TYPE_IMPLANT = 7;

    private static final Integer[] overloadEffectIds =
        FittingHelper.moduleOverloadEffectsMap.keySet().toArray(
            new Integer[FittingHelper.moduleOverloadEffectsMap.keySet().size()]);

    private int fittingItemType; // Char, Ship, Module?

    private Map<Long, Effect> effects;

    private Map<Long, FittingAttribute> attributes;

    private FittingCharacter currentCharacter;

    private FittingModel currentShip; // for everything else that is not a ship

    private List<Long> affectingSkillIds;

    private long uniqueId; // A unique identifier which will be different from the item ID

    private long groupId;

    public Map<Long, FittingAttribute> getAttributes() {
        return attributes;
    }

    public void setAttributes(Map<Long, FittingAttribute> attributes) {
        this.attributes = attributes;
    }

    public FittingModel getCurrentShip() {
        return currentShip;
    }

    public void setCurrentShip(FittingModel currentShip) {
        this.currentShip = currentShip;
    }

    public int getFittingItemType() {
        return fittingItemType;
    }

    public void setLocation(int fittingItemType) {
        this.fittingItemType = fittingItemType;
    }

    public long getGroupId() {
        return groupId;
    }

    public void setGroupId(long groupId) {
        this.groupId = groupId;
    }

    protected FittingModel() {
        attributes = new HashMap<Long, FittingAttribute>();
        effects = new HashMap<Long, Effect>();
        affectingSkillIds = new ArrayList<Long>();
    }

    public boolean isAffectedBySkill(long skillId) {
        return affectingSkillIds.contains(skillId);
    }

    public long getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(long uniqueId) {
        this.uniqueId = uniqueId;
    }

    public FittingCharacter getCurrentCharacter() {
        return currentCharacter;
    }

    public void setCurrentCharacter(FittingCharacter currentCharacter) {
        this.currentCharacter = currentCharacter;
    }

    public Map<Long, Effect> getEffects() {
        return effects;
    }

    public void setEffects(Map<Long, Effect> effects) {
        this.effects = effects;
    }

    public List<Long> getAffectingSkillIds() {
        return affectingSkillIds;
    }

    public void setAffectingSkillIds(List<Long> affectingSkillIds) {
        this.affectingSkillIds = affectingSkillIds;
    }

    @JsonIgnore
    public boolean canBeOnline() {
        return hasAttribute("duration") || hasAttribute("chargeGroup1");
    }

    @JsonIgnore
    public boolean canOverload() {
        for (int i = 0; i < overloadEffectIds.length; i++) {
            if (effects.containsKey(overloadEffectIds[i].longValue())) {
                return true;
            }
        }

        return false;
    }

    @JsonIgnore
    public boolean hasAttribute(long attributeId) {
        return attributes.containsKey(attributeId);
    }

    @JsonIgnore
    public boolean hasAttribute(String attributeName) {
        return FittingHelper.fittingAttributesMap.containsValue(attributeName)
            && hasAttribute(FittingHelper.fittingAttributesMap.inverse().get(attributeName).longValue());
    }

    @JsonIgnore
    public void setAttributes(List<FittingAttribute> attributeList) {
        for (int i = 0; i < attributeList.size(); i++) {
            FittingAttribute attribute = attributeList.get(i);
            if (attribute != null) {
                attributes.put(attribute.getId(), attribute);
            }
        }
    }

    @JsonIgnore
    public double getAttributeValue(long attributeId) {
        if (attributes.containsKey(attributeId)) {
            return attributes.get(attributeId).getValue();
        }

        return 0;
    }

    @JsonIgnore
    public double getAttributeValueByName(String attributeName) {
        for (FittingAttribute attribute : attributes.values()) {
            if (attributeName.equals(attribute.getName())) {
                return attribute.getValue();
            }
        }

        return 0;
    }

    @JsonIgnore
    public double getDisplayAttributeValue(long attributeId) {
        if (attributes.containsKey(attributeId)) {
            return attributes.get(attributeId).getDisplayValue();
        }

        return 0;
    }

    @JsonIgnore
    public double getDisplayAttributeValueByName(String attributeName) {
        for (FittingAttribute attribute : attributes.values()) {
            if (attributeName.equals(attribute.getName())) {
                return attribute.getDisplayValue();
            }
        }

        return 0;
    }

    @JsonIgnore
    public double getAffectedValue(long attributeId) {
        if (attributes.containsKey(attributeId)) {
            FittingAttribute attribute = attributes.get(attributeId);
            return attribute.getAffectedValue();
        }

        return 0;
    }

    @JsonIgnore
    public double getAffectedValueByName(String attributeName) {
        for (FittingAttribute attribute : attributes.values()) {
            if (attributeName.equals(attribute.getName())) {
                return attribute.getAffectedValue();
            }
        }

        return 0;
    }

    @JsonIgnore
    public double getDisplayAffectedValue(long attributeId) {
        if (attributes.containsKey(attributeId)) {
            FittingAttribute attribute = attributes.get(attributeId);
            return attribute.getDisplayAffectedValue();
        }

        return 0;
    }

    @JsonIgnore
    public double getDisplayAffectedValueByName(String attributeName) {
        for (FittingAttribute attribute : attributes.values()) {
            if (attributeName.equals(attribute.getName())) {
                return attribute.getDisplayAffectedValue();
            }
        }

        return 0;
    }

    @JsonIgnore
    public void setAffectedValue(long attributeId, double value) {
        if (attributes.containsKey(attributeId)) {
            Log.d(TAG, String.format("Setting affected value of attribute '%s' to %.2f, ActualValue = %.2f",
                    FittingHelper.fittingAttributesMap.get(Long.valueOf(attributeId).intValue()),
                    value, getAttributeValue(attributeId)));

            attributes.get(attributeId).setAffectedValue(value);
        }
    }

    @JsonIgnore
    public void setAffectedValue(String attributeName, double value) {
        long attributeId = FittingHelper.fittingAttributesMap.inverse().get(attributeName);
        setAffectedValue(attributeId, value);
    }

    @JsonIgnore
    public void setValue(String attributeName, double value) {
        long attributeId = FittingHelper.fittingAttributesMap.inverse().get(attributeName);
        setValue(attributeId, value);
    }

    @JsonIgnore
    public void insertValue(String attributeName, double value) {
        long attributeId = FittingHelper.fittingAttributesMap.inverse().get(attributeName);
        insertValue(attributeId, value);
    }

    @JsonIgnore
    public void insertValue(long attributeId, double value) {
        if (hasAttribute(attributeId)) {
            setValue(attributeId, value);
            return;
        }


        FittingAttribute attribute = new FittingAttribute();
        attribute.setId(attributeId);
        attribute.setName(FittingHelper.fittingAttributesMap.get(Long.valueOf(attributeId).intValue()));
        attribute.setValue(value);
        attribute.setAffectedValue(value);
        getAttributes().put(attributeId, attribute);
    }

    @JsonIgnore
    public void setValue(long attributeId, double value) {
        if (attributes.containsKey(attributeId)) {
            attributes.get(attributeId).setValue(value);
        }
        setAffectedValue(attributeId, value);
    }

    @Override
    public boolean equals(Object o) {
        if (o == null || !(o instanceof FittingModel)) {
            return false;
        }
        return uniqueId == ((FittingModel) o).getUniqueId();
    }

    @Override
    public int hashCode() {
        return Long.valueOf(uniqueId).hashCode();
    }
}
