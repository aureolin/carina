/**
 * Project Carina - EVE Fitting Engine Android Library
 *
 * Copyright (c) 2015 Aureolin
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package co.aureolin.labs.carina.model.fitting;

import java.util.HashMap;
import java.util.Map;

import co.aureolin.labs.carina.fitting.FittingTool;
import co.aureolin.labs.carina.model.Skill;

/**
 * Created by Akinwale on 12/10/2015.
 */
public class FittingCharacter extends FittingModel {
    private Map<Long, FittingSkill> skills;

    public FittingCharacter() {
        super();
        skills = new HashMap<Long, FittingSkill>();
    }

    public Map<Long, FittingSkill> getSkills() {
        return skills;
    }

    public void setSkills(Map<Long, FittingSkill> skills) {
        this.skills = skills;
    }

    public int getLevelForSkill(String skillName) {
        for (FittingSkill skill : skills.values()) {
            if (skillName.equals(skill.getName())) {
                return skill.getLevel();
            }
        }
        return 0;
    }

    public static FittingCharacter fromCharacterModel(
            co.aureolin.labs.carina.model.Character character) {
        FittingCharacter fitCharacter = new FittingCharacter();
        if (character != null ) {
            fitCharacter.setId(character.getId());
            fitCharacter.setName(character.getName());

            if (character.getSkills() != null) {
                Map<Long, FittingSkill> skillMap = new HashMap<Long, FittingSkill>();
                for (Skill skill : character.getSkills()) {
                    FittingSkill fittingSkill = new FittingSkill();
                    fittingSkill.setId(skill.getId());
                    fittingSkill.setName(skill.getName());
                    fittingSkill.setLevel(skill.getLevel());
                    skillMap.put(fittingSkill.getId(), fittingSkill);
                }
                fitCharacter.setSkills(skillMap);
            }
        }

        return fitCharacter;
    }
}
