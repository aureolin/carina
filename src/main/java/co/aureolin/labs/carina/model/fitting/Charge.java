package co.aureolin.labs.carina.model.fitting;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Akinwale on 18/12/2015.
 */
public class Charge extends FittingModel {
    private Module module;

    public Charge() {

    }

    @JsonIgnore
    public Module getModule() {
        return module;
    }

    @JsonIgnore
    public void setModule(Module module) {
        this.module = module;
        if (module != null && module.getCurrentShip() != null) {
            setCurrentShip(module.getCurrentShip());
        }
    }
}
