/**
 * Project Carina - EVE Fitting Engine Android Library
 *
 * Copyright (c) 2015 Aureolin
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package co.aureolin.labs.carina.model.fitting;

import android.util.Log;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import co.aureolin.labs.carina.fitting.Dogma;
import co.aureolin.labs.carina.fitting.Effect;

/**
 * Created by Akinwale on 11/10/2015.
 */
public class Module extends FittingModel {
    private static final String[] ChargeGroupAttributeNames =
        {"chargeGroup1", "chargeGroup2", "chargeGroup3", "chargeGroup4"};

    public static Map<Long, Integer> MaxFittedGroups = new HashMap<Long, Integer>();
    static {
        MaxFittedGroups.put(60L, 1);
        MaxFittedGroups.put(481L, 1);
        MaxFittedGroups.put(515L, 1);
        MaxFittedGroups.put(588L, 1);
        MaxFittedGroups.put(589L, 1);
        MaxFittedGroups.put(815L, 1);
        MaxFittedGroups.put(842L, 1);
        MaxFittedGroups.put(862L, 1);
        MaxFittedGroups.put(1150L, 1);
        MaxFittedGroups.put(1154L, 1);
        MaxFittedGroups.put(1189L, 1);
        MaxFittedGroups.put(1199L, 1);
        MaxFittedGroups.put(1226L, 1);
        MaxFittedGroups.put(1238L, 1);
        MaxFittedGroups.put(1289L, 3);
        MaxFittedGroups.put(1308L, 1);
        MaxFittedGroups.put(1313L, 1);
        MaxFittedGroups.put(1533L, 1);
    }

    public Module() {
        super();
    }

    private int state;

    private Charge currentCharge;

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    @JsonIgnore
    public int getSlotType() {
        if (canFitLauncherSlot()) {
            return Dogma.ModuleSlot.Launcher;
        }
        if (canFitTurretSlot()) {
            return Dogma.ModuleSlot.Turret;
        }
        if (canFitHighSlot()) {
            return Dogma.ModuleSlot.High;
        }
        if (canFitMediumSlot()) {
            return Dogma.ModuleSlot.Medium;
        }
        if (canFitLowSlot()) {
            return Dogma.ModuleSlot.Low;
        }
        if (canFitRigSlot()) {
            return Dogma.ModuleSlot.Rig;
        }
        if (canFitSubsystemSlot()) {
            return Dogma.ModuleSlot.Subsystem;
        }
        return -1;
    }

    public Charge getCurrentCharge() {
        return currentCharge;
    }

    public void setCurrentCharge(Charge currentCharge) {
        this.currentCharge = currentCharge;
        if (this.currentCharge != null) {
            this.currentCharge.setModule(this);
        }
    }

    @JsonIgnore
    public boolean hasMaxFitting() {
        return MaxFittedGroups.containsKey(getGroupId());
    }

    @JsonIgnore
    public boolean canUseAmmo() {
        return hasAttribute("chargeGroup1")
            || hasAttribute("chargeGroup2")
            || hasAttribute("chargeGroup3")
            || hasAttribute("chargeGroup4");
    }

    @JsonIgnore
    public boolean isSubsystem() {
        return hasAttribute("subSystemSlot");
    }

    @JsonIgnore
    public int getChargeSize() {
        return hasAttribute("chargeSize") ?
            Double.valueOf(getAttributeValueByName("chargeSize")).intValue() : 0;
    }

    @JsonIgnore
    public List<Long> getChargeGroupIds() {
        List<Long> chargeGroupIds = new ArrayList<Long>();
        for (int i = 0; i < ChargeGroupAttributeNames.length; i++) {
            String attributeName = ChargeGroupAttributeNames[i];
            if (hasAttribute(attributeName)) {
                chargeGroupIds.add(Double.valueOf(getAttributeValueByName(attributeName)).longValue());
            }
        }
        return chargeGroupIds;
    }

    @JsonIgnore
    public boolean canFitHighSlot() {
        return meetsFittingRequirement("hiPower");
    }

    @JsonIgnore
    public boolean canFitMediumSlot() {
        return meetsFittingRequirement("medPower");
    }

    @JsonIgnore
    public boolean canFitLowSlot() {
        return meetsFittingRequirement("loPower");
    }

    @JsonIgnore
    public boolean canFitLauncherSlot() {
        return meetsFittingRequirement("launcherFitted");
    }

    @JsonIgnore
    public boolean canFitTurretSlot() {
        return meetsFittingRequirement("turretFitted");
    }

    @JsonIgnore
    public boolean canFitRigSlot() {
        return meetsFittingRequirement("rigSlot");
    }

    @JsonIgnore
    public boolean canFitSubsystemSlot() {
        return isSubsystem();
    }

    @JsonIgnore
    public int getRigSize() {
        return Double.valueOf(getAttributeValueByName("rigSize")).intValue();
    }

    private boolean meetsFittingRequirement(String effectName) {
        for (Effect effect : getEffects().values()) {
            if (effectName.equals(effect.getName())) {
                return true;
            }
        }
        return false;
    }
}
