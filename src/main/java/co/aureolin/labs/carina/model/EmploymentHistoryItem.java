/**
 * Project Carina - EVE Fitting Engine Android Library
 *
 * Copyright (c) 2015 Aureolin
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package co.aureolin.labs.carina.model;

import android.content.res.Resources;

import java.util.Date;

import co.aureolin.labs.carina.util.Helper;

/**
 * Created by Akinwale on 22/09/2015.
 */
public class EmploymentHistoryItem implements IModel {
    private long id;

    private long corporationId;

    private String corporationName;

    private Date startDate;

    private Date endDate;

    public EmploymentHistoryItem() {

    }

    @Override
    public long getId() {
        return id;
    }

    @Override
    public void setId(long id) {
        this.id = id;
    }

    public long getCorporationId() {
        return corporationId;
    }

    public void setCorporationId(long corporationId) {
        this.corporationId = corporationId;
    }

    public String getCorporationName() {
        return corporationName;
    }

    public void setCorporationName(String corporationName) {
        this.corporationName = corporationName;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String toHtmlString(Resources resources, int thisDayResource, int formatResource) {
        if (resources == null) {
            return "";
        }

        String startDateStr = Helper.HUMAN_DATE_FORMAT.format(startDate);
        String endDateStr = (endDate == null) ? resources.getString(thisDayResource)
                : Helper.HUMAN_DATE_FORMAT.format(endDate);
        String timeDiff = Helper.getReadableTimeDifference(startDate,
            (endDate == null) ? new Date() : endDate);

        return String.format(resources.getString(formatResource),
                startDateStr, endDateStr, timeDiff);
    }

    @Override
    public boolean equals(Object object) {
        if (object == null || !(object instanceof EmploymentHistoryItem)) {
            return false;
        }
        return ((EmploymentHistoryItem) object).getId() == id;
    }
}
