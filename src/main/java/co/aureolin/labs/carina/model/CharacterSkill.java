/**
 * Project Carina - EVE Fitting Engine Android Library
 *
 * Copyright (c) 2015 Aureolin
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package co.aureolin.labs.carina.model;

import android.content.res.Resources;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.text.NumberFormat;

import co.aureolin.labs.carina.util.CarinaSdeHelper;

/**
 * Created by Akinwale on 20/09/2015.
 */
public class CharacterSkill extends Skill {
    private int skillPoints;

    private boolean published;

    public CharacterSkill() {

    }

    public int getSkillPoints() {
        return skillPoints;
    }

    public void setSkillPoints(int skillPoints) {
        this.skillPoints = skillPoints;
    }

    public boolean isPublished() {
        return published;
    }

    public void setPublished(boolean published) {
        this.published = published;
    }

    @JsonIgnore
    public int getSkillPointsToNextLevel() {
        int nextLevel = getLevel() + 1;
        if (nextLevel > 5) {
            return 0;
        }

        int targetSP = 0;
        switch (nextLevel) {
            case 1:
                targetSP = CarinaSdeHelper.BASE_LVL1_POINTS;
                break;
            case 2:
                targetSP = CarinaSdeHelper.BASE_LVL2_POINTS
                        + CarinaSdeHelper.BASE_LVL1_POINTS;
                break;
            case 3:
                targetSP = CarinaSdeHelper.BASE_LVL3_POINTS
                        + CarinaSdeHelper.BASE_LVL2_POINTS
                        + CarinaSdeHelper.BASE_LVL1_POINTS;
                break;
            case 4:
                targetSP = CarinaSdeHelper.BASE_LVL4_POINTS
                    + CarinaSdeHelper.BASE_LVL3_POINTS
                    + CarinaSdeHelper.BASE_LVL2_POINTS
                    + CarinaSdeHelper.BASE_LVL1_POINTS;
                break;
            case 5:
                targetSP = Skill.LVL5_1X_TOTAL_POINTS;
                break;
        }
        targetSP = targetSP * getMultiplier();
        return (targetSP - skillPoints);
    }

    @JsonIgnore
    public String getFullSkillPointsString(Resources resources, int spResource) {
        NumberFormat format = NumberFormat.getInstance();
        return String.format("<strong>%s: %s / %s</strong>",
                resources.getString(spResource),
                format.format(getSkillPoints()), format.format(getTotalSkillPoints()));
    }

}
