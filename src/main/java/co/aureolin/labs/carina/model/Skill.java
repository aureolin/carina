/**
 * Project Carina - EVE Fitting Engine Android Library
 *
 * Copyright (c) 2015 Aureolin
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package co.aureolin.labs.carina.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import co.aureolin.labs.carina.util.Helper;

public class Skill implements Comparable<Skill> {
    /*
     Level V: 256,000
     */
    public static final int LVL5_1X_TOTAL_POINTS = 256000;

    private long id;

    private String name;

    private String group;

    private int level;

    private int multiplier;

    public Skill() {

    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getMultiplier() {
        return multiplier;
    }

    public void setMultiplier(int multiplier) {
        this.multiplier = multiplier;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public String getGroup() {
        if (group == null || group.trim().length() == 0) {
            return "Unknown";
        }

        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    @JsonIgnore
    public int getTotalSkillPoints() {
        if (multiplier > 0) {
            return LVL5_1X_TOTAL_POINTS * multiplier;
        }

        return 0;
    }

    @JsonIgnore
    public String getNameWithLevel() {
        return String.format("%s %s", getName(), Helper.romanNumeralForValue(level));
    }

    @JsonIgnore
    public String getNameWithLevelAndMultiplier() {
        return String.format("%s %s (%dx)", getName(), Helper.romanNumeralForValue(level), multiplier);
    }

    public int compareTo(Skill skill) {
        if (skill == null) {
            return 1;
        }
        if (this.getName() == null && skill.getName() == null) {
            return 0;
        }
        if (this.getName() == null && skill.getName() != null) {
            return -1;
        }

        return this.getName().compareTo(skill.getName());

    }
}
