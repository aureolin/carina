/**
 * Project Carina - EVE Fitting Engine Android Library
 *
 * Copyright (c) 2015 Aureolin
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package co.aureolin.labs.carina.model.fitting;

import android.database.Cursor;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Created by Akinwale on 12/10/2015.
 */
public class FittingAttribute {
    public static final String UNIT_INVERSE_ABSOLUTE_PERCENT = "Inverse Absolute Percent";

    public long id;

    public String name;

    public double value;

    public double affectedValue; // Meant to store the value after all effects have been applied

    private String unitName;

    private String displayUnit;

    public FittingAttribute() {

    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    @JsonIgnore
    public double getDisplayValue() {
        if (UNIT_INVERSE_ABSOLUTE_PERCENT.equals(unitName)) {
            return (1.0 - value) * 100.0;
        }

        return value;
    }

    public double getAffectedValue() {
        return affectedValue;
    }

    public double getDisplayAffectedValue() {
        if (UNIT_INVERSE_ABSOLUTE_PERCENT.equals(unitName)) {
            return (1.0 - affectedValue) * 100.0;
        }

        return affectedValue;
    }

    public void setAffectedValue(double affectedValue) {
        this.affectedValue = affectedValue;
    }

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public String getDisplayUnit() {
        return displayUnit;
    }

    public void setDisplayUnit(String displayUnit) {
        this.displayUnit = displayUnit;
    }

    public static FittingAttribute fromCursor(Cursor cursor) {
        FittingAttribute attribute = new FittingAttribute();
        attribute.setId(cursor.getLong(0));
        attribute.setName(cursor.getString(1));
        attribute.setValue(cursor.getDouble(2));
        attribute.setUnitName(cursor.getString(3));
        attribute.setDisplayUnit(cursor.getString(4));
        attribute.setAffectedValue(attribute.getValue()); // Initial value to set
        return attribute;
    }
}
