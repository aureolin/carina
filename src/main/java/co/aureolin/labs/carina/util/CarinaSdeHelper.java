/**
 * Project Carina - EVE Fitting Engine Android Library
 *
 * Copyright (c) 2015 Aureolin
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package co.aureolin.labs.carina.util;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import co.aureolin.labs.carina.fitting.Effect;
import co.aureolin.labs.carina.fitting.Expression;

/**
 * Created by Akinwale on 13/10/2015.
 */
public class CarinaSdeHelper {
    private static final String SQL_SELECT_EXPRESSIONS_TREE_FORMAT =
        "WITH ExpressionTree (expressionID, operandID, arg1, arg2, expressionValue, expressionName, expressionTypeID, expressionGroupID, expressionAttributeID) AS\n" +
        "(SELECT Expr1.expressionID " +
        "      , Expr1.operandID " +
        "      , Expr1.arg1 " +
        "      , Expr1.arg2 " +
        "      , Expr1.expressionValue " +
        "      , Expr1.expressionName " +
        "      , Expr1.expressionTypeID " +
        "      , Expr1.expressionGroupID " +
        "      , Expr1.expressionAttributeID " +
        "    FROM dgmExpressions Expr1 " +
        "    WHERE Expr1.expressionID IN (%s) " +
        " UNION ALL " +
        " SELECT Expr2.expressionID" +
        "      , Expr2.operandID" +
        "      , Expr2.arg1" +
        "      , Expr2.arg2" +
        "      , Expr2.expressionValue" +
        "      , Expr2.expressionName" +
        "      , Expr2.expressionTypeID" +
        "      , Expr2.expressionGroupID" +
        "      , Expr2.expressionAttributeID FROM dgmExpressions Expr2, ExpressionTree ExprTree" +
        "    WHERE Expr2.expressionID = ExprTree.arg1 OR Expr2.expressionID = ExprTree.arg2)" +
        "SELECT DISTINCT * FROM ExpressionTree ORDER BY expressionID";

    private static final String SQL_SELECT_EXPRESSION_TREE_FOR_TYPE_EFFECTS_FORMAT =
        "SELECT expressionID, operandID, expressionValue, expressionName, " +
        "    expressionTypeID, expressionGroupID, expressionAttributeID, arg1, arg2 " +
        "FROM dgmExpressions WHERE expressionID IN (%s) ORDER BY expressionID";

    private static final String SQL_SELECT_EXPRESSION_BASE_FORMAT =
        "SELECT expressionID, operandID, expressionValue, expressionName, " +
        "    expressionTypeID, expressionGroupID, expressionAttributeID, arg1, arg2 " +
        "FROM dgmExpressions";

    public static Map<Long, Expression> expressionCache = new HashMap<Long, Expression>();

    public static Map<Long, Effect> effectCache = new HashMap<Long, Effect>();

    public static HashMap<Long, String> skillIdNameMap = new HashMap<Long, String>();

    /**
     * Level 1 - 250
     * level 2 - 1415
     * level 3 - 8000
     * level 4 - 45255
     * level 5 - 256000
     */
    public static final int BASE_LVL1_POINTS = 250;

    public static final int BASE_LVL2_POINTS = 1415 - BASE_LVL1_POINTS; // SP between Lvl 1 and Lvl 2

    public static final int BASE_LVL3_POINTS = 8000 - BASE_LVL2_POINTS; // etc.

    public static final int BASE_LVL4_POINTS = 45255 - BASE_LVL3_POINTS;

    public static final int BASE_LVL5_POINTS = 256000 - BASE_LVL4_POINTS;

    public static void initialise(SQLiteDatabase db) {
        if (skillIdNameMap == null || skillIdNameMap.keySet().size() == 0) {
            skillIdNameMap = getSkillIdNameMap(db);
        }
    }

    public static String getSkillName(long id) {
        return (skillIdNameMap.containsKey(id)) ? skillIdNameMap.get(id) : "Unknown";
    }

    public static HashMap<Long, String> getSkillIdNameMap(SQLiteDatabase db) {
        return getIdNameMap("EN_Skills", db);
    }

    public static HashMap<Long, String> getIdNameMap(String tableName, SQLiteDatabase db) {
        HashMap<Long, String> map = new HashMap<Long, String>();
        String sql = String.format("SELECT TypeId, Name FROM %s", tableName);
        Cursor cursor = null;
        try {
            cursor = db.rawQuery(sql, null);
            while (cursor.moveToNext()) {
                map.put(cursor.getLong(0), cursor.getString(1));
            }
        } finally {
            Helper.closeCursor(cursor);
        }

        return map;
    }

    public static void loadExpressions(long[] ids, SQLiteDatabase db) {
        Map<Long, Expression> loadedExpressions = new HashMap<Long, Expression>();
        String idList = Helper.getCommaSeparatedValues(ids);
        String sql = String.format(SQL_SELECT_EXPRESSIONS_TREE_FORMAT, idList);
        Cursor cursor = null;
        try {
            cursor = db.rawQuery(sql, null);
            while (cursor.moveToNext()) {
                Expression expression = Expression.fromCursor(cursor);
                expression.setArgument1Id(cursor.getLong(7));
                expression.setArgument2Id(cursor.getLong(8));
                loadedExpressions.put(expression.getId(), expression);
            }
        } finally {
            Helper.closeCursor(cursor);
        }

        processLoadedExpressions(loadedExpressions);

        // Save to the expression cache
        expressionCache.putAll(loadedExpressions);
    }

    private static final String SQL_SELECT_EXPRESSION_IDS_FORMAT =
        "SELECT expressionID, arg1, arg2 FROM dgmExpressions WHERE expressionID IN (%s)";

    private static final String SQL_SELECT_EXPRESSION_IDS_FOR_TYPES_FORMAT =
        "SELECT expressionID FROM dgmExpressions WHERE expressionID " +
        "    IN (SELECT preExpression FROM EN_TypeEffects WHERE TypeId IN (%s)%s)" +
        " OR expressionID IN (SELECT postExpression FROM EN_TypeEffects WHERE TypeId IN (%s)%s)";

    // Android 4.4- no support for WITH clause... Why?
    public static List<Long> getExpressionTreeIds(long[] expressionIds, SQLiteDatabase db) {
        List<Long> expressionIdList = new ArrayList<Long>();

        if (expressionIds.length > 0) {
            String idList = Helper.getCommaSeparatedValues(expressionIds);
            String sql = String.format(SQL_SELECT_EXPRESSION_IDS_FORMAT, idList);
            Cursor cursor = null;
            try {
                cursor = db.rawQuery(sql, null);
                while (cursor.moveToNext()) {
                    long expressionId = cursor.getLong(0);
                    long arg1Id = cursor.getLong(1);
                    long arg2Id = cursor.getLong(2);
                    if (!expressionIdList.contains(expressionId)) {
                        expressionIdList.add(expressionId);
                    }

                    List<Long> args = new ArrayList<Long>();
                    if (arg1Id > 0 && !expressionIdList.contains(arg1Id)) {
                        expressionIdList.add(arg1Id);
                        args.add(arg1Id);
                    }
                    if (arg2Id > 0 && !expressionIdList.contains(arg2Id)) {
                        expressionIdList.add(arg2Id);
                        args.add(arg2Id);
                    }

                    if (args.size() > 0) {
                        expressionIdList.addAll(getExpressionTreeIds(Helper.toPrimitive(args), db));
                    }
                }
            } finally {
                Helper.closeCursor(cursor);
            }
        }

        return expressionIdList;
    }

    public static void loadExpressionsForTypes(long[] typeIds, SQLiteDatabase db) {
        List<Long> typeExpressionIds = new ArrayList<Long>();
        String typeIdList = Helper.getCommaSeparatedValues(typeIds);

        String effectFilterClause = "";
        String cachedEffectIds = getEffectCacheIdList();
        if (!Helper.isEmpty(cachedEffectIds)) {
            effectFilterClause = String.format(" AND EffectId NOT IN (%s)", cachedEffectIds);
        }

        String sql = String.format(
                SQL_SELECT_EXPRESSION_IDS_FOR_TYPES_FORMAT,
                typeIdList,
                effectFilterClause,
                typeIdList,
                effectFilterClause);

        Cursor cursor = null;
        try {
            cursor = db.rawQuery(sql, null);
            while (cursor.moveToNext()) {
                typeExpressionIds.add(cursor.getLong(0));
            }
        } finally {
            Helper.closeCursor(cursor);
        }

        List<Long> expressionTreeIds = new ArrayList<Long>();
        if (typeExpressionIds.size() > 0) {
            long[] expressionIds = Helper.toPrimitive(typeExpressionIds);
            expressionTreeIds = getExpressionTreeIds(expressionIds, db);
        }

        if (expressionTreeIds.size() > 0) {
            String idList = Helper.getCommaSeparatedValues(Helper.toPrimitive(expressionTreeIds));
            Map<Long, Expression> loadedExpressions = new HashMap<Long, Expression>();
            sql = String.format(SQL_SELECT_EXPRESSION_TREE_FOR_TYPE_EFFECTS_FORMAT, idList);
            try {
                cursor = db.rawQuery(sql, null);
                while (cursor.moveToNext()) {
                    Expression expression = Expression.fromCursor(cursor);
                    expression.setArgument1Id(cursor.getLong(7));
                    expression.setArgument2Id(cursor.getLong(8));
                    loadedExpressions.put(expression.getId(), expression);
                }
            } finally {
                Helper.closeCursor(cursor);
            }

            processLoadedExpressions(loadedExpressions);

            // Save to the expression cache
            expressionCache.putAll(loadedExpressions);
        }
    }

    private static void processLoadedExpressions(Map<Long, Expression> loadedExpressions) {
        // Set parent/children associations
        for (Map.Entry<Long, Expression> entry : loadedExpressions.entrySet()) {
            long expressionId = entry.getKey();
            Expression expression = entry.getValue();
            long arg1Id = expression.getArgument1Id();
            long arg2Id = expression.getArgument2Id();
            if (loadedExpressions.containsKey(arg1Id)) {
                loadedExpressions.get(arg1Id).setParentExpression(expression);
                expression.setArgument1(loadedExpressions.get(arg1Id));
            }
            if (loadedExpressions.containsKey(arg2Id)) {
                loadedExpressions.get(arg2Id).setParentExpression(expression);
                expression.setArgument2(loadedExpressions.get(arg2Id));
            }
        }
    }

    // Use this to prevent reloading cache IDs from queries, make thing faster
    public static String getEffectCacheIdList() {
        return getEffectCacheIdList(new ArrayList<Long>());
    }
    public static String getEffectCacheIdList(List<Long> additionalIds) {
        List<Long> effectCacheIds = new ArrayList<Long>(effectCache.keySet());
        if (additionalIds != null && additionalIds.size() > 0) {
            effectCacheIds.addAll(additionalIds);
        }

        return Helper.getCommaSeparatedValues(Helper.toPrimitive(effectCacheIds));
    }

    public static Expression getCachedExpression(long id) {
        if (expressionCache.containsKey(id)) {
            return expressionCache.get(id);
        }
        return null;
    }

    public static Effect getCachedEffect(long id) {
        if (effectCache.containsKey(id)) {
            return effectCache.get(id);
        }
        return null;
    }

    public static Expression getExpression(long id, Expression parent, SQLiteDatabase db) {
        if (expressionCache.containsKey(id)) {
            return expressionCache.get(id);
        }

        Expression expression = null;
        String sql = String.format("%s WHERE expressionID = %d",
                SQL_SELECT_EXPRESSION_BASE_FORMAT, id);
        Cursor cursor = null;
        try {
            cursor = db.rawQuery(sql, null);
            if (cursor.moveToNext()) {
                expression = Expression.fromCursor(cursor);
                expression.setParentExpression(parent);
                long arg1Id = cursor.getLong(7);
                long arg2Id = cursor.getLong(8);

                if (arg1Id > 0) {
                    expression.setArgument1(getExpression(arg1Id, expression, db));
                }
                if (arg2Id > 0) {
                    expression.setArgument2(getExpression(arg2Id, expression, db));
                }
            }
        } finally {
            Helper.closeCursor(cursor);
        }

        return expression;
    }
}
