/**
 * Project Carina - EVE Fitting Engine Android Library
 *
 * Copyright (c) 2015 Aureolin
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package co.aureolin.labs.carina.util;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.Date;

/**
 * Created by Akinwale on 20/09/2015.
 */
public final class XmlModelHelper {
    public static String getStringAttribute(String attributeName, XmlPullParser parser) {
        return parser.getAttributeValue(null, attributeName);
    }

    public static int getIntAttribute(String attributeName, XmlPullParser parser) {
        String value = getStringAttribute(attributeName, parser);
        return Helper.parseInt(value, -1);
    }

    public static long getLongAttribute(String attributeName, XmlPullParser parser) {
        String value = getStringAttribute(attributeName, parser);
        return Helper.parseLong(value, -1);
    }

    public static Date getDateAttribute(String attributeName, XmlPullParser parser) {
        String value = getStringAttribute(attributeName, parser);
        return Helper.parseIsoDate(value, new Date(0));
    }

    public static String readXmlValue(String tagName, XmlPullParser parser)
            throws IOException, XmlPullParserException {
        String ns = null;
        parser.require(XmlPullParser.START_TAG, ns, tagName);
        String text = readText(parser);
        parser.require(XmlPullParser.END_TAG, ns, tagName);
        return text;
    }

    public static String readText(XmlPullParser parser) {
        String text = "";
        try {
            if (parser.next() == XmlPullParser.TEXT) {
                text = parser.getText();
                parser.nextTag();
            }
        } catch (XmlPullParserException | IOException e) {
            // ignore and continue
        }
        return text;
    }
}
