/**
 * Project Carina - EVE Fitting Engine Android Library
 *
 * Copyright (c) 2015 Aureolin
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package co.aureolin.labs.carina.util;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Akinwale on 12/10/2015.
 */
public final class FittingHelper {
    /**
     * http://go-dl1.eve-files.com/media/corp/thegrandpumba/StackingPenaltyGuide.pdf
     */
    public static final double[] penalisationConstants = {
            1.000000000000,
            0.869119980800,
            0.570583143511,
            0.282955154023,
            0.105992649743,
            0.029991166533,
            0.006410183118,
            0.001034920483,
            0.000126212683,
            0.000011626754,
            0.000000809046
    };

    public static BiMap<Integer, String> fittingAttributesMap = HashBiMap.create();
    static {
        // TODO: Database load? Faster to just put everything in memory...
        // TODO: Identify which attributes+effects aren't required for fitting tool and remove
        fittingAttributesMap.put(3, "damage");
        fittingAttributesMap.put(6, "capacitorNeed");
        fittingAttributesMap.put(9, "hp");
        fittingAttributesMap.put(11, "powerOutput");
        fittingAttributesMap.put(12, "lowSlots");
        fittingAttributesMap.put(13, "medSlots");
        fittingAttributesMap.put(14, "hiSlots");
        fittingAttributesMap.put(15, "powerLoad");
        fittingAttributesMap.put(19, "powerToSpeed");
        fittingAttributesMap.put(20, "speedFactor");
        fittingAttributesMap.put(21, "warpFactor");
        fittingAttributesMap.put(30, "power");
        fittingAttributesMap.put(37, "maxVelocity");
        fittingAttributesMap.put(39, "damageHP");
        fittingAttributesMap.put(47, "slots");
        fittingAttributesMap.put(48, "cpuOutput");
        fittingAttributesMap.put(49, "cpuLoad");
        fittingAttributesMap.put(50, "cpu");
        fittingAttributesMap.put(51, "speed");
        fittingAttributesMap.put(54, "maxRange");
        fittingAttributesMap.put(55, "rechargeRate");
        fittingAttributesMap.put(56, "chargeRate");
        fittingAttributesMap.put(61, "targetModule");
        fittingAttributesMap.put(63, "accuracyBonus");
        fittingAttributesMap.put(64, "damageMultiplier");
        fittingAttributesMap.put(66, "durationBonus");
        fittingAttributesMap.put(67, "capacitorBonus");
        fittingAttributesMap.put(68, "shieldBonus");
        fittingAttributesMap.put(70, "agility");
        fittingAttributesMap.put(72, "capacityBonus");
        fittingAttributesMap.put(73, "duration");
        fittingAttributesMap.put(76, "maxTargetRange");
        fittingAttributesMap.put(77, "miningAmount");
        fittingAttributesMap.put(79, "scanSpeed");
        fittingAttributesMap.put(80, "speedBonus");
        fittingAttributesMap.put(83, "structureDamageAmount");
        fittingAttributesMap.put(84, "armorDamageAmount");
        fittingAttributesMap.put(87, "shieldTransferRange");
        fittingAttributesMap.put(90, "powerTransferAmount");
        fittingAttributesMap.put(91, "powerTransferRange");
        fittingAttributesMap.put(97, "energyDestabilizationAmount");
        fittingAttributesMap.put(98, "energyDestabilizationRange");
        fittingAttributesMap.put(99, "empFieldRange");
        fittingAttributesMap.put(101, "launcherSlotsLeft");
        fittingAttributesMap.put(102, "turretSlotsLeft");
        fittingAttributesMap.put(103, "warpScrambleRange");
        fittingAttributesMap.put(104, "warpScrambleStatus");
        fittingAttributesMap.put(105, "warpScrambleStrength");
        fittingAttributesMap.put(107, "explosionRange");
        fittingAttributesMap.put(108, "detonationRange");
        fittingAttributesMap.put(109, "kineticDamageResonance");
        fittingAttributesMap.put(110, "thermalDamageResonance");
        fittingAttributesMap.put(111, "explosiveDamageResonance");
        fittingAttributesMap.put(113, "emDamageResonance");
        fittingAttributesMap.put(114, "emDamage");
        fittingAttributesMap.put(116, "explosiveDamage");
        fittingAttributesMap.put(117, "kineticDamage");
        fittingAttributesMap.put(118, "thermalDamage");
        fittingAttributesMap.put(120, "weaponRangeMultiplier");
        fittingAttributesMap.put(122, "armorPiercingChance");
        fittingAttributesMap.put(123, "shieldPiercingChance");
        fittingAttributesMap.put(124, "mainColor");
        fittingAttributesMap.put(125, "shipScanRange");
        fittingAttributesMap.put(126, "cargoScanRange");
        fittingAttributesMap.put(128, "chargeSize");
        fittingAttributesMap.put(129, "maxPassengers");
        fittingAttributesMap.put(134, "shieldRechargeRateMultiplier");
        fittingAttributesMap.put(136, "uniformity");
        fittingAttributesMap.put(137, "launcherGroup");
        fittingAttributesMap.put(142, "ecmBurstRange");
        fittingAttributesMap.put(143, "targetHostileRange");
        fittingAttributesMap.put(144, "capacitorRechargeRateMultiplier");
        fittingAttributesMap.put(145, "powerOutputMultiplier");
        fittingAttributesMap.put(146, "shieldCapacityMultiplier");
        fittingAttributesMap.put(147, "capacitorCapacityMultiplier");
        fittingAttributesMap.put(148, "armorHPMultiplier");
        fittingAttributesMap.put(149, "cargoCapacityMultiplier");
        fittingAttributesMap.put(150, "structureHPMultiplier");
        fittingAttributesMap.put(151, "agilityBonus");
        fittingAttributesMap.put(153, "warpCapacitorNeed");
        fittingAttributesMap.put(158, "falloff");
        fittingAttributesMap.put(160, "trackingSpeed");
        fittingAttributesMap.put(169, "agilityMultiplier");
        fittingAttributesMap.put(175, "charismaBonus");
        fittingAttributesMap.put(176, "intelligenceBonus");
        fittingAttributesMap.put(177, "memoryBonus");
        fittingAttributesMap.put(178, "perceptionBonus");
        fittingAttributesMap.put(179, "willpowerBonus");
        fittingAttributesMap.put(180, "primaryAttribute");
        fittingAttributesMap.put(181, "secondaryAttribute");
        fittingAttributesMap.put(182, "requiredSkill1");
        fittingAttributesMap.put(183, "requiredSkill2");
        fittingAttributesMap.put(184, "requiredSkill3");
        fittingAttributesMap.put(188, "cargoScanResistance");
        fittingAttributesMap.put(189, "targetGroup");
        fittingAttributesMap.put(191, "corporationMemberBonus");
        fittingAttributesMap.put(192, "maxLockedTargets");
        fittingAttributesMap.put(193, "maxAttackTargets");
        fittingAttributesMap.put(197, "surveyScanRange");
        fittingAttributesMap.put(202, "cpuMultiplier");
        fittingAttributesMap.put(204, "speedMultiplier");
        fittingAttributesMap.put(207, "miningAmountMultiplier");
        fittingAttributesMap.put(208, "scanRadarStrength");
        fittingAttributesMap.put(209, "scanLadarStrength");
        fittingAttributesMap.put(210, "scanMagnetometricStrength");
        fittingAttributesMap.put(211, "scanGravimetricStrength");
        fittingAttributesMap.put(213, "missileDamageMultiplierBonus");
        fittingAttributesMap.put(216, "capacitorNeedMultiplier");
        fittingAttributesMap.put(217, "propulsionGraphicID");
        fittingAttributesMap.put(235, "maxLockedTargetsBonus");
        fittingAttributesMap.put(237, "maxTargetRangeMultiplier");
        fittingAttributesMap.put(238, "scanGravimetricStrengthBonus");
        fittingAttributesMap.put(239, "scanLadarStrengthBonus");
        fittingAttributesMap.put(240, "scanMagnetometricStrengthBonus");
        fittingAttributesMap.put(241, "scanRadarStrengthBonus");
        fittingAttributesMap.put(243, "maxRangeMultiplier");
        fittingAttributesMap.put(244, "trackingSpeedMultiplier");
        fittingAttributesMap.put(246, "gfxBoosterID");
        fittingAttributesMap.put(263, "shieldCapacity");
        fittingAttributesMap.put(265, "armorHP");
        fittingAttributesMap.put(267, "armorEmDamageResonance");
        fittingAttributesMap.put(268, "armorExplosiveDamageResonance");
        fittingAttributesMap.put(269, "armorKineticDamageResonance");
        fittingAttributesMap.put(270, "armorThermalDamageResonance");
        fittingAttributesMap.put(271, "shieldEmDamageResonance");
        fittingAttributesMap.put(272, "shieldExplosiveDamageResonance");
        fittingAttributesMap.put(273, "shieldKineticDamageResonance");
        fittingAttributesMap.put(274, "shieldThermalDamageResonance");
        fittingAttributesMap.put(275, "skillTimeConstant");
        fittingAttributesMap.put(277, "requiredSkill1Level");
        fittingAttributesMap.put(278, "requiredSkill2Level");
        fittingAttributesMap.put(279, "requiredSkill3Level");
        fittingAttributesMap.put(280, "skillLevel");
        fittingAttributesMap.put(281, "explosionDelay");
        fittingAttributesMap.put(283, "droneCapacity");
        fittingAttributesMap.put(292, "damageMultiplierBonus");
        fittingAttributesMap.put(293, "rofBonus");
        fittingAttributesMap.put(294, "rangeSkillBonus");
        fittingAttributesMap.put(306, "maxVelocityBonus");
        fittingAttributesMap.put(308, "scanspeedBonus");
        fittingAttributesMap.put(309, "maxTargetRangeBonus");
        fittingAttributesMap.put(310, "cpuNeedBonus");
        fittingAttributesMap.put(311, "maxTargetBonus");
        fittingAttributesMap.put(312, "durationSkillBonus");
        fittingAttributesMap.put(313, "powerEngineeringOutputBonus");
        fittingAttributesMap.put(314, "capRechargeBonus");
        fittingAttributesMap.put(315, "velocityBonus");
        fittingAttributesMap.put(317, "capNeedBonus");
        fittingAttributesMap.put(318, "speedFBonus");
        fittingAttributesMap.put(319, "warpCapacitorNeedBonus");
        fittingAttributesMap.put(323, "powerNeedBonus");
        fittingAttributesMap.put(327, "hullHpBonus");
        fittingAttributesMap.put(330, "boosterDuration");
        fittingAttributesMap.put(331, "implantness");
        fittingAttributesMap.put(334, "shipPowerBonus");
        fittingAttributesMap.put(335, "armorHpBonus");
        fittingAttributesMap.put(336, "uniformityBonus");
        fittingAttributesMap.put(337, "shieldCapacityBonus");
        fittingAttributesMap.put(338, "rechargeratebonus");
        fittingAttributesMap.put(349, "falloffBonus");
        fittingAttributesMap.put(351, "maxRangeBonus");
        fittingAttributesMap.put(353, "maxActiveDroneBonus");
        fittingAttributesMap.put(379, "refiningYieldMutator");
        fittingAttributesMap.put(407, "researchGangSizeBonus");
        fittingAttributesMap.put(412, "connectionBonusMutator");
        fittingAttributesMap.put(413, "criminalConnectionsMutator");
        fittingAttributesMap.put(414, "diplomacyMutator");
        fittingAttributesMap.put(415, "fastTalkMutator");
        fittingAttributesMap.put(422, "techLevel");
        fittingAttributesMap.put(424, "cpuOutputBonus2");
        fittingAttributesMap.put(434, "miningAmountBonus");
        fittingAttributesMap.put(435, "maxGangModules");
        fittingAttributesMap.put(437, "negotiationBonus");
        fittingAttributesMap.put(438, "socialMutator");
        fittingAttributesMap.put(440, "manufacturingTimeBonus");
        fittingAttributesMap.put(441, "turretSpeeBonus");
        fittingAttributesMap.put(446, "tradePremiumBonus");
        fittingAttributesMap.put(447, "smugglingChanceBonus");
        fittingAttributesMap.put(450, "manufacturingSlotBonus");
        fittingAttributesMap.put(452, "copySpeedBonus");
        fittingAttributesMap.put(453, "blueprintmanufactureTimeBonus");
        fittingAttributesMap.put(459, "droneRangeBonus");
        fittingAttributesMap.put(460, "shipBonusMF");
        fittingAttributesMap.put(462, "shipBonusGF");
        fittingAttributesMap.put(463, "shipBonusCF");
        fittingAttributesMap.put(464, "shipBonusAF");
        fittingAttributesMap.put(468, "mineralNeedResearchBonus");
        fittingAttributesMap.put(471, "laboratorySlotsBonus");
        fittingAttributesMap.put(474, "inventionBonus");
        fittingAttributesMap.put(478, "shipBonusAC");
        fittingAttributesMap.put(479, "shieldRechargeRate");
        fittingAttributesMap.put(482, "capacitorCapacity");
        fittingAttributesMap.put(484, "shieldUniformity");
        fittingAttributesMap.put(485, "shipBonus2AF");
        fittingAttributesMap.put(486, "shipBonusGC");
        fittingAttributesMap.put(487, "shipBonusCC");
        fittingAttributesMap.put(489, "shipBonusMC");
        fittingAttributesMap.put(490, "shipBonusMB");
        fittingAttributesMap.put(491, "shipBonusCB");
        fittingAttributesMap.put(492, "shipBonusAB");
        fittingAttributesMap.put(493, "shipBonusMI");
        fittingAttributesMap.put(494, "shipBonusAI");
        fittingAttributesMap.put(495, "shipBonusCI");
        fittingAttributesMap.put(496, "shipBonusGI");
        fittingAttributesMap.put(500, "shipBonusGB");
        fittingAttributesMap.put(501, "shipBonus2CB");
        fittingAttributesMap.put(510, "shipScanFalloff");
        fittingAttributesMap.put(511, "shipScanResistance");
        fittingAttributesMap.put(517, "fallofMultiplier");
        fittingAttributesMap.put(518, "shipBonusMB2");
        fittingAttributesMap.put(522, "damageCloudChance");
        fittingAttributesMap.put(524, "armorUniformity");
        fittingAttributesMap.put(525, "structureUniformity");
        fittingAttributesMap.put(543, "damageCloudChanceReduction");
        fittingAttributesMap.put(547, "missileVelocityBonus");
        fittingAttributesMap.put(548, "shieldBoostMultiplier");
        fittingAttributesMap.put(549, "powerIncrease");
        fittingAttributesMap.put(550, "resistanceBonus");
        fittingAttributesMap.put(552, "signatureRadius");
        fittingAttributesMap.put(554, "signatureRadiusBonus");
        fittingAttributesMap.put(557, "maxFlightTimeBonus");
        fittingAttributesMap.put(560, "cloakingTargetingDelay");
        fittingAttributesMap.put(561, "shipBonusGB2");
        fittingAttributesMap.put(564, "scanResolution");
        fittingAttributesMap.put(565, "scanResolutionMultiplier");
        fittingAttributesMap.put(566, "scanResolutionBonus");
        fittingAttributesMap.put(567, "speedBoostFactor");
        fittingAttributesMap.put(568, "eliteBonusInterceptor");
        fittingAttributesMap.put(569, "eliteBonusCoverOps1");
        fittingAttributesMap.put(576, "speedBoostFactorCalc");
        fittingAttributesMap.put(578, "speedBoostFactorCalc2");
        fittingAttributesMap.put(585, "shipBonusAB2");
        fittingAttributesMap.put(586, "shipBonusGF2");
        fittingAttributesMap.put(587, "shipBonusMF2");
        fittingAttributesMap.put(588, "shipBonusCF2");
        fittingAttributesMap.put(591, "droneMaxVelocityBonus");
        fittingAttributesMap.put(596, "explosionDelayBonus");
        fittingAttributesMap.put(598, "shipBonusCB3");
        fittingAttributesMap.put(600, "warpSpeedMultiplier");
        fittingAttributesMap.put(602, "launcherGroup2");
        fittingAttributesMap.put(603, "launcherGroup3");
        fittingAttributesMap.put(604, "chargeGroup1");
        fittingAttributesMap.put(605, "chargeGroup2");
        fittingAttributesMap.put(606, "chargeGroup3");
        fittingAttributesMap.put(608, "powerNeedMultiplier");
        fittingAttributesMap.put(609, "chargeGroup4");
        fittingAttributesMap.put(612, "baseShieldDamage");
        fittingAttributesMap.put(613, "baseArmorDamage");
        fittingAttributesMap.put(614, "cargoCapacityBonus");
        fittingAttributesMap.put(616, "boosterShieldBoostAmountPenalty");
        fittingAttributesMap.put(619, "cloakingTargetingDelayBonus");
        fittingAttributesMap.put(620, "optimalSigRadius");
        fittingAttributesMap.put(624, "WarpSBonus");
        fittingAttributesMap.put(633, "metaLevel");
        fittingAttributesMap.put(644, "aimedLaunch");
        fittingAttributesMap.put(649, "cloakingCpuNeedBonus");
        fittingAttributesMap.put(653, "aoeVelocity");
        fittingAttributesMap.put(654, "aoeCloudSize");
        fittingAttributesMap.put(655, "aoeFalloff");
        fittingAttributesMap.put(656, "shipBonusAC2");
        fittingAttributesMap.put(657, "shipBonusCC2");
        fittingAttributesMap.put(658, "shipBonusGC2");
        fittingAttributesMap.put(659, "shipBonusMC2");
        fittingAttributesMap.put(661, "maxDirectionalVelocity");
        fittingAttributesMap.put(662, "minTargetVelDmgMultiplier");
        fittingAttributesMap.put(669, "moduleReactivationDelay");
        fittingAttributesMap.put(673, "eliteBonusGunship1");
        fittingAttributesMap.put(675, "eliteBonusGunship2");
        fittingAttributesMap.put(678, "eliteBonusLogistics1");
        fittingAttributesMap.put(679, "eliteBonusLogistics2");
        fittingAttributesMap.put(692, "eliteBonusHeavyGunship1");
        fittingAttributesMap.put(693, "eliteBonusHeavyGunship2");
        fittingAttributesMap.put(713, "consumptionType");
        fittingAttributesMap.put(714, "consumptionQuantity");
        fittingAttributesMap.put(715, "maxOperationalDistance");
        fittingAttributesMap.put(716, "maxOperationalUsers");
        fittingAttributesMap.put(727, "destroyerROFpenality");
        fittingAttributesMap.put(729, "shipBonusMD1");
        fittingAttributesMap.put(734, "shipBonusCD1");
        fittingAttributesMap.put(735, "shipBonusCD2");
        fittingAttributesMap.put(738, "shipBonusGD1");
        fittingAttributesMap.put(739, "shipBonusGD2");
        fittingAttributesMap.put(740, "shipBonusMD2");
        fittingAttributesMap.put(743, "shipBonusCBC1");
        fittingAttributesMap.put(745, "shipBonusCBC2");
        fittingAttributesMap.put(746, "shipBonusGBC2");
        fittingAttributesMap.put(747, "shipBonusGBC1");
        fittingAttributesMap.put(748, "shipBonusMBC1");
        fittingAttributesMap.put(749, "shipBonusMBC2");
        fittingAttributesMap.put(763, "maxGroupActive");
        fittingAttributesMap.put(765, "scanRange");
        fittingAttributesMap.put(767, "trackingSpeedBonus");
        fittingAttributesMap.put(774, "shipBonusORE2");
        fittingAttributesMap.put(779, "entityFlyRangeMultiplier");
        fittingAttributesMap.put(780, "iceHarvestCycleBonus");
        fittingAttributesMap.put(781, "specialisationAsteroidGroup");
        fittingAttributesMap.put(782, "specialisationAsteroidYieldMultiplier");
        fittingAttributesMap.put(783, "crystalVolatilityChance");
        fittingAttributesMap.put(784, "crystalVolatilityDamage");
        fittingAttributesMap.put(785, "unfitCapCost");
        fittingAttributesMap.put(786, "crystalsGetDamaged");
        fittingAttributesMap.put(789, "specialtyMiningAmount");
        fittingAttributesMap.put(793, "shipBonusPirateFaction");
        fittingAttributesMap.put(794, "probesInGroup");
        fittingAttributesMap.put(795, "shipBonusABC1");
        fittingAttributesMap.put(796, "massAddition");
        fittingAttributesMap.put(797, "maximumRangeCap");
        fittingAttributesMap.put(799, "implantSetBloodraider");
        fittingAttributesMap.put(801, "deadspaceUnsafe");
        fittingAttributesMap.put(802, "implantSetSerpentis");
        fittingAttributesMap.put(804, "eliteBonusInterceptor2");
        fittingAttributesMap.put(806, "repairBonus");
        fittingAttributesMap.put(807, "eliteBonusIndustrial1");
        fittingAttributesMap.put(808, "eliteBonusIndustrial2");
        fittingAttributesMap.put(809, "shipBonusAI2");
        fittingAttributesMap.put(811, "shipBonusCI2");
        fittingAttributesMap.put(813, "shipBonusGI2");
        fittingAttributesMap.put(814, "shipBonusMI2");
        fittingAttributesMap.put(828, "scanSkillEwStrengthBonus");
        fittingAttributesMap.put(829, "propulsionSkillPropulsionStrengthBonus");
        fittingAttributesMap.put(831, "ewTargetJam");
        fittingAttributesMap.put(832, "scanSkillTargetPaintStrengthBonus");
        fittingAttributesMap.put(833, "commandBonus");
        fittingAttributesMap.put(834, "wingCommandBonus");
        fittingAttributesMap.put(837, "stealthBomberLauncherPower");
        fittingAttributesMap.put(838, "implantSetGuristas");
        fittingAttributesMap.put(839, "eliteBonusCoverOps2");
        fittingAttributesMap.put(845, "hiddenLauncherDamageBonus");
        fittingAttributesMap.put(846, "scanStrengthBonus");
        fittingAttributesMap.put(847, "aoeVelocityBonus");
        fittingAttributesMap.put(848, "aoeCloudSizeBonus");
        fittingAttributesMap.put(849, "canUseCargoInSpace");
        fittingAttributesMap.put(850, "squadronCommandBonus");
        fittingAttributesMap.put(851, "shieldBoostCapacitorBonus");
        fittingAttributesMap.put(852, "siegeModeWarpStatus");
        fittingAttributesMap.put(853, "advancedAgility");
        fittingAttributesMap.put(854, "disallowAssistance");
        fittingAttributesMap.put(855, "activationTargetLoss");
        fittingAttributesMap.put(861, "canJump");
        fittingAttributesMap.put(863, "implantSetAngel");
        fittingAttributesMap.put(864, "implantSetSansha");
        fittingAttributesMap.put(866, "jumpDriveConsumptionType");
        fittingAttributesMap.put(867, "jumpDriveRange");
        fittingAttributesMap.put(868, "jumpDriveConsumptionAmount");
        fittingAttributesMap.put(869, "jumpDriveDuration");
        fittingAttributesMap.put(870, "jumpDriveRangeBonus");
        fittingAttributesMap.put(872, "disallowOffensiveModifiers");
        fittingAttributesMap.put(874, "advancedCapitalAgility");
        fittingAttributesMap.put(875, "dreadnoughtShipBonusA1");
        fittingAttributesMap.put(876, "dreadnoughtShipBonusA2");
        fittingAttributesMap.put(877, "dreadnoughtShipBonusC1");
        fittingAttributesMap.put(878, "dreadnoughtShipBonusC2");
        fittingAttributesMap.put(879, "dreadnoughtShipBonusG1");
        fittingAttributesMap.put(880, "dreadnoughtShipBonusG2");
        fittingAttributesMap.put(881, "dreadnoughtShipBonusM1");
        fittingAttributesMap.put(884, "mindlinkBonus");
        fittingAttributesMap.put(885, "consumptionQuantityBonus");
        fittingAttributesMap.put(886, "freighterBonusA1");
        fittingAttributesMap.put(887, "freighterBonusA2");
        fittingAttributesMap.put(888, "freighterBonusC1");
        fittingAttributesMap.put(889, "freighterBonusC2");
        fittingAttributesMap.put(890, "freighterBonusG2");
        fittingAttributesMap.put(891, "freighterBonusG1");
        fittingAttributesMap.put(892, "freighterBonusM1");
        fittingAttributesMap.put(893, "freighterBonusM2");
        fittingAttributesMap.put(895, "armorDamageAmountBonus");
        fittingAttributesMap.put(896, "armorDamageDurationBonus");
        fittingAttributesMap.put(897, "shieldBonusDurationBonus");
        fittingAttributesMap.put(898, "jumpDriveCapacitorNeed");
        fittingAttributesMap.put(899, "jumpDriveCapacitorNeedBonus");
        fittingAttributesMap.put(902, "accessDifficultyBonus");
        fittingAttributesMap.put(906, "disallowEarlyDeactivation");
        fittingAttributesMap.put(907, "hasShipMaintenanceBay");
        fittingAttributesMap.put(908, "shipMaintenanceBayCapacity");
        fittingAttributesMap.put(911, "hasFleetHangars");
        fittingAttributesMap.put(912, "fleetHangarCapacity");
        fittingAttributesMap.put(914, "gallenteNavyBonusMultiplier");
        fittingAttributesMap.put(916, "caldariNavyBonusMultiplier");
        fittingAttributesMap.put(918, "amarrNavyBonusMulitplier");
        fittingAttributesMap.put(920, "republicFleetBonusMultiplier");
        fittingAttributesMap.put(924, "eliteBonusBarge1");
        fittingAttributesMap.put(925, "eliteBonusBarge2");
        fittingAttributesMap.put(926, "shipBonusORE3");
        fittingAttributesMap.put(927, "miningUpgradeCPUReductionBonus");
        fittingAttributesMap.put(958, "hardeningBonus");
        fittingAttributesMap.put(962, "eliteBonusReconShip1");
        fittingAttributesMap.put(963, "eliteBonusReconShip2");
        fittingAttributesMap.put(973, "signatureRadiusBonusPercent");
        fittingAttributesMap.put(974, "hullEmDamageResonance");
        fittingAttributesMap.put(975, "hullExplosiveDamageResonance");
        fittingAttributesMap.put(976, "hullKineticDamageResonance");
        fittingAttributesMap.put(977, "hullThermalDamageResonance");
        fittingAttributesMap.put(978, "maxGroupOnline");
        fittingAttributesMap.put(979, "maxJumpClones");
        fittingAttributesMap.put(981, "allowsCloneJumpsWhenActive");
        fittingAttributesMap.put(982, "canReceiveCloneJumps");
        fittingAttributesMap.put(983, "signatureRadiusAdd");
        fittingAttributesMap.put(984, "emDamageResistanceBonus");
        fittingAttributesMap.put(985, "explosiveDamageResistanceBonus");
        fittingAttributesMap.put(986, "kineticDamageResistanceBonus");
        fittingAttributesMap.put(987, "thermalDamageResistanceBonus");
        fittingAttributesMap.put(999, "eliteBonusCommandShips2");
        fittingAttributesMap.put(1000, "eliteBonusCommandShips1");
        fittingAttributesMap.put(1001, "jumpPortalConsumptionMassFactor");
        fittingAttributesMap.put(1002, "jumpPortalDuration");
        fittingAttributesMap.put(1005, "jumpPortalCapacitorNeed");
        fittingAttributesMap.put(1012, "eliteBonusInterdictors1");
        fittingAttributesMap.put(1013, "eliteBonusInterdictors2");
        fittingAttributesMap.put(1014, "disallowRepeatingActivation");
        fittingAttributesMap.put(1027, "scanGravimetricStrengthPercent");
        fittingAttributesMap.put(1028, "scanLadarStrengthPercent");
        fittingAttributesMap.put(1029, "scanMagnetometricStrengthPercent");
        fittingAttributesMap.put(1030, "scanRadarStrengthPercent");
        fittingAttributesMap.put(1034, "covertOpsAndReconOpsCloakModuleDelay");
        fittingAttributesMap.put(1035, "covertOpsStealthBomberTargettingDelay");
        fittingAttributesMap.put(1038, "titanAmarrBonus2");
        fittingAttributesMap.put(1039, "shipBonusCT1");
        fittingAttributesMap.put(1040, "shipBonusCT2");
        fittingAttributesMap.put(1041, "titanGallenteBonus1");
        fittingAttributesMap.put(1042, "titanGallenteBonus2");
        fittingAttributesMap.put(1043, "titanMinmatarBonus1");
        fittingAttributesMap.put(1044, "titanMinmatarBonus2");
        fittingAttributesMap.put(1045, "maxTractorVelocity");
        fittingAttributesMap.put(1047, "canNotBeTrainedOnTrial");
        fittingAttributesMap.put(1049, "carrierAmarrBonus1");
        fittingAttributesMap.put(1050, "carrierAmarrBonus2");
        fittingAttributesMap.put(1051, "carrierAmarrBonus3");
        fittingAttributesMap.put(1052, "carrierCaldariBonus1");
        fittingAttributesMap.put(1053, "carrierCaldariBonus2");
        fittingAttributesMap.put(1054, "carrierCaldariBonus3");
        fittingAttributesMap.put(1055, "carrierGallenteBonus1");
        fittingAttributesMap.put(1056, "carrierGallenteBonus2");
        fittingAttributesMap.put(1057, "carrierGallenteBonus3");
        fittingAttributesMap.put(1058, "carrierMinmatarBonus1");
        fittingAttributesMap.put(1059, "carrierMinmatarBonus2");
        fittingAttributesMap.put(1060, "carrierMinmatarBonus3");
        fittingAttributesMap.put(1061, "titanAmarrBonus3");
        fittingAttributesMap.put(1062, "titanAmarrBonus4");
        fittingAttributesMap.put(1064, "titanCaldariBonus4");
        fittingAttributesMap.put(1066, "titanGallenteBonus4");
        fittingAttributesMap.put(1067, "titanMinmatarBonus4");
        fittingAttributesMap.put(1068, "titanMinmatarBonus3");
        fittingAttributesMap.put(1069, "carrierAmarrBonus4");
        fittingAttributesMap.put(1070, "carrierCaldariBonus4");
        fittingAttributesMap.put(1071, "carrierGallenteBonus4");
        fittingAttributesMap.put(1072, "carrierMinmatarBonus4");
        fittingAttributesMap.put(1073, "maxJumpClonesBonus");
        fittingAttributesMap.put(1074, "disallowInEmpireSpace");
        fittingAttributesMap.put(1075, "missileNeverDoesDamage");
        fittingAttributesMap.put(1076, "implantBonusVelocity");
        fittingAttributesMap.put(1079, "capacitorCapacityBonus");
        fittingAttributesMap.put(1082, "cpuPenaltyPercent");
        fittingAttributesMap.put(1083, "armorHpBonus2");
        fittingAttributesMap.put(1087, "boosterness");
        fittingAttributesMap.put(1089, "boosterEffectChance1");
        fittingAttributesMap.put(1090, "boosterEffectChance2");
        fittingAttributesMap.put(1091, "boosterEffectChance3");
        fittingAttributesMap.put(1092, "boosterEffectChance4");
        fittingAttributesMap.put(1093, "boosterEffectChance5");
        fittingAttributesMap.put(1125, "boosterChanceBonus");
        fittingAttributesMap.put(1126, "boosterAttributeModifier");
        fittingAttributesMap.put(1130, "ecmStrengthBonusPercent");
        fittingAttributesMap.put(1131, "massBonusPercentage");
        fittingAttributesMap.put(1132, "upgradeCapacity");
        fittingAttributesMap.put(1137, "rigSlots");
        fittingAttributesMap.put(1138, "drawback");
        fittingAttributesMap.put(1139, "rigDrawbackBonus");
        fittingAttributesMap.put(1141, "boosterArmorHPPenalty");
        fittingAttributesMap.put(1142, "boosterArmorRepairAmountPenalty");
        fittingAttributesMap.put(1143, "boosterShieldCapacityPenalty");
        fittingAttributesMap.put(1144, "boosterTurretOptimalRange");
        fittingAttributesMap.put(1145, "boosterTurretTrackingPenalty");
        fittingAttributesMap.put(1146, "boosterTurretFalloffPenalty");
        fittingAttributesMap.put(1147, "boosterAOEVelocityPenalty");
        fittingAttributesMap.put(1148, "boosterMissileVelocityPenalty");
        fittingAttributesMap.put(1149, "boosterMissileAOECloudPenalty");
        fittingAttributesMap.put(1150, "boosterCapacitorCapacityPenalty");
        fittingAttributesMap.put(1151, "boosterMaxVelocityPenalty");
        fittingAttributesMap.put(1153, "upgradeCost");
        fittingAttributesMap.put(1154, "upgradeSlotsLeft");
        fittingAttributesMap.put(1156, "maxScanDeviationModifier");
        fittingAttributesMap.put(1159, "armorHPBonusAdd");
        fittingAttributesMap.put(1160, "accessDifficultyBonusModifier");
        fittingAttributesMap.put(1163, "canCloak");
        fittingAttributesMap.put(1164, "speedFactorBonus");
        fittingAttributesMap.put(1174, "posStructureControlAmount");
        fittingAttributesMap.put(1178, "heatCapacityHi");
        fittingAttributesMap.put(1179, "heatDissipationRateHi");
        fittingAttributesMap.put(1180, "heatAbsorbtionRateModifier");
        fittingAttributesMap.put(1186, "remoteArmorDamageAmountBonus");
        fittingAttributesMap.put(1187, "remoteArmorDamageDurationBonus");
        fittingAttributesMap.put(1188, "shieldTransportDurationBonus");
        fittingAttributesMap.put(1189, "shieldTransportAmountBonus");
        fittingAttributesMap.put(1190, "ewCapacitorNeedBonus");
        fittingAttributesMap.put(1191, "maxDronePercentageBonus");
        fittingAttributesMap.put(1193, "projECMDurationBonus");
        fittingAttributesMap.put(1196, "heatDissipationRateMed");
        fittingAttributesMap.put(1198, "heatDissipationRateLow");
        fittingAttributesMap.put(1199, "heatCapacityMed");
        fittingAttributesMap.put(1200, "heatCapacityLow");
        fittingAttributesMap.put(1201, "remoteHullDamageAmountBonus");
        fittingAttributesMap.put(1202, "remoteHullDamageDurationBonus");
        fittingAttributesMap.put(1203, "powerTransferAmountBonus");
        fittingAttributesMap.put(1204, "powerTransferDurationBonus");
        fittingAttributesMap.put(1205, "overloadRofBonus");
        fittingAttributesMap.put(1206, "overloadSelfDurationBonus");
        fittingAttributesMap.put(1208, "overloadHardeningBonus");
        fittingAttributesMap.put(1210, "overloadDamageModifier");
        fittingAttributesMap.put(1211, "heatDamage");
        fittingAttributesMap.put(1212, "requiredThermoDynamicsSkill");
        fittingAttributesMap.put(1213, "heatDamageBonus");
        fittingAttributesMap.put(1216, "shieldTransportCpuNeedBonus");
        fittingAttributesMap.put(1217, "remoteArmorPowerNeedBonus");
        fittingAttributesMap.put(1218, "powerTransferPowerNeedBonus");
        fittingAttributesMap.put(1219, "droneArmorDamageAmountBonus");
        fittingAttributesMap.put(1220, "droneShieldBonusBonus");
        fittingAttributesMap.put(1221, "jumpDelayDuration");
        fittingAttributesMap.put(1222, "overloadRangeBonus");
        fittingAttributesMap.put(1223, "overloadSpeedFactorBonus");
        fittingAttributesMap.put(1224, "heatGenerationMultiplier");
        fittingAttributesMap.put(1225, "overloadECMStrengthBonus");
        fittingAttributesMap.put(1226, "overloadECCMStrenghtBonus");
        fittingAttributesMap.put(1227, "signatureRadiusBonusBonus");
        fittingAttributesMap.put(1229, "thermodynamicsHeatDamage");
        fittingAttributesMap.put(1230, "overloadArmorDamageAmount");
        fittingAttributesMap.put(1231, "overloadShieldBonus");
        fittingAttributesMap.put(1234, "surveyScannerRangeBonus");
        fittingAttributesMap.put(1235, "cargoScannerRangeBonus");
        fittingAttributesMap.put(1236, "commandBonusEffective");
        fittingAttributesMap.put(1238, "commandBonusEffectiveAdd");
        fittingAttributesMap.put(1239, "shipBonusORECapital1");
        fittingAttributesMap.put(1240, "shipBonusORECapital2");
        fittingAttributesMap.put(1243, "shipBonusORECapital3");
        fittingAttributesMap.put(1244, "shipBonusORECapital4");
        fittingAttributesMap.put(1245, "disallowActivateOnWarp");
        fittingAttributesMap.put(1246, "eliteBonusHeavyInterdictors1");
        fittingAttributesMap.put(1247, "eliteBonusHeavyInterdictors2");
        fittingAttributesMap.put(1249, "eliteBonusElectronicAttackShip1");
        fittingAttributesMap.put(1250, "eliteBonusElectronicAttackShip2");
        fittingAttributesMap.put(1252, "isCovert");
        fittingAttributesMap.put(1253, "jumpHarmonics");
        fittingAttributesMap.put(1255, "droneDamageBonus");
        fittingAttributesMap.put(1257, "eliteBonusBlackOps1");
        fittingAttributesMap.put(1258, "eliteBonusBlackOps2");
        fittingAttributesMap.put(1259, "heatAttenuationHi");
        fittingAttributesMap.put(1261, "heatAttenuationMed");
        fittingAttributesMap.put(1262, "heatAttenuationLow");
        fittingAttributesMap.put(1265, "eliteBonusViolators1");
        fittingAttributesMap.put(1266, "eliteBonusViolators2");
        fittingAttributesMap.put(1268, "eliteBonusViolatorsRole1");
        fittingAttributesMap.put(1269, "eliteBonusViolatorsRole2");
        fittingAttributesMap.put(1270, "speedBoostFactorBonus");
        fittingAttributesMap.put(1271, "droneBandwidth");
        fittingAttributesMap.put(1279, "eliteBonusViolatorsRole3");
        fittingAttributesMap.put(1280, "eliteBonusInterceptorRole");
        fittingAttributesMap.put(1281, "baseWarpSpeed");
        fittingAttributesMap.put(1282, "implantSetThukker");
        fittingAttributesMap.put(1284, "implantSetSisters");
        fittingAttributesMap.put(1285, "requiredSkill4");
        fittingAttributesMap.put(1286, "requiredSkill4Level");
        fittingAttributesMap.put(1287, "requiredSkill5Level");
        fittingAttributesMap.put(1288, "requiredSkill6Level");
        fittingAttributesMap.put(1289, "requiredSkill5");
        fittingAttributesMap.put(1290, "requiredSkill6");
        fittingAttributesMap.put(1291, "implantSetSyndicate");
        fittingAttributesMap.put(1292, "implantSetORE");
        fittingAttributesMap.put(1293, "implantSetMordus");
        fittingAttributesMap.put(1294, "shipBrokenRepairCostMultiplierBonus");
        fittingAttributesMap.put(1295, "moduleRepairRateBonus");
        fittingAttributesMap.put(1296, "consumptionQuantityBonusPercentage");
        fittingAttributesMap.put(1298, "canFitShipGroup1");
        fittingAttributesMap.put(1299, "canFitShipGroup2");
        fittingAttributesMap.put(1300, "canFitShipGroup3");
        fittingAttributesMap.put(1301, "canFitShipGroup4");
        fittingAttributesMap.put(1302, "canFitShipType1");
        fittingAttributesMap.put(1303, "canFitShipType2");
        fittingAttributesMap.put(1304, "canFitShipType3");
        fittingAttributesMap.put(1305, "canFitShipType4");
        fittingAttributesMap.put(1310, "commandBonusHidden");
        fittingAttributesMap.put(1311, "eliteBonusJumpFreighter1");
        fittingAttributesMap.put(1312, "eliteBonusJumpFreighter2");
        fittingAttributesMap.put(1313, "maxTargetRangeBonusBonus");
        fittingAttributesMap.put(1314, "scanResolutionBonusBonus");
        fittingAttributesMap.put(1315, "maxRangeBonusBonus");
        fittingAttributesMap.put(1316, "trackingSpeedBonusBonus");
        fittingAttributesMap.put(1317, "maxRangeHidden");
        fittingAttributesMap.put(1318, "warpScrambleStrengthHidden");
        fittingAttributesMap.put(1319, "capacitorNeedHidden");
        fittingAttributesMap.put(1320, "commandBonusECM");
        fittingAttributesMap.put(1321, "commandBonusRSD");
        fittingAttributesMap.put(1322, "commandBonusTD");
        fittingAttributesMap.put(1323, "commandBonusTP");
        fittingAttributesMap.put(1324, "massBonusPercentageBonus");
        fittingAttributesMap.put(1325, "speedBoostFactorBonusBonus");
        fittingAttributesMap.put(1326, "speedFactorBonusBonus");
        fittingAttributesMap.put(1327, "warpScrambleRangeBonus");
        fittingAttributesMap.put(1332, "falloffBonusBonus");
        fittingAttributesMap.put(1333, "maxVelocityLimited");
        fittingAttributesMap.put(1334, "maxVelocityActivationLimit");
        fittingAttributesMap.put(1336, "jumpClonesLeft");
        fittingAttributesMap.put(1350, "activationBlockedStrenght");
        fittingAttributesMap.put(1353, "aoeDamageReductionFactor");
        fittingAttributesMap.put(1354, "aoeDamageReductionSensitivity");
        fittingAttributesMap.put(1355, "shipOrcaTractorBeamRangeBonus1");
        fittingAttributesMap.put(1356, "shipOrcaCargoBonusOrca1");
        fittingAttributesMap.put(1357, "shipOrcaTractorBeamVelocityBonus2");
        fittingAttributesMap.put(1358, "shipOrcaForemanBonus");
        fittingAttributesMap.put(1359, "shipOrcaSurveyScannerBonus");
        fittingAttributesMap.put(1361, "eliteIndustrialCovertCloakBonus");
        fittingAttributesMap.put(1366, "subSystemSlot");
        fittingAttributesMap.put(1367, "maxSubSystems");
        fittingAttributesMap.put(1368, "turretHardPointModifier");
        fittingAttributesMap.put(1369, "launcherHardPointModifier");
        fittingAttributesMap.put(1370, "baseScanRange");
        fittingAttributesMap.put(1371, "baseSensorStrength");
        fittingAttributesMap.put(1372, "baseMaxScanDeviation");
        fittingAttributesMap.put(1373, "rangeFactor");
        fittingAttributesMap.put(1374, "hiSlotModifier");
        fittingAttributesMap.put(1375, "medSlotModifier");
        fittingAttributesMap.put(1376, "lowSlotModifier");
        fittingAttributesMap.put(1380, "fitsToShipType");
        fittingAttributesMap.put(1413, "probeCanScanShips");
        fittingAttributesMap.put(1418, "passiveArmorEmDamageResonance");
        fittingAttributesMap.put(1419, "passiveArmorThermalDamageResonance");
        fittingAttributesMap.put(1420, "passiveArmorKineticDamageResonance");
        fittingAttributesMap.put(1421, "passiveArmorExplosiveDamageResonance");
        fittingAttributesMap.put(1422, "passiveShieldExplosiveDamageResonance");
        fittingAttributesMap.put(1423, "passiveShieldEmDamageResonance");
        fittingAttributesMap.put(1424, "passiveShieldKineticDamageResonance");
        fittingAttributesMap.put(1425, "passiveShieldThermalDamageResonance");
        fittingAttributesMap.put(1430, "lightColor");
        fittingAttributesMap.put(1431, "subsystemBonusAmarrEngineering");
        fittingAttributesMap.put(1432, "subsystemBonusAmarrElectronic");
        fittingAttributesMap.put(1433, "subsystemBonusAmarrDefensive");
        fittingAttributesMap.put(1434, "subsystemBonusAmarrOffensive");
        fittingAttributesMap.put(1435, "subsystemBonusAmarrPropulsion");
        fittingAttributesMap.put(1436, "subsystemBonusGallenteEngineering");
        fittingAttributesMap.put(1437, "subsystemBonusGallenteElectronic");
        fittingAttributesMap.put(1438, "subsystemBonusGallenteDefensive");
        fittingAttributesMap.put(1439, "subsystemBonusGallenteOffensive");
        fittingAttributesMap.put(1440, "subsystemBonusGallentePropulsion");
        fittingAttributesMap.put(1441, "subsystemBonusCaldariEngineering");
        fittingAttributesMap.put(1442, "subsystemBonusCaldariElectronic");
        fittingAttributesMap.put(1443, "subsystemBonusCaldariDefensive");
        fittingAttributesMap.put(1444, "subsystemBonusCaldariOffensive");
        fittingAttributesMap.put(1445, "subsystemBonusCaldariPropulsion");
        fittingAttributesMap.put(1446, "subsystemBonusMinmatarEngineering");
        fittingAttributesMap.put(1447, "subsystemBonusMinmatarElectronic");
        fittingAttributesMap.put(1448, "subsystemBonusMinmatarDefensive");
        fittingAttributesMap.put(1449, "subsystemBonusMinmatarOffensive");
        fittingAttributesMap.put(1450, "subsystemBonusMinmatarPropulsion");
        fittingAttributesMap.put(1470, "maxVelocityMultiplier");
        fittingAttributesMap.put(1471, "massMultiplier");
        fittingAttributesMap.put(1503, "shipBonusStrategicCruiserAmarr");
        fittingAttributesMap.put(1504, "shipBonusStrategicCruiserCaldari");
        fittingAttributesMap.put(1505, "shipBonusStrategicCruiserGallente");
        fittingAttributesMap.put(1506, "shipBonusStrategicCruiserMinmatar");
        fittingAttributesMap.put(1507, "subsystemBonusAmarrDefensive2");
        fittingAttributesMap.put(1508, "subsystemBonusAmarrElectronic2");
        fittingAttributesMap.put(1510, "subsystemBonusCaldariOffensive2");
        fittingAttributesMap.put(1511, "subsystemBonusAmarrOffensive2");
        fittingAttributesMap.put(1513, "subsystemBonusCaldariPropulsion2");
        fittingAttributesMap.put(1514, "subsystemBonusCaldariElectronic2");
        fittingAttributesMap.put(1516, "subsystemBonusCaldariDefensive2");
        fittingAttributesMap.put(1517, "subsystemBonusGallenteDefensive2");
        fittingAttributesMap.put(1518, "subsystemBonusGallenteElectronic2");
        fittingAttributesMap.put(1519, "subsystemBonusGallenteEngineering2");
        fittingAttributesMap.put(1520, "subsystemBonusGallentePropulsion2");
        fittingAttributesMap.put(1521, "subsystemBonusGallenteOffensive2");
        fittingAttributesMap.put(1522, "subsystemBonusMinmatarOffensive2");
        fittingAttributesMap.put(1524, "subsystemBonusMinmatarElectronic2");
        fittingAttributesMap.put(1526, "subsystemBonusMinmatarDefensive2");
        fittingAttributesMap.put(1531, "subsystemBonusAmarrOffensive3");
        fittingAttributesMap.put(1532, "subsystemBonusGallenteOffensive3");
        fittingAttributesMap.put(1533, "subsystemBonusCaldariOffensive3");
        fittingAttributesMap.put(1534, "subsystemBonusMinmatarOffensive3");
        fittingAttributesMap.put(1536, "ecmRangeBonus");
        fittingAttributesMap.put(1537, "eliteBonusReconShip3");
        fittingAttributesMap.put(1538, "warpBubbleImmune");
        fittingAttributesMap.put(1539, "warpBubbleImmuneModifier");
        fittingAttributesMap.put(1541, "jumpHarmonicsModifier");
        fittingAttributesMap.put(1544, "maxGroupFitted");
        fittingAttributesMap.put(1545, "dreadnoughtShipBonusM3");
        fittingAttributesMap.put(1547, "rigSize");
        fittingAttributesMap.put(1549, "specialFuelBayCapacity");
        fittingAttributesMap.put(1550, "implantSetImperialNavy");
        fittingAttributesMap.put(1552, "implantSetCaldariNavy");
        fittingAttributesMap.put(1553, "implantSetFederationNavy");
        fittingAttributesMap.put(1554, "implantSetRepublicFleet");
        fittingAttributesMap.put(1555, "fwLpKill");
        fittingAttributesMap.put(1556, "specialOreHoldCapacity");
        fittingAttributesMap.put(1558, "specialMineralHoldCapacity");
        fittingAttributesMap.put(1565, "scanRadarStrengthModifier");
        fittingAttributesMap.put(1566, "scanLadarStrengthModifier");
        fittingAttributesMap.put(1567, "scanGravimetricStrengthModifier");
        fittingAttributesMap.put(1568, "scanMagnetometricStrengthModifier");
        fittingAttributesMap.put(1569, "implantSetLGImperialNavy");
        fittingAttributesMap.put(1570, "implantSetLGFederationNavy");
        fittingAttributesMap.put(1571, "implantSetLGCaldariNavy");
        fittingAttributesMap.put(1572, "implantSetLGRepublicFleet");
        fittingAttributesMap.put(1573, "specialAmmoHoldCapacity");
        fittingAttributesMap.put(1574, "shipBonusATC1");
        fittingAttributesMap.put(1575, "shipBonusATC2");
        fittingAttributesMap.put(1576, "shipBonusATF1");
        fittingAttributesMap.put(1577, "shipBonusATF2");
        fittingAttributesMap.put(1578, "eliteBonusCoverOps3");
        fittingAttributesMap.put(1579, "effectDeactivationDelay");
        fittingAttributesMap.put(1581, "eliteBonusAssaultShips1");
        fittingAttributesMap.put(1589, "siegeModeGallenteDreadnoughtBonus2");
        fittingAttributesMap.put(1619, "webSpeedFactorBonus");
        fittingAttributesMap.put(1646, "specialCommandCenterHoldCapacity");
        fittingAttributesMap.put(1647, "boosterMaxCharAgeHours");
        fittingAttributesMap.put(1653, "specialPlanetaryCommoditiesHoldCapacity");
        fittingAttributesMap.put(1656, "AI_TankingModifierDrone");
        fittingAttributesMap.put(1669, "shipBonusOreIndustrial1");
        fittingAttributesMap.put(1670, "shipBonusOreIndustrial2");
        fittingAttributesMap.put(1688, "shipBonusPirateFaction2");
        fittingAttributesMap.put(1692, "metaGroupID");
        fittingAttributesMap.put(1768, "typeColorScheme");
        fittingAttributesMap.put(1772, "accessDifficultyBonusAbsolutePercent");
        fittingAttributesMap.put(1775, "consumptionQuantityBonusPercent");
        fittingAttributesMap.put(1782, "allowedDroneGroup1");
        fittingAttributesMap.put(1783, "allowedDroneGroup2");
        fittingAttributesMap.put(1785, "isCapitalSize");
        fittingAttributesMap.put(1786, "bcLargeTurretPower");
        fittingAttributesMap.put(1787, "bcLargeTurretCPU");
        fittingAttributesMap.put(1788, "bcLargeTurretCap");
        fittingAttributesMap.put(1795, "reloadTime");
        fittingAttributesMap.put(1798, "disallowAgainstEwImmuneTarget");
        fittingAttributesMap.put(1799, "implantSetChristmas");
        fittingAttributesMap.put(1802, "triageRemoteModuleCapNeed");
        fittingAttributesMap.put(1803, "MWDSignatureRadiusBonus");
        fittingAttributesMap.put(1804, "specialQuafeHoldCapacity");
        fittingAttributesMap.put(1811, "capAttackReflector");
        fittingAttributesMap.put(1813, "titanBonusScalingRadius");
        fittingAttributesMap.put(1816, "neutReflectAmountBonus");
        fittingAttributesMap.put(1817, "nosReflectAmountBonus");
        fittingAttributesMap.put(1820, "baseDefenderAllyCost");
        fittingAttributesMap.put(1821, "skillAllyCostModifierBonus");
        fittingAttributesMap.put(1822, "rookieSETCapBonus");
        fittingAttributesMap.put(1823, "rookieSETDamageBonus");
        fittingAttributesMap.put(1824, "rookieWeaponDisruptionBonus");
        fittingAttributesMap.put(1825, "rookieArmorResistanceBonus");
        fittingAttributesMap.put(1826, "rookieSHTOptimalBonus");
        fittingAttributesMap.put(1827, "rookieMissileKinDamageBonus");
        fittingAttributesMap.put(1828, "rookieECMStrengthBonus");
        fittingAttributesMap.put(1829, "rookieShieldResistBonus");
        fittingAttributesMap.put(1830, "rookieSHTDamageBonus");
        fittingAttributesMap.put(1831, "rookieDroneBonus");
        fittingAttributesMap.put(1832, "rookieDampStrengthBonus");
        fittingAttributesMap.put(1833, "rookieArmorRepBonus");
        fittingAttributesMap.put(1834, "rookieTargetPainterStrengthBonus");
        fittingAttributesMap.put(1835, "rookieShipVelocityBonus");
        fittingAttributesMap.put(1836, "rookieSPTDamageBonus");
        fittingAttributesMap.put(1837, "rookieShieldBoostBonus");
        fittingAttributesMap.put(1838, "miniProfessionRangeBonus");
        fittingAttributesMap.put(1839, "damageDelayDuration");
        fittingAttributesMap.put(1840, "energyTransferAmountBonus");
        fittingAttributesMap.put(1842, "shipBonusOREfrig1");
        fittingAttributesMap.put(1843, "shipBonusOREfrig2");
        fittingAttributesMap.put(1849, "resistanceShiftAmount");
        fittingAttributesMap.put(1851, "sensorStrengthBonus");
        fittingAttributesMap.put(1855, "AI_IgnoreDronesBelowSignatureRadius");
        fittingAttributesMap.put(1856, "massPenaltyReduction");
        fittingAttributesMap.put(1857, "rookieSETTracking");
        fittingAttributesMap.put(1858, "rookieSETOptimal");
        fittingAttributesMap.put(1859, "rookieNosDrain");
        fittingAttributesMap.put(1860, "rookieNeutDrain");
        fittingAttributesMap.put(1861, "rookieWebAmount");
        fittingAttributesMap.put(1862, "rookieLightMissileVelocity");
        fittingAttributesMap.put(1863, "rookieRocketVelocity");
        fittingAttributesMap.put(1864, "rookieDroneMWDspeed");
        fittingAttributesMap.put(1865, "rookieSHTTracking");
        fittingAttributesMap.put(1866, "rookieSHTFalloff");
        fittingAttributesMap.put(1867, "rookieSPTTracking");
        fittingAttributesMap.put(1868, "rookieSPTFalloff");
        fittingAttributesMap.put(1869, "rookieSPTOptimal");
        fittingAttributesMap.put(1870, "covertCloakCPUAdd");
        fittingAttributesMap.put(1871, "covertCloakCPUPenalty");
        fittingAttributesMap.put(1872, "canFitShipGroup5");
        fittingAttributesMap.put(1879, "canFitShipGroup6");
        fittingAttributesMap.put(1880, "canFitShipGroup7");
        fittingAttributesMap.put(1881, "canFitShipGroup8");
        fittingAttributesMap.put(1882, "warfareLinkCPUAdd");
        fittingAttributesMap.put(1883, "warfareLinkCPUPenalty");
        fittingAttributesMap.put(1886, "chargedArmorDamageMultiplier");
        fittingAttributesMap.put(1887, "shipBonusAD1");
        fittingAttributesMap.put(1888, "shipBonusAD2");
        fittingAttributesMap.put(1889, "shipBonusABC2");
        fittingAttributesMap.put(1890, "nondestructible");
        fittingAttributesMap.put(1891, "allowedInCapIndustrialMaintenanceBay");
        fittingAttributesMap.put(1895, "entityCapacitorLevelModifierSmall");
        fittingAttributesMap.put(1896, "entityCapacitorLevelModifierMedium");
        fittingAttributesMap.put(1897, "entityCapacitorLevelModifierLarge");
        fittingAttributesMap.put(1905, "maxScanDeviationModifierModule");
        fittingAttributesMap.put(1906, "scanDurationBonus");
        fittingAttributesMap.put(1907, "scanStrengthBonusModule");
        fittingAttributesMap.put(1909, "virusCoherence");
        fittingAttributesMap.put(1910, "virusStrength");
        fittingAttributesMap.put(1911, "virusElementSlots");
        fittingAttributesMap.put(1915, "virusCoherenceBonus");
        fittingAttributesMap.put(1916, "followsJumpClones");
        fittingAttributesMap.put(1918, "virusStrengthBonus");
        fittingAttributesMap.put(1920, "disallowActivateInForcefield");
        fittingAttributesMap.put(1923, "roleBonusMarauder");
        fittingAttributesMap.put(1924, "eliteBonusCommandShips3");
        fittingAttributesMap.put(1925, "piTaxReductionModifer");
        fittingAttributesMap.put(1932, "implantSetWarpSpeed");
        fittingAttributesMap.put(1934, "deactivateIfOffensive");
        fittingAttributesMap.put(1935, "overloadTrackingModuleStrengthBonus");
        fittingAttributesMap.put(1936, "overloadSensorModuleStrengthBonus");
        fittingAttributesMap.put(1937, "overloadPainterStrengthBonus");
        fittingAttributesMap.put(1942, "eliteBonusExpedition1");
        fittingAttributesMap.put(1943, "eliteBonusExpedition2");
        fittingAttributesMap.put(1944, "canFitShipType5");
        fittingAttributesMap.put(1945, "nosOverride");
        fittingAttributesMap.put(1949, "roleBonusOverheatDST");
        fittingAttributesMap.put(1950, "warpSpeedAdd");
        fittingAttributesMap.put(1958, "dscanImmune");
        fittingAttributesMap.put(1961, "advancedIndustrySkillIndustryJobTimeBonus");
        fittingAttributesMap.put(1970, "disallowInHighSec");
        fittingAttributesMap.put(1971, "jumpFatigueMultiplier");
        fittingAttributesMap.put(1972, "jumpThroughFatigueMultiplier");
        fittingAttributesMap.put(1973, "gateScrambleStatus");
        fittingAttributesMap.put(1978, "resistanceKiller");
        fittingAttributesMap.put(1979, "resistanceKillerHull");
        fittingAttributesMap.put(1982, "manufactureTimePerLevel");
        fittingAttributesMap.put(1983, "freighterBonusO1");
        fittingAttributesMap.put(1984, "freighterBonusO2");
        fittingAttributesMap.put(1985, "stanceSwitchTime");
        fittingAttributesMap.put(1986, "shipBonusTacticalDestroyerAmarr1");
        fittingAttributesMap.put(1987, "shipBonusTacticalDestroyerAmarr2");
        fittingAttributesMap.put(1988, "shipBonusTacticalDestroyerAmarr3");
        fittingAttributesMap.put(1989, "roleBonusTacticalDestroyer1");
        fittingAttributesMap.put(2004, "shipBonusTacticalDestroyerMinmatar1");
        fittingAttributesMap.put(2005, "shipBonusTacticalDestroyerMinmatar2");
        fittingAttributesMap.put(2006, "shipBonusTacticalDestroyerMinmatar3");
        fittingAttributesMap.put(2014, "shipBonusGC3");
        fittingAttributesMap.put(2015, "shipBonusTacticalDestroyerCaldari1");
        fittingAttributesMap.put(2016, "shipBonusTacticalDestroyerCaldari2");
        fittingAttributesMap.put(2017, "shipBonusTacticalDestroyerCaldari3");
        fittingAttributesMap.put(2020, "shipBonusAT");
        fittingAttributesMap.put(2021, "entosisDurationMultiplier");
        fittingAttributesMap.put(2023, "aoeCloudSizeBonusBonus");
        fittingAttributesMap.put(2024, "aoeVelocityBonusBonus");
        fittingAttributesMap.put(2025, "missileVelocityBonusBonus");
        fittingAttributesMap.put(2026, "explosionDelayBonusBonus");
        fittingAttributesMap.put(2027, "shipBonusTacticalDestroyerGallente1");
        fittingAttributesMap.put(2028, "shipBonusTacticalDestroyerGallente2");
        fittingAttributesMap.put(2029, "shipBonusTacticalDestroyerGallente3");
        fittingAttributesMap.put(2033, "speedLimit");
        fittingAttributesMap.put(2041, "entosisCPUAdd");
        fittingAttributesMap.put(2042, "entosisCPUPenalty");
        fittingAttributesMap.put(2043, "roleBonusCBC");
    }

    public static List<Integer> penalisableAttributeIds = Arrays.asList(new Integer[] {
        3, // damage
        9, // hp
        11, // powerOutput
        19, // powerToSpeed
        21, // warpFactor
        37, // maxVelocity
        48, // cpuOutput
        55, // rechargeRate
        63, // accuracyBonus
        66, // durationBonus
        70, // agility
        76, // maxTargetRange
        79, // scanSpeed
        104, // warpScrambleStatus
        109, // kineticDamageResonance
        110, // thermalDamageResonance
        111, // explosiveDamageResonance
        113, // emDamageResonance
        136, // uniformity
        153, // warpCapacitorNeed
        188, // cargoScanResistance
        192, // maxLockedTargets
        207, // miningAmountMultiplier
        208, // scanRadarStrength
        209, // scanLadarStrength
        210, // scanMagnetometricStrength
        211, // scanGravimetricStrength
        216, // capacitorNeedMultiplier
        217, // propulsionGraphicID
        246, // gfxBoosterID
        263, // shieldCapacity
        265, // armorHP
        267, // armorEmDamageResonance
        268, // armorExplosiveDamageResonance
        269, // armorKineticDamageResonance
        270, // armorThermalDamageResonance
        271, // shieldEmDamageResonance
        272, // shieldExplosiveDamageResonance
        273, // shieldKineticDamageResonance
        274, // shieldThermalDamageResonance
        283, // droneCapacity
        292, // damageMultiplierBonus
        310, // cpuNeedBonus
        335, // armorHpBonus
        337, // shieldCapacityBonus
        351, // maxRangeBonus
        435, // maxGangModules
        459, // droneRangeBonus
        479, // shieldRechargeRate
        482, // capacitorCapacity
        484, // shieldUniformity
        511, // shipScanResistance
        524, // armorUniformity
        525, // structureUniformity
        552, // signatureRadius
        564, // scanResolution
        600, // warpSpeedMultiplier
        649, // cloakingCpuNeedBonus
        661, // maxDirectionalVelocity
        662, // minTargetVelDmgMultiplier
        715, // maxOperationalDistance
        716, // maxOperationalUsers
        727, // destroyerROFpenality
        767, // trackingSpeedBonus
        780, // iceHarvestCycleBonus
        797, // maximumRangeCap
        834, // wingCommandBonus
        837, // stealthBomberLauncherPower
        839, // eliteBonusCoverOps2
        849, // canUseCargoInSpace
        853, // advancedAgility
        861, // canJump
        866, // jumpDriveConsumptionType
        867, // jumpDriveRange
        868, // jumpDriveConsumptionAmount
        869, // jumpDriveDuration
        872, // disallowOffensiveModifiers
        874, // advancedCapitalAgility
        898, // jumpDriveCapacitorNeed
        907, // hasShipMaintenanceBay
        908, // shipMaintenanceBayCapacity
        911, // hasFleetHangars
        912, // fleetHangarCapacity
        974, // hullEmDamageResonance
        975, // hullExplosiveDamageResonance
        976, // hullKineticDamageResonance
        977, // hullThermalDamageResonance
        979, // maxJumpClones
        982, // canReceiveCloneJumps
        1001, // jumpPortalConsumptionMassFactor
        1002, // jumpPortalDuration
        1005, // jumpPortalCapacitorNeed
        1034, // covertOpsAndReconOpsCloakModuleDelay
        1035, // covertOpsStealthBomberTargettingDelay
        1132, // upgradeCapacity
        1154, // upgradeSlotsLeft
        1178, // heatCapacityHi
        1179, // heatDissipationRateHi
        1196, // heatDissipationRateMed
        1198, // heatDissipationRateLow
        1199, // heatCapacityMed
        1200, // heatCapacityLow
        1216, // shieldTransportCpuNeedBonus
        1217, // remoteArmorPowerNeedBonus
        1218, // powerTransferPowerNeedBonus
        1219, // droneArmorDamageAmountBonus
        1220, // droneShieldBonusBonus
        1224, // heatGenerationMultiplier
        1234, // surveyScannerRangeBonus
        1235, // cargoScannerRangeBonus
        1236, // commandBonusEffective
        1253, // jumpHarmonics
        1259, // heatAttenuationHi
        1261, // heatAttenuationMed
        1262, // heatAttenuationLow
        1271, // droneBandwidth
        1279, // eliteBonusViolatorsRole3
        1280, // eliteBonusInterceptorRole
        1281, // baseWarpSpeed
        1296, // consumptionQuantityBonusPercentage
        1311, // eliteBonusJumpFreighter1
        1312, // eliteBonusJumpFreighter2
        1355, // shipOrcaTractorBeamRangeBonus1
        1356, // shipOrcaCargoBonusOrca1
        1357, // shipOrcaTractorBeamVelocityBonus2
        1358, // shipOrcaForemanBonus
        1359, // shipOrcaSurveyScannerBonus
        1361, // eliteIndustrialCovertCloakBonus
        1367, // maxSubSystems
        1537, // eliteBonusReconShip3
        1538, // warpBubbleImmune
        1545, // dreadnoughtShipBonusM3
        1549, // specialFuelBayCapacity
        1556, // specialOreHoldCapacity
        1558, // specialMineralHoldCapacity
        1573, // specialAmmoHoldCapacity1
        1646, // specialCommandCenterHoldCapacity
        1653, // specialPlanetaryCommoditiesHoldCapacity
        1782, // allowedDroneGroup1
        1783, // allowedDroneGroup2
        1786, // bcLargeTurretPower
        1787, // bcLargeTurretCPU
        1788, // bcLargeTurretCap
        1803, // MWDSignatureRadiusBonus
        1804, // specialQuafeHoldCapacity
        1813, // titanBonusScalingRadius
        1822, // rookieSETCapBonus
        1823, // rookieSETDamageBonus
        1824, // rookieWeaponDisruptionBonus
        1825, // rookieArmorResistanceBonus
        1826, // rookieSHTOptimalBonus
        1827, // rookieMissileKinDamageBonus
        1828, // rookieECMStrengthBonus
        1829, // rookieShieldResistBonus
        1830, // rookieSHTDamageBonus
        1831, // rookieDroneBonus
        1832, // rookieDampStrengthBonus
        1833, // rookieArmorRepBonus
        1834, // rookieTargetPainterStrengthBonus
        1835, // rookieShipVelocityBonus
        1836, // rookieSPTDamageBonus
        1837, // rookieShieldBoostBonus
        1838, // miniProfessionRangeBonus
        1840, // energyTransferAmountBonus
        1857, // rookieSETTracking
        1858, // rookieSETOptimal
        1859, // rookieNosDrain
        1860, // rookieNeutDrain
        1861, // rookieWebAmount
        1862, // rookieLightMissileVelocity
        1863, // rookieRocketVelocity
        1864, // rookieDroneMWDspeed
        1865, // rookieSHTTracking
        1866, // rookieSHTFalloff
        1867, // rookieSPTTracking
        1868, // rookieSPTFalloff
        1869, // rookieSPTOptimal
        1891, // allowedInCapIndustrialMaintenanceBay
        1918, // virusStrengthBonus
        1923, // roleBonusMarauder
        1924, // eliteBonusCommandShips3
        1942, // eliteBonusExpedition1
        1943, // eliteBonusExpedition2
        1945, // nosOverride
        1949, // roleBonusOverheatDST
        1958, // dscanImmune
        1970, // disallowInHighSec
        1971, // jumpFatigueMultiplier
        1972, // jumpThroughFatigueMultiplier
        1973, // gateScrambleStatus
        1985, // stanceSwitchTime
        2021, // entosisDurationMultiplier
        2042 // entosisCPUPenalty
    });

    public static BiMap<Integer, String> fittingEffectsMap = HashBiMap.create();
    static {
        fittingEffectsMap.put(4, "shieldBoosting");
        fittingEffectsMap.put(9, "missileLaunching");
        fittingEffectsMap.put(10, "targetAttack");
        fittingEffectsMap.put(11, "loPower");
        fittingEffectsMap.put(12, "hiPower");
        fittingEffectsMap.put(13, "medPower");
        fittingEffectsMap.put(16, "online");
        fittingEffectsMap.put(18, "shieldTransfer");
        fittingEffectsMap.put(21, "shieldCapacityBonusOnline");
        fittingEffectsMap.put(25, "capacitorCapacityBonus");
        fittingEffectsMap.put(26, "structureRepair");
        fittingEffectsMap.put(27, "armorRepair");
        fittingEffectsMap.put(31, "energyTransfer");
        fittingEffectsMap.put(34, "projectileFired");
        fittingEffectsMap.put(38, "empWave");
        fittingEffectsMap.put(39, "warpScramble");
        fittingEffectsMap.put(40, "launcherFitted");
        fittingEffectsMap.put(42, "turretFitted");
        fittingEffectsMap.put(46, "shipScan");
        fittingEffectsMap.put(47, "cargoScan");
        fittingEffectsMap.put(48, "powerBooster");
        fittingEffectsMap.put(50, "modifyShieldRechargeRate");
        fittingEffectsMap.put(51, "modifyPowerRechargeRate");
        fittingEffectsMap.put(53, "ecmBurst");
        fittingEffectsMap.put(54, "targetPassively");
        fittingEffectsMap.put(55, "targetHostiles");
        fittingEffectsMap.put(56, "powerOutputMultiply");
        fittingEffectsMap.put(57, "shieldCapacityMultiply");
        fittingEffectsMap.put(58, "capacitorCapacityMultiply");
        fittingEffectsMap.put(59, "cargoCapacityMultiply");
        fittingEffectsMap.put(60, "structureHPMultiply");
        fittingEffectsMap.put(63, "armorHPMultiply");
        fittingEffectsMap.put(67, "miningLaser");
        fittingEffectsMap.put(81, "surveyScan");
        fittingEffectsMap.put(89, "projectileWeaponSpeedMultiply");
        fittingEffectsMap.put(91, "energyWeaponDamageMultiply");
        fittingEffectsMap.put(92, "projectileWeaponDamageMultiply");
        fittingEffectsMap.put(93, "hybridWeaponDamageMultiply");
        fittingEffectsMap.put(95, "energyWeaponSpeedMultiply");
        fittingEffectsMap.put(96, "hybridWeaponSpeedMultiply");
        fittingEffectsMap.put(101, "useMissiles");
        fittingEffectsMap.put(103, "defenderMissileLaunching");
        fittingEffectsMap.put(104, "fofMissileLaunching");
        fittingEffectsMap.put(118, "electronicAttributeModifyOnline");
        fittingEffectsMap.put(127, "torpedoLaunching");
        fittingEffectsMap.put(212, "sensorUpgradesCpuNeedBonusPostPercentCpuLocationShipModulesRequiringSensorUpgrades");
        fittingEffectsMap.put(227, "accerationControlCapNeedBonusPostPercentCapacitorNeedLocationShipGroupAfterburner");
        fittingEffectsMap.put(253, "shadowBarrageDmgMultiplierWithDamageMultiplierPostPercentBarrageDmgMutator");
        fittingEffectsMap.put(254, "shadowBarrageFalloffWithFalloffPostPercentBarrageFalloffMutator");
        fittingEffectsMap.put(263, "barrage");
        fittingEffectsMap.put(271, "hullUpgradesArmorHpBonusPostPercentHpLocationShip");
        fittingEffectsMap.put(272, "repairSystemsDurationBonusPostPercentDurationLocationShipModulesRequiringRepairSystems");
        fittingEffectsMap.put(273, "shieldUpgradesPowerNeedBonusPostPercentPowerLocationShipModulesRequiringShieldUpgrades");
        fittingEffectsMap.put(392, "mechanicHullHpBonusPostPercentHpShip");
        fittingEffectsMap.put(394, "navigationVelocityBonusPostPercentMaxVelocityShip");
        fittingEffectsMap.put(395, "evasiveManeuveringAgilityBonusPostPercentAgilityShip");
        fittingEffectsMap.put(396, "energyGridUpgradesCpuNeedBonusPostPercentCpuLocationShipModulesRequiringEnergyGridUpgrades");
        fittingEffectsMap.put(397, "electronicsCpuOutputBonusPostPercentCpuOutputLocationShipGroupComputer");
        fittingEffectsMap.put(446, "shieldManagementShieldCapacityBonusPostPercentCapacityLocationShipGroupShield");
        fittingEffectsMap.put(485, "energysystemsoperationCapRechargeBonusPostPercentRechargeRateLocationShipGroupCapacitor");
        fittingEffectsMap.put(486, "shieldOperationRechargeratebonusPostPercentRechargeRateLocationShipGroupShield");
        fittingEffectsMap.put(490, "engineeringPowerEngineeringOutputBonusPostPercentPowerOutputLocationShipGroupPowerCore");
        fittingEffectsMap.put(494, "warpDriveOperationWarpCapacitorNeedBonusPostPercentWarpCapacitorNeedLocationShipGroupPropulsion");
        fittingEffectsMap.put(504, "scoutDroneOperationDroneRangeBonusModAddDroneControlDistanceChar");
        fittingEffectsMap.put(536, "cpuMultiplierPostMulCpuOutputShip");
        fittingEffectsMap.put(586, "decreaseTargetSpeed");
        fittingEffectsMap.put(592, "targetArmorRepair");
        fittingEffectsMap.put(596, "ammoInfluenceRange");
        fittingEffectsMap.put(598, "ammoSpeedMultiplier");
        fittingEffectsMap.put(599, "ammoFallofMultiplier");
        fittingEffectsMap.put(600, "ammoTrackingMultiplier");
        fittingEffectsMap.put(607, "cloaking");
        fittingEffectsMap.put(623, "miningDroneOperationMiningAmountBonusPostPercentMiningDroneAmountPercentChar");
        fittingEffectsMap.put(627, "powerIncrease");
        fittingEffectsMap.put(657, "agilityMultiplierEffect");
        fittingEffectsMap.put(670, "antiWarpScramblingPassive");
        fittingEffectsMap.put(699, "signatureAnalysisScanResolutionBonusPostPercentScanResolutionShip");
        fittingEffectsMap.put(710, "speedBoostFactorCalculator");
        fittingEffectsMap.put(712, "speedBoostFactorCalculator2");
        fittingEffectsMap.put(744, "surveyScanspeedBonusPostPercentDurationLocationShipModulesRequiringElectronics");
        fittingEffectsMap.put(763, "missileDMGBonus");
        fittingEffectsMap.put(784, "missileBombardmentMaxFlightTimeBonusPostPercentExplosionDelayOwnerCharModulesRequiringMissileLauncherOperation");
        fittingEffectsMap.put(804, "ammoInfluenceCapNeed");
        fittingEffectsMap.put(836, "skillFreightBonus");
        fittingEffectsMap.put(854, "cloakingScanResolutionMultiplier");
        fittingEffectsMap.put(856, "warpSkillSpeed");
        fittingEffectsMap.put(889, "missileLauncherSpeedMultiplier");
        fittingEffectsMap.put(896, "covertOpsCpuBonus1");
        fittingEffectsMap.put(980, "cloakingWarpSafe");
        fittingEffectsMap.put(1030, "remoteArmorSystemsCapNeedBonusPostPercentCapacitorNeedLocationShipModulesRequiringRemoteArmorSystems");
        fittingEffectsMap.put(1173, "ammoInfluenceEntityFlyRange");
        fittingEffectsMap.put(1190, "iceHarvestCycleTimeModulesRequiringIceHarvesting");
        fittingEffectsMap.put(1200, "miningInfoMultiplier");
        fittingEffectsMap.put(1212, "crystalMiningamountInfo2");
        fittingEffectsMap.put(1253, "speedBoostMassAddition");
        fittingEffectsMap.put(1254, "speedBoostMassSigRad");
        fittingEffectsMap.put(1281, "structuralAnalysisEffect");
        fittingEffectsMap.put(1318, "ewSkillScanStrengthBonus");
        fittingEffectsMap.put(1358, "ewTestEffectJam");
        fittingEffectsMap.put(1372, "ewSkillEwCapNeedSkillLevel");
        fittingEffectsMap.put(1411, "gangBonusSignature");
        fittingEffectsMap.put(1445, "ewSkillRsdMaxRangeBonus");
        fittingEffectsMap.put(1446, "ewSkillTpMaxRangeBonus");
        fittingEffectsMap.put(1448, "ewSkillTdMaxRangeBonus");
        fittingEffectsMap.put(1452, "ewSkillEwMaxRangeBonus");
        fittingEffectsMap.put(1472, "missileSkillAoeCloudSizeBonus");
        fittingEffectsMap.put(1495, "flagshipmultiRelayEffect");
        fittingEffectsMap.put(1500, "shieldOperationSkillBoostCapacitorNeedBonus");
        fittingEffectsMap.put(1510, "gangArmorHardening");
        fittingEffectsMap.put(1546, "gangPropulsionJammingBoost");
        fittingEffectsMap.put(1548, "gangShieldHardening");
        fittingEffectsMap.put(1549, "ewTargetPaint");
        fittingEffectsMap.put(1590, "missileSkillAoeVelocityBonus");
        fittingEffectsMap.put(1634, "capitalShieldOperationSkillCapacitorNeedBonus");
        fittingEffectsMap.put(1635, "capitalRepairSystemsSkillDurationBonus");
        fittingEffectsMap.put(1720, "shieldBoostAmplifier");
        fittingEffectsMap.put(1738, "doHacking");
        fittingEffectsMap.put(1755, "gangAbMwdFactorBoost");
        fittingEffectsMap.put(1764, "missileSkillMissileProjectileVelocityBonus");
        fittingEffectsMap.put(1822, "increaseGallenteNavyBonus");
        fittingEffectsMap.put(1824, "increaseCaldariNavyBonus");
        fittingEffectsMap.put(1825, "increaseAmarrNavyBonus");
        fittingEffectsMap.put(1826, "increaseRepublicFleetBonus");
        fittingEffectsMap.put(1882, "miningYieldMultiplyPercent");
        fittingEffectsMap.put(1959, "armorReinforcerMassAdd");
        fittingEffectsMap.put(2000, "droneRangeBonusAdd");
        fittingEffectsMap.put(2013, "droneMaxVelocityBonus");
        fittingEffectsMap.put(2014, "droneMaxRangeBonus");
        fittingEffectsMap.put(2015, "droneDurabilityShieldCapBonus");
        fittingEffectsMap.put(2016, "droneDurabilityArmorHPBonus");
        fittingEffectsMap.put(2017, "droneDurabilityHPBonus");
        fittingEffectsMap.put(2019, "repairDroneShieldBonusBonus");
        fittingEffectsMap.put(2020, "repairDroneArmorDamageAmountBonus");
        fittingEffectsMap.put(2029, "addToSignatureRadius2");
        fittingEffectsMap.put(2041, "modifyArmorResonancePostPercent");
        fittingEffectsMap.put(2052, "modifyShieldResonancePostPercent");
        fittingEffectsMap.put(2152, "jumpPortalGeneration");
        fittingEffectsMap.put(2231, "scanStrengthBonusPercentActivate");
        fittingEffectsMap.put(2232, "scanStrengthBonusPercentOnline");
        fittingEffectsMap.put(2246, "scanStrengthTargetPercentBonus");
        fittingEffectsMap.put(2247, "dronesMaxActiveDroneBonusModAddMaxActiveActive");
        fittingEffectsMap.put(2252, "covertOpsAndReconOpsCloakModuleDelayBonus");
        fittingEffectsMap.put(2255, "tractorBeamCan");
        fittingEffectsMap.put(2302, "damageControl");
        fittingEffectsMap.put(2303, "energyDestabilizationNew");
        fittingEffectsMap.put(2354, "capitalRemoteArmorRepairerCapNeedBonusSkill");
        fittingEffectsMap.put(2413, "snowBallLaunching");
        fittingEffectsMap.put(2415, "gangShieldBoosterAndTransporterSpeed");
        fittingEffectsMap.put(2418, "gangShieldBoosteAndTransporterCapacitorNeed");
        fittingEffectsMap.put(2432, "energyManagementCapacitorBonusPostPercentCapacityLocationShipGroupCapacitorCapacityBonus");
        fittingEffectsMap.put(2444, "minerCpuUsageMultiplyPercent2");
        fittingEffectsMap.put(2445, "iceMinerCpuUsagePercent");
        fittingEffectsMap.put(2479, "iceHarvestCycleTimeModulesRequiringIceHarvestingOnline");
        fittingEffectsMap.put(2491, "ewSkillEcmBurstRangeBonus");
        fittingEffectsMap.put(2492, "ewSkillEcmBurstCapNeedBonus");
        fittingEffectsMap.put(2644, "increaseSignatureRadiusOnline");
        fittingEffectsMap.put(2645, "scanResolutionMultiplierOnline");
        fittingEffectsMap.put(2646, "maxTargetRangeBonus");
        fittingEffectsMap.put(2663, "rigSlot");
        fittingEffectsMap.put(2670, "sensorBoosterActivePercentage");
        fittingEffectsMap.put(2688, "capNeedBonusEffectLasers");
        fittingEffectsMap.put(2689, "capNeedBonusEffectHybrids");
        fittingEffectsMap.put(2690, "cpuNeedBonusEffectLasers");
        fittingEffectsMap.put(2691, "cpuNeedBonusEffectHybrid");
        fittingEffectsMap.put(2693, "falloffBonusEffectLasers");
        fittingEffectsMap.put(2694, "falloffBonusEffectHybrids");
        fittingEffectsMap.put(2695, "falloffBonusEffectProjectiles");
        fittingEffectsMap.put(2696, "maxRangeBonusEffectLasers");
        fittingEffectsMap.put(2697, "maxRangeBonusEffectHybrids");
        fittingEffectsMap.put(2698, "maxRangeBonusEffectProjectiles");
        fittingEffectsMap.put(2706, "drawbackPowerNeedLasers");
        fittingEffectsMap.put(2707, "drawbackPowerNeedHybrids");
        fittingEffectsMap.put(2708, "drawbackPowerNeedProjectiles");
        fittingEffectsMap.put(2712, "drawbackArmorHP");
        fittingEffectsMap.put(2713, "drawbackCPUOutput");
        fittingEffectsMap.put(2714, "drawbackCPUNeedLaunchers");
        fittingEffectsMap.put(2716, "drawbackSigRad");
        fittingEffectsMap.put(2717, "drawbackMaxVelocity");
        fittingEffectsMap.put(2718, "drawbackShieldCapacity");
        fittingEffectsMap.put(2726, "miningClouds");
        fittingEffectsMap.put(2757, "salvaging");
        fittingEffectsMap.put(2792, "modifyArmorResonancePostPercentPassive");
        fittingEffectsMap.put(2794, "salvagingAccessDifficultyBonusEffectPassive");
        fittingEffectsMap.put(2795, "modifyShieldResonancePostPercentPassive");
        fittingEffectsMap.put(2796, "massReductionBonusPassive");
        fittingEffectsMap.put(2797, "projectileWeaponSpeedMultiplyPassive");
        fittingEffectsMap.put(2798, "projectileWeaponDamageMultiplyPassive");
        fittingEffectsMap.put(2799, "missileLauncherSpeedMultiplierPassive");
        fittingEffectsMap.put(2801, "energyWeaponSpeedMultiplyPassive");
        fittingEffectsMap.put(2802, "hybridWeaponDamageMultiplyPassive");
        fittingEffectsMap.put(2803, "energyWeaponDamageMultiplyPassive");
        fittingEffectsMap.put(2804, "hybridWeaponSpeedMultiplyPassive");
        fittingEffectsMap.put(2837, "armorHPBonusAdd");
        fittingEffectsMap.put(2848, "accessDifficultyBonusModifierRequiringArchaelogy");
        fittingEffectsMap.put(2849, "accessDifficultyBonusModifierRequiringHacking");
        fittingEffectsMap.put(2850, "durationBonusForGroupAfterburner");
        fittingEffectsMap.put(2851, "missileDMGBonusPassive");
        fittingEffectsMap.put(2853, "cloakingTargetingDelayBonusLRSMCloakingPassive");
        fittingEffectsMap.put(2857, "cynosuralGeneration");
        fittingEffectsMap.put(2858, "cloneJumpAccepting");
        fittingEffectsMap.put(2865, "velocityBonusOnline");
        fittingEffectsMap.put(2867, "sentryDroneDamageBonus");
        fittingEffectsMap.put(2868, "armorDamageAmountBonusCapitalArmorRepairers");
        fittingEffectsMap.put(2913, "remoteEcmBurst");
        fittingEffectsMap.put(2971, "bombLaunching");
        fittingEffectsMap.put(3041, "remoteHullRepair");
        fittingEffectsMap.put(3046, "modifyMaxVelocityOfShipPassive");
        fittingEffectsMap.put(3047, "structureHPMultiplyPassive");
        fittingEffectsMap.put(3061, "heatDamageBonus");
        fittingEffectsMap.put(3165, "gangArmorRepairCapReducerSelfAndProjected");
        fittingEffectsMap.put(3167, "gangArmorRepairSpeedAmplifierSelfAndProjected");
        fittingEffectsMap.put(3250, "leech");
        fittingEffectsMap.put(3302, "gangGasHarvesterAndIceHarvesterAndMiningLaserDurationBonus");
        fittingEffectsMap.put(3307, "gangGasHarvesterAndIceHarvesterAndMiningLaserCapNeedBonus");
        fittingEffectsMap.put(3380, "warpDisruptSphere");
        fittingEffectsMap.put(3381, "droneDamageBonusOnline");
        fittingEffectsMap.put(3561, "ewSkillTrackingDisruptionTrackingSpeedBonus");
        fittingEffectsMap.put(3583, "targetMaxTargetRangeAndScanResolutionBonusAssistance");
        fittingEffectsMap.put(3584, "targetMaxTargetRangeAndScanResolutionBonusHostile");
        fittingEffectsMap.put(3586, "ewSkillSignalSuppressionScanResolutionBonus");
        fittingEffectsMap.put(3591, "ewSkillSignalSuppressionMaxTargetRangeBonus");
        fittingEffectsMap.put(3597, "scriptSensorBoosterScanResolutionBonusBonus");
        fittingEffectsMap.put(3598, "scriptSensorBoosterMaxTargetRangeBonusBonus");
        fittingEffectsMap.put(3599, "scriptTrackingComputerTrackingSpeedBonusBonus");
        fittingEffectsMap.put(3600, "scriptTrackingComputerMaxRangeBonusBonus");
        fittingEffectsMap.put(3601, "scriptWarpDisruptionFieldGeneratorSetDisallowInEmpireSpace");
        fittingEffectsMap.put(3602, "scriptDurationBonus");
        fittingEffectsMap.put(3611, "commandBonusECMMultiplyWithCommandBonusHidden");
        fittingEffectsMap.put(3612, "commandBonusRSDMultiplyWithCommandBonusHidden");
        fittingEffectsMap.put(3613, "commandBonusTDMultiplyWithCommandBonusHidden");
        fittingEffectsMap.put(3614, "commandBonusTPMultiplyWithCommandBonusHidden");
        fittingEffectsMap.put(3615, "scriptWarpDisruptionFieldGeneratorSetScriptCapacitorNeedHidden");
        fittingEffectsMap.put(3617, "scriptSignatureRadiusBonusBonus");
        fittingEffectsMap.put(3618, "scriptMassBonusPercentageBonus");
        fittingEffectsMap.put(3619, "scriptSpeedBoostFactorBonusBonus");
        fittingEffectsMap.put(3620, "scriptSpeedFactorBonusBonus");
        fittingEffectsMap.put(3648, "scriptWarpScrambleRangeBonus");
        fittingEffectsMap.put(3655, "gunneryMaxRangeBonusOnline");
        fittingEffectsMap.put(3656, "gunneryTrackingSpeedBonusOnline");
        fittingEffectsMap.put(3657, "shipScanResolutionBonusOnline");
        fittingEffectsMap.put(3659, "shipMaxTargetRangeBonusOnline");
        fittingEffectsMap.put(3660, "shipMaxLockedTargetsBonusAddOnline");
        fittingEffectsMap.put(3674, "jumpPortalGenerationBO");
        fittingEffectsMap.put(3686, "scriptTrackingComputerFalloffBonusBonus");
        fittingEffectsMap.put(3690, "targetGunneryMaxRangeAndTrackingSpeedAndFalloffBonusHostile");
        fittingEffectsMap.put(3726, "agilityMultiplierEffectPassive");
        fittingEffectsMap.put(3727, "velocityBonusPassive");
        fittingEffectsMap.put(3771, "armorHPBonusAddPassive");
        fittingEffectsMap.put(3772, "subSystem");
        fittingEffectsMap.put(3773, "hardPointModifierEffect");
        fittingEffectsMap.put(3774, "slotModifier");
        fittingEffectsMap.put(3782, "powerOutputAddPassive");
        fittingEffectsMap.put(3783, "cpuOutputAddCpuOutputPassive");
        fittingEffectsMap.put(3784, "maxVelocityAddPassive");
        fittingEffectsMap.put(3793, "probeLaunching");
        fittingEffectsMap.put(3797, "droneBandwidthAddPassive");
        fittingEffectsMap.put(3799, "droneCapacityAdddroneCapacityPassive");
        fittingEffectsMap.put(3806, "scanStrengthAddPassive");
        fittingEffectsMap.put(3807, "maxTargetRangeAddPassive");
        fittingEffectsMap.put(3809, "scanResolutionAddPassive");
        fittingEffectsMap.put(3810, "capacityAddPassive");
        fittingEffectsMap.put(3811, "capacitorCapacityAddPassive");
        fittingEffectsMap.put(3831, "shieldCapacityAddPassive");
        fittingEffectsMap.put(3853, "rechargeRateAddPassive");
        fittingEffectsMap.put(3856, "shieldRechargeRateAddPassive");
        fittingEffectsMap.put(3857, "subsystemBonusAmarrPropulsionMaxVelocity");
        fittingEffectsMap.put(3860, "subsystemBonusMinmatarPropulsionMaxVelocity");
        fittingEffectsMap.put(3861, "subsystemBonusMinmatarPropulsionAfterburnerSpeedFactor");
        fittingEffectsMap.put(3863, "subsystemBonusCaldariPropulsionAfterburnerSpeedFactor");
        fittingEffectsMap.put(3864, "subsystemBonusAmarrPropulsionAfterburnerSpeedFactor");
        fittingEffectsMap.put(3865, "subsystemBonusAmarrPropulsionAgility");
        fittingEffectsMap.put(3866, "subsystemBonusCaldariPropulsionAgility");
        fittingEffectsMap.put(3867, "subsystemBonusGallentePropulsionAgility");
        fittingEffectsMap.put(3868, "subsystemBonusMinmatarPropulsionAgility");
        fittingEffectsMap.put(3870, "subsystemBonusGallentePropulsionMWDPenalty");
        fittingEffectsMap.put(3872, "subsystemBonusAmarrPropulsionMWDPenalty");
        fittingEffectsMap.put(3875, "subsystemBonusGallentePropulsionABMWDCapNeed");
        fittingEffectsMap.put(3886, "subsystemBonusGallenteElectronicCPU");
        fittingEffectsMap.put(3887, "subsystemBonusCaldariElectronicCPU");
        fittingEffectsMap.put(3893, "subsystemBonusMinmatarElectronicScanStrengthLADAR");
        fittingEffectsMap.put(3895, "subsystemBonusGallenteElectronicScanStrengthMagnetometric");
        fittingEffectsMap.put(3897, "subsystemBonusCaldariElectronicScanStrengthGravimetric");
        fittingEffectsMap.put(3900, "subsystemBonusAmarrElectronicScanStrengthRADAR");
        fittingEffectsMap.put(3908, "subsystemBonusAmarrDefensiveArmorResistance");
        fittingEffectsMap.put(3922, "subsystemBonusGallenteDefensiveArmorResistance");
        fittingEffectsMap.put(3930, "subsystemBonusMinmatarDefensiveArmorResistance");
        fittingEffectsMap.put(3949, "subsystemBonusMinmatarDefensiveShieldResistance");
        fittingEffectsMap.put(3955, "subsystemBonusCaldariDefensiveShieldResistance");
        fittingEffectsMap.put(3959, "subsystemBonusAmarrDefensiveArmorRepairAmount");
        fittingEffectsMap.put(3961, "subsystemBonusGallenteDefensiveArmorRepairAmount");
        fittingEffectsMap.put(3964, "subsystemBonusCaldariDefensiveShieldBoostAmount");
        fittingEffectsMap.put(3976, "subsystemBonusCaldariDefensiveShieldHP");
        fittingEffectsMap.put(3980, "subsystemBonusGallenteDefensiveArmorHP");
        fittingEffectsMap.put(3982, "subsystemBonusAmarrDefensiveArmorHP");
        fittingEffectsMap.put(4093, "subsystemBonusAmarrOffensiveEnergyWeaponDamageMultiplier");
        fittingEffectsMap.put(4104, "subsystemBonusCaldariOffensiveHybridWeaponMaxRange");
        fittingEffectsMap.put(4106, "subsystemBonusGallenteOffensiveHybridWeaponFalloff");
        fittingEffectsMap.put(4109, "subsystemBonusGallenteOffensiveHybridWeaponDamageMultiplier");
        fittingEffectsMap.put(4111, "subsystemBonusMinmatarOffensiveProjectileWeaponROF");
        fittingEffectsMap.put(4114, "subsystemBonusMinmatarOffensiveProjectileWeaponFalloff");
        fittingEffectsMap.put(4115, "subsystemBonusMinmatarOffensiveProjectileWeaponMaxRange");
        fittingEffectsMap.put(4120, "subsystemBonusCaldariOffensiveHeavyMissileLauncherROF");
        fittingEffectsMap.put(4121, "subsystemBonusCaldariOffensiveHeavyAssaultMissileLauncherROF");
        fittingEffectsMap.put(4122, "subsystemBonusCaldariOffensiveAssaultMissileLauncherROF");
        fittingEffectsMap.put(4152, "subsystemBonusAmarrEngineeringHeatDamageReduction");
        fittingEffectsMap.put(4153, "subsystemBonusCaldariEngineeringHeatDamageReduction");
        fittingEffectsMap.put(4154, "subsystemBonusGallenteEngineeringHeatDamageReduction");
        fittingEffectsMap.put(4155, "subsystemBonusMinmatarEngineeringHeatDamageReduction");
        fittingEffectsMap.put(4156, "subsystemBonusMinmatarEngineeringCapacitorCapacity");
        fittingEffectsMap.put(4158, "subsystemBonusCaldariEngineeringCapacitorCapacity");
        fittingEffectsMap.put(4159, "subsystemBonusAmarrEngineeringCapacitorCapacity");
        fittingEffectsMap.put(4160, "massAddPassive");
        fittingEffectsMap.put(4162, "baseSensorStrengthModifierRequiringAstrometrics");
        fittingEffectsMap.put(4185, "subsystemBonusAmarrOffensiveEnergyWeaponCapacitorNeed");
        fittingEffectsMap.put(4215, "subsystemBonusAmarrOffensive2EnergyWeaponCapacitorNeed");
        fittingEffectsMap.put(4216, "subsystemBonusAmarrElectronicEnergyVampireAmount");
        fittingEffectsMap.put(4217, "subsystemBonusAmarrElectronicEnergyDestabilizerAmount");
        fittingEffectsMap.put(4240, "modifyArmorResonancePassivePreAssignment");
        fittingEffectsMap.put(4247, "modifyShieldResonancePassivePreAssignment");
        fittingEffectsMap.put(4248, "subsystemBonusCaldariOffensive2MissileLauncherKineticDamage");
        fittingEffectsMap.put(4250, "subsystemBonusGallenteOffensiveDroneHP");
        fittingEffectsMap.put(4251, "subsystemBonusMinmatarOffensive2ProjectileWeaponDamageMultiplier");
        fittingEffectsMap.put(4253, "subsystemBonusMinmatarOffensive2ProjectileWeaponROF");
        fittingEffectsMap.put(4257, "subsystemBonusMinmatarOffensiveAssaultMissileLauncherROF");
        fittingEffectsMap.put(4258, "subsystemBonusMinmatarOffensiveHeavyMissileLauncherROF");
        fittingEffectsMap.put(4259, "subsystemBonusMinmatarOffensiveHeavyAssaultMissileLauncherROF");
        fittingEffectsMap.put(4260, "subsystemBonusAmarrEngineeringPowerOutput");
        fittingEffectsMap.put(4261, "subsystemBonusCaldariEngineeringPowerOutput");
        fittingEffectsMap.put(4262, "subsystemBonusGallenteEngineeringPowerOutput");
        fittingEffectsMap.put(4263, "subsystemBonusMinmatarEngineeringPowerOutput");
        fittingEffectsMap.put(4264, "subsystemBonusMinmatarEngineeringCapacitorRecharge");
        fittingEffectsMap.put(4265, "subsystemBonusGallenteEngineeringCapacitorRecharge");
        fittingEffectsMap.put(4266, "subsystemBonusCaldariEngineeringCapacitorRecharge");
        fittingEffectsMap.put(4267, "subsystemBonusAmarrEngineeringCapacitorRecharge");
        fittingEffectsMap.put(4269, "subsystemBonusAmarrElectronic2ScanResolution");
        fittingEffectsMap.put(4270, "subsystemBonusMinmatarElectronic2ScanResolution");
        fittingEffectsMap.put(4271, "subsystemBonusCaldariElectronic2MaxTargetingRange");
        fittingEffectsMap.put(4272, "subsystemBonusGallenteElectronic2MaxTargetingRange");
        fittingEffectsMap.put(4273, "subsystemBonusGallenteElectronicWarpScrambleRange");
        fittingEffectsMap.put(4274, "subsystemBonusMinmatarElectronicStasisWebifierRange");
        fittingEffectsMap.put(4275, "subsystemBonusCaldariPropulsionWarpSpeed");
        fittingEffectsMap.put(4277, "subsystemBonusGallentePropulsion2WarpCapacitor");
        fittingEffectsMap.put(4278, "subsystemBonusGallentePropulsionWarpSpeed");
        fittingEffectsMap.put(4281, "modifyShipAgilityPassivePreAssignment");
        fittingEffectsMap.put(4282, "subsystemBonusGallenteOffensive2HybridWeaponDamageMultiplier");
        fittingEffectsMap.put(4283, "subsystemBonusCaldariOffensive2HybridWeaponDamageMultiplier");
        fittingEffectsMap.put(4286, "subsystemBonusAmarrDefensive2RemoteArmorRepairAmount");
        fittingEffectsMap.put(4288, "subsystemBonusGallenteDefensive2RemoteArmorRepairAmount");
        fittingEffectsMap.put(4290, "subsystemBonusMinmatarDefensive2RemoteShieldTransporterAmount");
        fittingEffectsMap.put(4292, "subsystemBonusCaldariDefensive2RemoteShieldTransporterAmount");
        fittingEffectsMap.put(4317, "subsystemBonusGallenteEngineeringDroneHP");
        fittingEffectsMap.put(4320, "subsystemBonusGallenteEngineering2DroneMWD");
        fittingEffectsMap.put(4321, "subsystemBonusCaldariElectronicECMRange");
        fittingEffectsMap.put(4322, "subsystemBonusAmarrOffensiveDroneDamageMultiplier");
        fittingEffectsMap.put(4327, "subsystemBonusAmarrOffensive3DroneHP");
        fittingEffectsMap.put(4329, "subsystemBonusGallenteOffensive3DroneDamageMultiplier");
        fittingEffectsMap.put(4330, "subsystemBonusAmarrOffensive3EnergyWeaponMaxRange");
        fittingEffectsMap.put(4331, "subsystemBonusCaldariOffensive3HeavyAssaultMissileVelocity");
        fittingEffectsMap.put(4332, "subsystemBonusCaldariOffensive3HeavyMissileVelocity");
        fittingEffectsMap.put(4334, "subsystemBonusCaldariOffensive3EwStrengthGrav");
        fittingEffectsMap.put(4335, "subsystemBonusCaldariOffensive3EwStrengthLadar");
        fittingEffectsMap.put(4336, "subsystemBonusCaldariOffensive3EwStrengthMagn");
        fittingEffectsMap.put(4337, "subsystemBonusCaldariOffensive3EwStrengthRadar");
        fittingEffectsMap.put(4342, "subsystemBonusMinmatarElectronic2MaxTargetingRange");
        fittingEffectsMap.put(4343, "subsystemBonusAmarrElectronic2MaxTargetingRange");
        fittingEffectsMap.put(4344, "subsystemBonusMinmatarDefensiveSignatureRadius");
        fittingEffectsMap.put(4347, "subsystemBonusGallenteOffensive3TurretTracking");
        fittingEffectsMap.put(4351, "subsystemBonusMinmatarOffensive3TurretTracking");
        fittingEffectsMap.put(4359, "subsystemBonusAmarrOffensiveHeavyMissileLauncherROF");
        fittingEffectsMap.put(4360, "subsystemBonusAmarrOffensiveAssaultMissileLauncherROF");
        fittingEffectsMap.put(4361, "subsystemBonusAmarrOffensiveHeavyAssaultMissileLauncherROF");
        fittingEffectsMap.put(4362, "subsystemBonusAmarrOffensive2HAMEmDamage");
        fittingEffectsMap.put(4363, "subsystemBonusAmarrOffensive2HAMThermalDamage");
        fittingEffectsMap.put(4364, "subsystemBonusAmarrOffensive2HAMKineticDamage");
        fittingEffectsMap.put(4365, "subsystemBonusAmarrOffensive2HAMExplosiveDamage");
        fittingEffectsMap.put(4369, "subsystemBonusWarpBubbleImmune");
        fittingEffectsMap.put(4373, "subSystemBonusAmarrDefensiveArmoredWarfare");
        fittingEffectsMap.put(4374, "subSystemBonusCaldariDefensiveSiegeWarfare");
        fittingEffectsMap.put(4375, "subSystemBonusGallenteDefensiveInformationWarfare");
        fittingEffectsMap.put(4376, "subSystemBonusMinmatarDefensiveSkirmishWarfare");
        fittingEffectsMap.put(4389, "subSystemBonusAmarrElectronicScanProbeStrength");
        fittingEffectsMap.put(4390, "subSystemBonusCaldariElectronicScanProbeStrength");
        fittingEffectsMap.put(4391, "subSystemBonusGallenteElectronicScanProbeStrength");
        fittingEffectsMap.put(4392, "subSystemBonusMinmatarElectronicScanProbeStrength");
        fittingEffectsMap.put(4401, "subSystemBonusAmarrElectronic2TractorBeamVelocity");
        fittingEffectsMap.put(4402, "subSystemBonusCaldariElectronic2TractorBeamVelocity");
        fittingEffectsMap.put(4403, "subSystemBonusGallenteElectronic2TractorBeamVelocity");
        fittingEffectsMap.put(4404, "subSystemBonusMinmatarElectronic2TractorBeamVelocity");
        fittingEffectsMap.put(4405, "subSystemBonusMinmatarElectronic2TractorBeamRange");
        fittingEffectsMap.put(4406, "subSystemBonusGallenteElectronic2TractorBeamRange");
        fittingEffectsMap.put(4407, "subSystemBonusCaldariElectronic2TractorBeamRange");
        fittingEffectsMap.put(4408, "subSystemBonusAmarrElectronic2TractorBeamRange");
        fittingEffectsMap.put(4409, "signatureRadiusPreAssignment");
        fittingEffectsMap.put(4410, "subsystemBonusScanProbeLauncherCPU");
        fittingEffectsMap.put(4411, "subSystemBonusGallenteDefensiveInformationWarfareHidden");
        fittingEffectsMap.put(4412, "subsystemBonusOffensiveJumpHarmonics");
        fittingEffectsMap.put(4760, "subsystemBonusCaldariPropulsion2WarpCapacitor2");
        fittingEffectsMap.put(4358, "ecmRangeBonusModuleEffect");
        fittingEffectsMap.put(4489, "superWeaponAmarr");
        fittingEffectsMap.put(4490, "superWeaponCaldari");
        fittingEffectsMap.put(4491, "superWeaponGallente");
        fittingEffectsMap.put(4492, "superWeaponMinmatar");
        fittingEffectsMap.put(4527, "gunneryFalloffBonusOnline");
        fittingEffectsMap.put(4559, "gunneryMaxRangeFalloffTrackingSpeedBonus");
        fittingEffectsMap.put(4560, "targetGunneryMaxRangeFalloffTrackingSpeedBonusAssistance");
        fittingEffectsMap.put(4575, "industrialCoreEffect2");
        fittingEffectsMap.put(4579, "droneRigStasisWebSpeedFactorBonus");
        fittingEffectsMap.put(4773, "hackOrbital");
        fittingEffectsMap.put(4807, "gangInformationWarfareRangeBonusWithEcmBurst");
        fittingEffectsMap.put(4809, "ecmGravimetricStrengthBonusPercent");
        fittingEffectsMap.put(4810, "ecmLadarStrengthBonusPercent");
        fittingEffectsMap.put(4811, "ecmMagnetometricStrengthBonusPercent");
        fittingEffectsMap.put(4812, "ecmRadarStrengthBonusPercent");
        fittingEffectsMap.put(4839, "triageModeEffect3");
        fittingEffectsMap.put(4877, "siegeModeEffect6");
        fittingEffectsMap.put(4893, "triageModeEffect7");
        fittingEffectsMap.put(4894, "maxRangeHiddenPreAssignmentWarpScrambleRange");
        fittingEffectsMap.put(4911, "modifyShieldRechargeRatePassive");
        fittingEffectsMap.put(4921, "microJumpDrive");
        fittingEffectsMap.put(4926, "nosAttackReflect");
        fittingEffectsMap.put(4927, "neutAttackReflect");
        fittingEffectsMap.put(4928, "adaptiveArmorHardener");
        fittingEffectsMap.put(4936, "fueledShieldBoosting");
        fittingEffectsMap.put(4939, "neutReflectAmount");
        fittingEffectsMap.put(4940, "nosReflectAmount");
        fittingEffectsMap.put(4942, "targetBreaker");
        fittingEffectsMap.put(4967, "shieldBoosterDurationBonusShieldSkills");
        fittingEffectsMap.put(5069, "mercoxitCrystalBonus");
        fittingEffectsMap.put(5081, "maxTargetingRangeBonusPostPercentPassive");
        fittingEffectsMap.put(5188, "trackingSpeedBonusEffectHybrids");
        fittingEffectsMap.put(5189, "trackingSpeedBonusEffectLasers");
        fittingEffectsMap.put(5190, "trackingSpeedBonusEffectProjectiles");
        fittingEffectsMap.put(5230, "modifyActiveShieldResonancePostPercent");
        fittingEffectsMap.put(5231, "modifyActiveArmorResonancePostPercent");
        fittingEffectsMap.put(5261, "CovertCloakCPUAddition");
        fittingEffectsMap.put(5262, "covertOpsCloakCpuPenalty");
        fittingEffectsMap.put(5263, "covertCynoCpuPenalty");
        fittingEffectsMap.put(5264, "warfareLinkCPUAddition");
        fittingEffectsMap.put(5265, "warfareLinkCpuPenalty");
        fittingEffectsMap.put(5267, "drawbackRepairSystemsPGNeed");
        fittingEffectsMap.put(5268, "drawbackCapRepPGNeed");
        fittingEffectsMap.put(5275, "fueledArmorRepair");
        fittingEffectsMap.put(5397, "baseMaxScanDeviationModifierModuleOnline2None");
        fittingEffectsMap.put(5398, "systemScanDurationModuleModifier");
        fittingEffectsMap.put(5399, "baseSensorStrengthModifierModule");
        fittingEffectsMap.put(5433, "hackingSkillVirusBonus");
        fittingEffectsMap.put(5437, "archaeologySkillVirusBonus");
        fittingEffectsMap.put(5460, "minigameVirusStrengthBonus");
        fittingEffectsMap.put(5461, "shieldOperationRechargeratebonusPostPercentOnline");
        fittingEffectsMap.put(5551, "gangSensorIntegrity");
        fittingEffectsMap.put(5561, "subSystemBonusAmarrDefensiveInformationWarfare");
        fittingEffectsMap.put(5562, "subSystemBonusAmarrDefensiveInformationWarfareHidden");
        fittingEffectsMap.put(5563, "subSystemBonusAmarrDefensiveSkirmishWarfare");
        fittingEffectsMap.put(5564, "subSystemBonusCaldariDefensiveInformationWarfare");
        fittingEffectsMap.put(5565, "subSystemBonusCaldariDefensiveInformationWarfareHidden");
        fittingEffectsMap.put(5566, "subSystemBonusCaldariDefensiveSkirmishWarfare");
        fittingEffectsMap.put(5567, "subSystemBonusGallenteDefensiveSkirmishWarfare");
        fittingEffectsMap.put(5568, "subSystemBonusGallenteDefensiveArmoredWarfare");
        fittingEffectsMap.put(5569, "subSystemBonusMinmatarDefensiveSiegeWarfare");
        fittingEffectsMap.put(5570, "subSystemBonusMinmatarDefensiveArmoredWarfare");
        fittingEffectsMap.put(5607, "capacitorEmissionSystemskill");
        fittingEffectsMap.put(5765, "droneTrackingComputerBonus");
        fittingEffectsMap.put(5769, "repairDroneHullBonusBonus");
        fittingEffectsMap.put(5788, "marauderModeEffect26");
        fittingEffectsMap.put(5793, "ewSkillTrackingDisruptionRangeDisruptionBonus");
        fittingEffectsMap.put(5801, "gangInformationWarfareSuperiorityAll");
        fittingEffectsMap.put(5841, "droneTrackingEnhancerBonus");
        fittingEffectsMap.put(5842, "droneMWDSpeedBonus");
        fittingEffectsMap.put(5846, "gangMiningLaserIceHarvesterGasHarvesterSurveyScannerMaxRangeBonus");
        fittingEffectsMap.put(5868, "drawbackCargoCapacity");
        fittingEffectsMap.put(5900, "warpSpeedAddition");
        fittingEffectsMap.put(5911, "onlineJumpDriveConsumptionAmountBonusPercentage");
        fittingEffectsMap.put(5934, "warpScrambleBlockMWDWithNPCEffect");
        fittingEffectsMap.put(5945, "cloakingPrototype");
        fittingEffectsMap.put(5951, "drawbackWarpSpeed");
        fittingEffectsMap.put(5994, "resistanceKillerHullAll");
        fittingEffectsMap.put(5995, "resistanceKillerShieldArmorAll");
        fittingEffectsMap.put(6043, "subsystemBonusCaldariDefensiveShieldRechargeRate");
        fittingEffectsMap.put(6063, "entosisLink");
        fittingEffectsMap.put(6110, "missileVelocityBonusOnline");
        fittingEffectsMap.put(6111, "missileExplosionDelayBonusOnline");
        fittingEffectsMap.put(6112, "missileAOECloudSizeBonusOnline");
        fittingEffectsMap.put(6113, "missileAOEVelocityBonusOnline");
        fittingEffectsMap.put(6128, "scriptMissileGuidanceComputerAOECloudSizeBonusBonus");
        fittingEffectsMap.put(6129, "scriptMissileGuidanceComputerAOEVelocityBonusBonus");
        fittingEffectsMap.put(6130, "scriptMissileGuidanceComputerMissileVelocityBonusBonus");
        fittingEffectsMap.put(6131, "scriptMissileGuidanceComputerExplosionDelayBonusBonus");
        fittingEffectsMap.put(6135, "missileGuidanceComputerBonus4");
        fittingEffectsMap.put(6163, "passiveSpeedLimit");
        fittingEffectsMap.put(6171, "entosisCPUAddition");
        fittingEffectsMap.put(6200, "targetSetWarpGateMWDNPCScramble");
    }

    public static BiMap<Integer, String> moduleOverloadEffectsMap = HashBiMap.create();
    static {
        moduleOverloadEffectsMap.put(3001, "overloadRofBonus");
        moduleOverloadEffectsMap.put(3002, "overloadSelfDurationBonus");
        moduleOverloadEffectsMap.put(3025, "overloadSelfDamageBonus");
        moduleOverloadEffectsMap.put(3029, "overloadSelfEmHardeningBonus");
        moduleOverloadEffectsMap.put(3030, "overloadSelfThermalHardeningBonus");
        moduleOverloadEffectsMap.put(3031, "overloadSelfExplosiveHardeningBonus");
        moduleOverloadEffectsMap.put(3032, "overloadSelfKineticHardeningBonus");
        moduleOverloadEffectsMap.put(3035, "overloadSelfHardeningInvulnerabilityBonus");
        moduleOverloadEffectsMap.put(3174, "overloadSelfRangeBonus");
        moduleOverloadEffectsMap.put(3175, "overloadSelfSpeedBonus");
        moduleOverloadEffectsMap.put(3182, "overloadSelfECMStrenghtBonus");
        moduleOverloadEffectsMap.put(3189, "overloadSelfECCMStrenghtBonus");
        moduleOverloadEffectsMap.put(3200, "overloadSelfArmorDamageAmountDurationBonus");
        moduleOverloadEffectsMap.put(3201, "overloadSelfShieldBonusDurationBonus");
        moduleOverloadEffectsMap.put(5754, "overloadSelfTrackingModuleBonus");
        moduleOverloadEffectsMap.put(5757, "overloadSelfSensorModuleBonus");
        moduleOverloadEffectsMap.put(5758, "overloadSelfPainterBonus");
        moduleOverloadEffectsMap.put(6144, "overloadSelfMissileGuidanceBonus5");
    }

    public static BiMap<Integer, String> skillEffectsMap = HashBiMap.create();
    static {
        skillEffectsMap.put(82, "corporationMemberLimitBonus");
        skillEffectsMap.put(132, "skillEffect");
        skillEffectsMap.put(146, "damageMultiplierSkillBonus");
        skillEffectsMap.put(152, "skillBoostDamageMultiplierBonus");
        skillEffectsMap.put(157, "largeHybridTurretDamageMultiplierBonusPostPercentDamageMultiplierLocationShipModulesRequiringLargeHybridTurret");
        skillEffectsMap.put(159, "mediumEnergyTurretDamageMultiplierBonusPostPercentDamageMultiplierLocationShipModulesRequiringMediumEnergyTurret");
        skillEffectsMap.put(160, "mediumHybridTurretDamageMultiplierBonusPostPercentDamageMultiplierLocationShipModulesRequiringMediumHybridTurret");
        skillEffectsMap.put(161, "mediumProjectileTurretDamageMultiplierBonusPostPercentDamageMultiplierLocationShipModulesRequiringMediumProjectileTurret");
        skillEffectsMap.put(162, "largeEnergyTurretDamageMultiplierBonusPostPercentDamageMultiplierLocationShipModulesRequiringLargeEnergyTurret");
        skillEffectsMap.put(163, "rapidFiringSkillBoostRofBonus");
        skillEffectsMap.put(167, "sharpshooterSkillBoostRangeSkillBonus");
        skillEffectsMap.put(172, "smallEnergyTurretDamageMultiplierBonusPostPercentDamageMultiplierLocationShipModulesRequiringSmallEnergyTurret");
        skillEffectsMap.put(173, "smallHybridTurretDamageMultiplierBonusPostPercentDamageMultiplierLocationShipModulesRequiringSmallHybridTurret");
        skillEffectsMap.put(174, "smallProjectileTurretDamageMultiplierBonusPostPercentDamageMultiplierLocationShipModulesRequiringSmallProjectileTurret");
        skillEffectsMap.put(204, "signatureAnalysisSkillBoostScanspeedBonus");
        skillEffectsMap.put(205, "signatureAnalysisScanspeedBonusPostPercentScanSpeedLocationShip");
        skillEffectsMap.put(206, "longRangeTargetingSkillBoostMaxTargetRangeBonus");
        skillEffectsMap.put(208, "propulsionJammingSkillBoostDurationBonus");
        skillEffectsMap.put(211, "sensorUpgradesSkillBoostCpuNeedBonus");
        skillEffectsMap.put(212, "sensorUpgradesCpuNeedBonusPostPercentCpuLocationShipModulesRequiringSensorUpgrades");
        skillEffectsMap.put(213, "targetingSkillBoostMaxTargetBonus");
        skillEffectsMap.put(214, "targetingMaxTargetBonusModAddMaxLockedTargetsLocationChar");
        skillEffectsMap.put(216, "electronicWarfareSkillBoostDurationBonus");
        skillEffectsMap.put(218, "engineeringSkillBoostPowerOutputBonus");
        skillEffectsMap.put(220, "energyManagementSkillBoostCapRechargeBonus");
        skillEffectsMap.put(222, "navigationSkillBoostVelocityBonus");
        skillEffectsMap.put(225, "accerationControlSkillBoostCapNeedBonus");
        skillEffectsMap.put(228, "accerationControlSkillBoostSpeedFBonus");
        skillEffectsMap.put(230, "afterburnerDurationBonusPostPercentDurationLocationShipModulesRequiringAfterburner");
        skillEffectsMap.put(232, "evasiveManuveringSkillBoostAgilityBonus");
        skillEffectsMap.put(234, "warpdriveoperationSkillBoostWarpCapacitorNeedBonus");
        skillEffectsMap.put(239, "corporationManagementSkillBoostCorporationMemberBonus");
        skillEffectsMap.put(243, "highSpeedManuveringSkillBoostCapacitorNeedMultiplier");
        skillEffectsMap.put(244, "highSpeedManuveringCapacitorNeedMultiplierPostPercentCapacitorNeedLocationShipModulesRequiringHighSpeedManuvering");
        skillEffectsMap.put(246, "sensorUpgradesSkillBoostPowerNeedBonus");
        skillEffectsMap.put(251, "hullUpgradesSkillBoostHullHpBonus");
        skillEffectsMap.put(270, "hullUpgradesSkillBoostArmorHpBonus");
        skillEffectsMap.put(271, "hullUpgradesArmorHpBonusPostPercentHpLocationShip");
        skillEffectsMap.put(272, "repairSystemsDurationBonusPostPercentDurationLocationShipModulesRequiringRepairSystems");
        skillEffectsMap.put(273, "shieldUpgradesPowerNeedBonusPostPercentPowerLocationShipModulesRequiringShieldUpgrades");
        skillEffectsMap.put(277, "tacticalshieldManipulationSkillBoostUniformityBonus");
        skillEffectsMap.put(279, "shieldEmmisionSystemsCapNeedBonusPostPercentCapacitorNeedLocationShipModulesRequiringShieldEmmisionSystems");
        skillEffectsMap.put(280, "shieldManagementSkillBoostShieldCapacityBonus");
        skillEffectsMap.put(283, "shieldoperationSkillBoostRechargeratebonus");
        skillEffectsMap.put(287, "controlledBurstsCapNeedBonusPostPercentCapacitorNeedLocationShipModulesRequiringGunnery");
        skillEffectsMap.put(290, "sharpshooterRangeSkillBonusPostPercentMaxRangeLocationShipModulesRequiringGunnery");
        skillEffectsMap.put(297, "skillBoostFalloffBonus");
        skillEffectsMap.put(298, "surgicalStrikeFalloffBonusPostPercentFalloffLocationShipModulesRequiringGunnery");
        skillEffectsMap.put(315, "dronesSkillBoostMaxActiveDroneBonus");
        skillEffectsMap.put(316, "dronesMaxActiveDroneBonusModAddMaxActiveLocationChar");
        skillEffectsMap.put(331, "refineryEfficiencySkillBoostRefiningYieldMutator");
        skillEffectsMap.put(334, "cSOTrainingSkillBoostResearchGangSizeBonus");
        skillEffectsMap.put(335, "cSOTrainingResearchGangSizeBonusModAddMaxResearchGangSizeShip");
        skillEffectsMap.put(336, "battleshipConstructionSkillBoostBattleshipConstructionTimeBonus");
        skillEffectsMap.put(337, "battleshipConstructionBattleshipConstructionTimeBonusPostPercentBattleshipConstructionTimeChar");
        skillEffectsMap.put(338, "cruiserConstructionSkillBoostCruiserConstructionTimeBonus");
        skillEffectsMap.put(339, "cruiserConstructionCruiserConstructionTimeBonusPostPercentCruiserConstructionTimeChar");
        skillEffectsMap.put(340, "frigateConstructionSkillBoostFrigateConstructionTimeBonus");
        skillEffectsMap.put(341, "frigateConstructionFrigateConstructionTimeBonusPostPercentFrigateConstructionTimeChar");
        skillEffectsMap.put(342, "industrialConstructionSkillBoostIndustrialConstructionTimeBonus");
        skillEffectsMap.put(343, "industrialConstructionIndustrialConstructionTimeBonusPostPercentIndustrialConstructionTimeChar");
        skillEffectsMap.put(344, "connectionsSkillBoostConnectionBonusMutator");
        skillEffectsMap.put(346, "criminalConnectionsSkillBoostCriminalConnectionsMutator");
        skillEffectsMap.put(348, "diplomacySkillBoostDiplomacyMutator");
        skillEffectsMap.put(349, "diplomacyDiplomacyMutatorModAddDiplomacyBonusChar");
        skillEffectsMap.put(350, "fasttalkSkillBoostFastTalkMutator");
        skillEffectsMap.put(352, "fasttalkFastTalkMutatorPostPercentFastTalkPercentageChar");
        skillEffectsMap.put(368, "gallenteFrigateSkillBoostCpuOutputBonus");
        skillEffectsMap.put(389, "astrogeologySkillBoostMiningAmountBonus");
        skillEffectsMap.put(391, "astrogeologyMiningAmountBonusPostPercentMiningAmountLocationShipModulesRequiringMining");
        skillEffectsMap.put(392, "mechanicHullHpBonusPostPercentHpShip");
        skillEffectsMap.put(394, "navigationVelocityBonusPostPercentMaxVelocityShip");
        skillEffectsMap.put(395, "evasiveManeuveringAgilityBonusPostPercentAgilityShip");
        skillEffectsMap.put(396, "energyGridUpgradesCpuNeedBonusPostPercentCpuLocationShipModulesRequiringEnergyGridUpgrades");
        skillEffectsMap.put(397, "electronicsCpuOutputBonusPostPercentCpuOutputLocationShipGroupComputer");
        skillEffectsMap.put(399, "connectionsConnectionBonusMutatorModAddConnectionsBonusChar");
        skillEffectsMap.put(402, "criminalConnectionsCriminalConnectionsMutatorModAddCriminalConnectionsBonusChar");
        skillEffectsMap.put(403, "negotiationSkillBoostNegotiationBonus");
        skillEffectsMap.put(404, "negotiationNegotiationBonusPostPercentNegotiationPercentageChar");
        skillEffectsMap.put(406, "socialSkillBoostSocialMutator");
        skillEffectsMap.put(408, "largeProjectileTurretDamageMultiplierBonusPostPercentDamageMultiplierLocationShipModulesRequiringLargeProjectileTurret");
        skillEffectsMap.put(412, "industrySkillBoostManufacturingTimeBonus");
        skillEffectsMap.put(413, "gunnerySkillBoostTurretSpeeBonus");
        skillEffectsMap.put(414, "gunneryTurretSpeeBonusPostPercentSpeedLocationShipModulesRequiringGunnery");
        skillEffectsMap.put(415, "tradeSkillBoostTradePremiumBonus");
        skillEffectsMap.put(416, "tradeTradePremiumBonusPostPercentTradePremiumChar");
        skillEffectsMap.put(425, "industryManufacturingTimeBonusPostPercentManufactureTimeMultiplierChar");
        skillEffectsMap.put(430, "scienceSkillBoostCopySpeedBonus");
        skillEffectsMap.put(431, "scienceCopySpeedBonusPostPercentCopySpeedPercentChar");
        skillEffectsMap.put(432, "researchSkillBoostBlueprintmanufactureTimeBonus");
        skillEffectsMap.put(433, "researchBlueprintmanufactureTimeBonusPostPercentProductionTimeResearchSpeedChar");
        skillEffectsMap.put(435, "refineryEfficiencyRefiningYieldMutatorPostPercentRefiningYieldPercentageChar");
        skillEffectsMap.put(446, "shieldManagementShieldCapacityBonusPostPercentCapacityLocationShipGroupShield");
        skillEffectsMap.put(448, "scoutDroneOperationSkillBoostDroneRangeBonus");
        skillEffectsMap.put(453, "minmatarFrigateSkillLevelPreMulShipBonusMFShip");
        skillEffectsMap.put(460, "caldariFrigateSkillLevelPreMulShipBonusCFShip");
        skillEffectsMap.put(468, "gallenteFrigateSkillLevelPreMulShipBonusGFShip");
        skillEffectsMap.put(476, "amarrFrigateSkillLevelPreMulShipBonusAFShip");
        skillEffectsMap.put(481, "metallurgySkillBoostMineralNeedResearchBonus");
        skillEffectsMap.put(485, "energysystemsoperationCapRechargeBonusPostPercentRechargeRateLocationShipGroupCapacitor");
        skillEffectsMap.put(486, "shieldOperationRechargeratebonusPostPercentRechargeRateLocationShipGroupShield");
        skillEffectsMap.put(490, "engineeringPowerEngineeringOutputBonusPostPercentPowerOutputLocationShipGroupPowerCore");
        skillEffectsMap.put(494, "warpDriveOperationWarpCapacitorNeedBonusPostPercentWarpCapacitorNeedLocationShipGroupPropulsion");
        skillEffectsMap.put(498, "inventionSkillBoostInventionBonus");
        skillEffectsMap.put(500, "amarrCruiserSkillLevelPreMulShipBonusACShip");
        skillEffectsMap.put(504, "scoutDroneOperationDroneRangeBonusModAddDroneControlDistanceChar");
        skillEffectsMap.put(506, "fuelConservationCapNeedBonusPostPercentCapacitorNeedLocationShipModulesRequiringAfterburner");
        skillEffectsMap.put(507, "longRangeTargetingMaxTargetRangeBonusPostPercentMaxTargetRangeLocationShipGroupElectronic");
        skillEffectsMap.put(510, "amarrFrigateSkillLevelPreMulShipBonus2AFShip");
        skillEffectsMap.put(517, "gallenteCruiserSkillLevelPreMulShipBonusGCShip");
        skillEffectsMap.put(520, "caldariCruiserSkillLevelPreMulShipBonusCCShip");
        skillEffectsMap.put(524, "minmatarCruiserSkillLevelPreMulShipBonusMCShip");
        skillEffectsMap.put(526, "minmatarIndustrialSkillLevelPreMulShipBonusMIShip");
        skillEffectsMap.put(530, "caldariIndustrialSkillLevelPreMulShipBonusCIShip");
        skillEffectsMap.put(532, "gallenteIndustrialSkillLevelPreMulShipBonusGIShip");
        skillEffectsMap.put(538, "amarrBattleshipSkillLevelPostMulShipBonusABShip");
        skillEffectsMap.put(541, "caldariBattleshipSkillLevelPreMulShipBonusCBShip");
        skillEffectsMap.put(548, "minmatarBattleshipSkillLevelPreMulShipBonusMBShip");
        skillEffectsMap.put(556, "caldariBattleshipSkillLevelPreMulShipBonus2CBShip");
        skillEffectsMap.put(558, "metallurgyMineralNeedResearchBonusPostPercentMineralNeedResearchSpeedChar");
        skillEffectsMap.put(566, "laboratoryOperationSkillLevelModAddMaxLaborotorySlotsChar");
        skillEffectsMap.put(568, "massProductionSkillLevelModAddManufactureSlotLimitChar");
        skillEffectsMap.put(571, "reverseEngineeringInventionBonusModAddReverseEngineeringChanceLocationChar");
        skillEffectsMap.put(581, "weaponUpgradesCpuNeedBonusPostPercentCpuLocationShipModulesRequiringGunnery");
        skillEffectsMap.put(582, "rapidFiringRofBonusPostPercentSpeedLocationShipModulesRequiringGunnery");
        skillEffectsMap.put(583, "amarrIndustrialSkillLevelPreMulShipBonusAI2Ship");
        skillEffectsMap.put(587, "surgicalStrikeDamageMultiplierBonusPostPercentDamageMultiplierLocationShipGroupEnergyWeapon");
        skillEffectsMap.put(588, "surgicalStrikeDamageMultiplierBonusPostPercentDamageMultiplierLocationShipGroupProjectileWeapon");
        skillEffectsMap.put(589, "surgicalStrikeDamageMultiplierBonusPostPercentDamageMultiplierLocationShipGroupHybridWeapon");
        skillEffectsMap.put(590, "energyPulseWeaponsDurationBonusPostPercentDurationLocationShipModulesRequiringEnergyPulseWeapons");
        skillEffectsMap.put(605, "minmatarBattleshipSkillLevelPreMulShipBonusMB2Ship");
        skillEffectsMap.put(611, "gallenteBattleshipSkillLevelPreMulShipBonusGBShip");
        skillEffectsMap.put(623, "miningDroneOperationMiningAmountBonusPostPercentMiningDroneAmountPercentChar");
        skillEffectsMap.put(624, "standardMissilesSkillBoostMissileVelocityBonus");
        skillEffectsMap.put(638, "shieldDefensiveOperationsShieldCapacityBonusPostPercentShieldCapacityGangShips");
        skillEffectsMap.put(639, "reconOperationsMaxTargetRangeBonusPostPercentMaxTargetRangeGangShips");
        skillEffectsMap.put(658, "missileVelocityBonusGeneric");
        skillEffectsMap.put(660, "missileEMDmgBonus");
        skillEffectsMap.put(661, "missileExplosiveDmgBonus");
        skillEffectsMap.put(662, "missileThermalDmgBonus");
        skillEffectsMap.put(663, "heavyMissilesSkillBoostMaxflightTimeBonus");
        skillEffectsMap.put(668, "missileKineticDmgBonus2");
        skillEffectsMap.put(672, "weaponUpgradesSkillBoostCpu");
        skillEffectsMap.put(675, "weaponUpgradesCpuNeedBonusPostPercentCpuLocationShipModulesRequiringEnergyPulseWeapons");
        skillEffectsMap.put(677, "weaponUpgradesCpuNeedBonusPostPercentCpuLocationShipModulesRequiringMissileLauncherOperation");
        skillEffectsMap.put(682, "gallenteBattleshipSkillLevelPreMulShipBonusGB2Ship");
        skillEffectsMap.put(698, "signatureAnalysisSkillLevelPreMulScanResolutionBonusSelf");
        skillEffectsMap.put(699, "signatureAnalysisScanResolutionBonusPostPercentScanResolutionShip");
        skillEffectsMap.put(703, "interceptorsSkillLevelPreMulEliteBonusInterceptorShip");
        skillEffectsMap.put(704, "covertOpsSkillLevelPreMulEliteBonusCoverOpsShip");
        skillEffectsMap.put(734, "amarrBattleshipSkillLevelPreMulShipBonusAB2Ship");
        skillEffectsMap.put(744, "surveyScanspeedBonusPostPercentDurationLocationShipModulesRequiringElectronics");
        skillEffectsMap.put(751, "gallenteFrigateSkillLevelPreMulShipBonusGF2Ship");
        skillEffectsMap.put(761, "caldariFrigateSkillLevelPreMulShipBonusCF2Ship");
        skillEffectsMap.put(762, "minmatarFrigateSkillLevelPreMulShipBonusMF2Ship");
        skillEffectsMap.put(765, "leadershipEffect");
        skillEffectsMap.put(784, "missileBombardmentMaxFlightTimeBonusPostPercentExplosionDelayOwnerCharModulesRequiringMissileLauncherOperation");
        skillEffectsMap.put(797, "caldariBattleshipSkillLevelPreMulShipBonusCB3Ship");
        skillEffectsMap.put(848, "cloakingTargetingDelayBonusPostPercentCloakingTargetingDelayBonusForShipModulesRequiringCloaking");
        skillEffectsMap.put(850, "cloakingSkillBoostCloakingTargetingDelayBonus");
        skillEffectsMap.put(926, "amarrCruiserSkillLevelPreMulShipBonusAC2Ship");
        skillEffectsMap.put(927, "caldariCruiserSkillLevelPreMulShipBonusCC2Ship");
        skillEffectsMap.put(928, "gallenteCruiserSkillLevelPreMulShipBonusGC2Ship");
        skillEffectsMap.put(929, "minmatarCruiserSkillLevelPreMulShipBonusMC2Ship");
        skillEffectsMap.put(987, "gunshipSkillMultiplier1");
        skillEffectsMap.put(988, "gunshipSkillMultiplier2");
        skillEffectsMap.put(1003, "selfT2SmallLaserPulseDamageBonus");
        skillEffectsMap.put(1004, "selfT2SmallLaserBeamDamageBonus");
        skillEffectsMap.put(1005, "selfT2SmallHybridBlasterDamageBonus");
        skillEffectsMap.put(1006, "selfT2SmallHybridRailDamageBonus");
        skillEffectsMap.put(1007, "selfT2SmallProjectileACDamageBonus");
        skillEffectsMap.put(1008, "selfT2SmallProjectileArtyDamageBonus");
        skillEffectsMap.put(1009, "selfT2MediumLaserPulseDamageBonus");
        skillEffectsMap.put(1010, "selfT2MediumLaserBeamDamageBonus");
        skillEffectsMap.put(1011, "selfT2MediumHybridBlasterDamageBonus");
        skillEffectsMap.put(1012, "selfT2MediumHybridRailDamageBonus");
        skillEffectsMap.put(1013, "selfT2MediumProjectileACDamageBonus");
        skillEffectsMap.put(1014, "selfT2MediumProjectileArtyDamageBonus");
        skillEffectsMap.put(1015, "selfT2LargeLaserPulseDamageBonus");
        skillEffectsMap.put(1016, "selfT2LargeLaserBeamDamageBonus");
        skillEffectsMap.put(1017, "selfT2LargeHybridBlasterDamageBonus");
        skillEffectsMap.put(1018, "selfT2LargeHybridRailDamageBonus");
        skillEffectsMap.put(1019, "selfT2LargeProjectileACDamageBonus");
        skillEffectsMap.put(1020, "selfT2LargeProjectileArtyDamageBonus");
        skillEffectsMap.put(1030, "remoteArmorSystemsCapNeedBonusPostPercentCapacitorNeedLocationShipModulesRequiringRemoteArmorSystems");
        skillEffectsMap.put(1052, "logisticSkillMultiplier1");
        skillEffectsMap.put(1053, "logisticSkillMultiplier2");
        skillEffectsMap.put(1077, "warshipSkillMultiplier2");
        skillEffectsMap.put(1079, "warshipSkillMultiplier1");
        skillEffectsMap.put(1176, "accerationControlSkillAb&MwdSpeedBoost");
        skillEffectsMap.put(1188, "miningBargeSkillLevelPostMulShipBonusORE2Ship");
        skillEffectsMap.put(1190, "iceHarvestCycleTimeModulesRequiringIceHarvesting");
        skillEffectsMap.put(1191, "iceHarvestCycleTimeBonus");
        skillEffectsMap.put(1279, "interceptors2SkillLevelPreMulEliteBonusInterceptorShip");
        skillEffectsMap.put(1282, "eliteIndustrial1SkillLevelPreMulEliteBonusEliteIndustrialShip");
        skillEffectsMap.put(1283, "eliteIndustrial2SkillLevelPreMulEliteBonusEliteIndustrialShip");
        skillEffectsMap.put(1290, "amarrIndustrialSkillLevelPreMulShipBonusAI22Ship");
        skillEffectsMap.put(1291, "caldariIndustrialSkillLevelPreMulShipBonusCI2Ship");
        skillEffectsMap.put(1292, "gallenteIndustrialSkillLevelPreMulShipBonusGI2Ship");
        skillEffectsMap.put(1293, "minmatarIndustrialSkillLevelPreMulShipBonusMI2Ship");
        skillEffectsMap.put(1318, "ewSkillScanStrengthBonus");
        skillEffectsMap.put(1321, "ewSkillScanStrengthBonusSkillLevel");
        skillEffectsMap.put(1360, "ewSkillRsdCapNeedBonusSkillLevel");
        skillEffectsMap.put(1361, "ewSkillTdCapNeedBonusSkillLevel");
        skillEffectsMap.put(1370, "ewSkillTpCapNeedBonusSkillLevel");
        skillEffectsMap.put(1372, "ewSkillEwCapNeedSkillLevel");
        skillEffectsMap.put(1373, "ewSkillTargetPaintingStrengthBonusSkillLevel");
        skillEffectsMap.put(1405, "covertOpsSkillLevelPreMulEliteBonusCoverOpsShip2");
        skillEffectsMap.put(1409, "systemScanDurationSkillAstrometrics");
        skillEffectsMap.put(1410, "propulsionSkillCapNeedBonusSkillLevel");
        skillEffectsMap.put(1445, "ewSkillRsdMaxRangeBonus");
        skillEffectsMap.put(1446, "ewSkillTpMaxRangeBonus");
        skillEffectsMap.put(1448, "ewSkillTdMaxRangeBonus");
        skillEffectsMap.put(1449, "ewSkillRsdFallOffBonus");
        skillEffectsMap.put(1450, "ewSkillTpFallOffBonus");
        skillEffectsMap.put(1451, "ewSkillTdFallOffBonus");
        skillEffectsMap.put(1452, "ewSkillEwMaxRangeBonus");
        skillEffectsMap.put(1453, "ewSkillEwFallOffBonus");
        skillEffectsMap.put(1467, "missileSkillAoeVelocityBonusSkillLevel");
        skillEffectsMap.put(1472, "missileSkillAoeCloudSizeBonus");
        skillEffectsMap.put(1473, "missileSkillAoeCloudSizeBonusSkillLevel");
        skillEffectsMap.put(1499, "shieldOperationSkillBoostCapacitorNeedBonusSkillLevel");
        skillEffectsMap.put(1500, "shieldOperationSkillBoostCapacitorNeedBonus");
        skillEffectsMap.put(1502, "armorTankingGang");
        skillEffectsMap.put(1541, "squadronmultiplyer");
        skillEffectsMap.put(1542, "squadronCommand");
        skillEffectsMap.put(1550, "ewSkillTargetPaintingStrengthBonus");
        skillEffectsMap.put(1581, "jumpDriveSkillsRangeBonus");
        skillEffectsMap.put(1582, "jumpDriveSkillsRangeBonusSkillLevel");
        skillEffectsMap.put(1585, "capitalTurretSkillLaserDamage");
        skillEffectsMap.put(1586, "capitalTurretSkillProjectileDamage");
        skillEffectsMap.put(1587, "capitalTurretSkillHybridDamage");
        skillEffectsMap.put(1588, "capitalLauncherSkillCitadelKineticDamage");
        skillEffectsMap.put(1590, "missileSkillAoeVelocityBonus");
        skillEffectsMap.put(1592, "capitalLauncherSkillCitadelEmDamage");
        skillEffectsMap.put(1593, "capitalLauncherSkillCitadelExplosiveDamage");
        skillEffectsMap.put(1594, "capitalLauncherSkillCitadelThermalDamage");
        skillEffectsMap.put(1595, "missileSkillWarheadUpgradesEmDamageBonus");
        skillEffectsMap.put(1596, "missileSkillWarheadUpgradesExplosiveDamageBonus");
        skillEffectsMap.put(1597, "missileSkillWarheadUpgradesKineticDamageBonus");
        skillEffectsMap.put(1614, "skillAdvancedSpaceshipAgilityBonus");
        skillEffectsMap.put(1616, "skillCapitalShipsAdvancedAgility");
        skillEffectsMap.put(1618, "dreadnoughtBoostShipBonusA1");
        skillEffectsMap.put(1619, "dreadnoughtBoostShipBonusA2");
        skillEffectsMap.put(1620, "dreadnoughtBoostShipBonusC1");
        skillEffectsMap.put(1621, "dreadnoughtBoostShipBonusC2");
        skillEffectsMap.put(1622, "dreadnoughtBoostShipBonusG1");
        skillEffectsMap.put(1623, "dreadnoughtBoostShipBonusG2");
        skillEffectsMap.put(1624, "dreadnoughtBoostShipBonusM1");
        skillEffectsMap.put(1625, "dreadnoughtBoostShipBonusM2");
        skillEffectsMap.put(1634, "capitalShieldOperationSkillCapacitorNeedBonus");
        skillEffectsMap.put(1635, "capitalRepairSystemsSkillDurationBonus");
        skillEffectsMap.put(1638, "skillAdvancedWeaponUpgradesPowerNeedBonus");
        skillEffectsMap.put(1650, "skillSiegeModuleConsumptionQuantityBonus");
        skillEffectsMap.put(1651, "skillCynosural&SiegeConsumQuantityBonusSkillLevel");
        skillEffectsMap.put(1657, "missileSkillWarheadUpgradesThermalDamageBonus");
        skillEffectsMap.put(1659, "freighterA1SkillLevel");
        skillEffectsMap.put(1660, "freighterA2SkillLevel");
        skillEffectsMap.put(1661, "freighterC1SkillLevel");
        skillEffectsMap.put(1663, "freighterC2SkillLevel");
        skillEffectsMap.put(1664, "freighterG1SkillLevel");
        skillEffectsMap.put(1665, "freighterG2SkillLevel");
        skillEffectsMap.put(1666, "freighterM1SkillLevel");
        skillEffectsMap.put(1667, "freighterM2SkillLevel");
        skillEffectsMap.put(1722, "jumpDriveSkillsCapacitorNeedBonus");
        skillEffectsMap.put(1723, "jumpDriveSkillsCapacitorNeedBonusSkillLevel");
        skillEffectsMap.put(1730, "droneDmgBonus");
        skillEffectsMap.put(1763, "missileSkillRapidLauncherRoF");
        skillEffectsMap.put(1764, "missileSkillMissileProjectileVelocityBonus");
        skillEffectsMap.put(1765, "missileSkillMissileProjectileVelocityBonusSkillLevel");
        skillEffectsMap.put(1838, "eliteBargeSkillMultiplier1");
        skillEffectsMap.put(1839, "eliteBargeSkillMultiplier2");
        skillEffectsMap.put(1849, "miningBargeSkillLevelPostMulShipBonusORE3Ship");
        skillEffectsMap.put(1851, "selfRof");
        skillEffectsMap.put(1895, "miningUpgradeCPUReductionBonus");
        skillEffectsMap.put(1897, "shieldCompensationSkillBoostHardeningBonus");
        skillEffectsMap.put(1908, "reconShipSkillMultiplier1");
        skillEffectsMap.put(1909, "reconShipSkillMultiplier2");
        skillEffectsMap.put(2011, "droneMaxVelocitySkillBonus");
        skillEffectsMap.put(2013, "droneMaxVelocityBonus");
        skillEffectsMap.put(2014, "droneMaxRangeBonus");
        skillEffectsMap.put(2017, "droneDurabilityHPBonus");
        skillEffectsMap.put(2018, "damageRepairedSkillBonus");
        skillEffectsMap.put(2019, "repairDroneShieldBonusBonus");
        skillEffectsMap.put(2020, "repairDroneArmorDamageAmountBonus");
        skillEffectsMap.put(2053, "emShieldCompensationHardeningBonusGroupShieldAmp");
        skillEffectsMap.put(2054, "explosiveShieldCompensationHardeningBonusGroupShieldAmp");
        skillEffectsMap.put(2055, "kineticShieldCompensationHardeningBonusGroupShieldAmp");
        skillEffectsMap.put(2056, "thermalShieldCompensationHardeningBonusGroupShieldAmp");
        skillEffectsMap.put(2105, "emArmorCompensationHardeningBonusGroupArmorCoating");
        skillEffectsMap.put(2106, "explosiveArmorCompensationHardeningBonusGroupArmorCoating");
        skillEffectsMap.put(2107, "kineticArmorCompensationHardeningBonusGroupArmorCoating");
        skillEffectsMap.put(2108, "thermicArmorCompensationHardeningBonusGroupArmorCoating");
        skillEffectsMap.put(2109, "emArmorCompensationHardeningBonusGroupEnergized");
        skillEffectsMap.put(2110, "explosiveArmorCompensationHardeningBonusGroupEnergized");
        skillEffectsMap.put(2111, "kineticArmorCompensationHardeningBonusGroupEnergized");
        skillEffectsMap.put(2112, "thermicArmorCompensationHardeningBonusGroupEnergized");
        skillEffectsMap.put(2148, "droneDamageBonusRequringDrones");
        skillEffectsMap.put(2287, "titanAmarrSkillLevel1");
        skillEffectsMap.put(2288, "titanAmarrSkillLevel2");
        skillEffectsMap.put(2289, "titanCaldariSkillLevel1");
        skillEffectsMap.put(2290, "titanCaldariSkillLevel2");
        skillEffectsMap.put(2291, "titanGallenteSkillLevel1");
        skillEffectsMap.put(2292, "titanGallenteSkillLevel2");
        skillEffectsMap.put(2293, "titanMinmatarSkillLevel2");
        skillEffectsMap.put(2294, "titanMinmatarSkillLevel1");
        skillEffectsMap.put(2311, "skillADDmaxJumpClones");
        skillEffectsMap.put(2313, "carrierAmarrSkillLevel1");
        skillEffectsMap.put(2314, "carrierAmarrSkillLevel2");
        skillEffectsMap.put(2315, "carrierAmarrSkillLevel3");
        skillEffectsMap.put(2316, "carrierCaldariSkillLevel1");
        skillEffectsMap.put(2317, "carrierCaldariSkillLevel2");
        skillEffectsMap.put(2318, "carrierCaldariSkillLevel3");
        skillEffectsMap.put(2319, "carrierGallenteSkillLevel1");
        skillEffectsMap.put(2320, "carrierGallenteSkillLevel2");
        skillEffectsMap.put(2321, "carrierGallenteSkillLevel3");
        skillEffectsMap.put(2322, "carrierMinmatarSkillLevel1");
        skillEffectsMap.put(2323, "carrierMinmatarSkillLevel2");
        skillEffectsMap.put(2324, "carrierMinmatarSkillLevel3");
        skillEffectsMap.put(2336, "titanAmarrSkillLevel3");
        skillEffectsMap.put(2337, "titanAmarrSkillLevel4");
        skillEffectsMap.put(2338, "titanCaldariSkillLevel3");
        skillEffectsMap.put(2339, "titanCaldariSkillLevel4");
        skillEffectsMap.put(2340, "titanGallenteSkillLevel3");
        skillEffectsMap.put(2341, "titanGallenteSkillLevel4");
        skillEffectsMap.put(2342, "titanMinmatarSkillLevel3");
        skillEffectsMap.put(2343, "titanMinmatarSkillLevel4");
        skillEffectsMap.put(2344, "carrierAmarrSkillLevel4");
        skillEffectsMap.put(2345, "carrierCaldariSkillLevel4");
        skillEffectsMap.put(2346, "carrierGallenteSkillLevel4");
        skillEffectsMap.put(2354, "capitalRemoteArmorRepairerCapNeedBonusSkill");
        skillEffectsMap.put(2355, "capitalRemoteShieldTransferCapNeedBonusSkill");
        skillEffectsMap.put(2356, "capitalRemoteEnergyTransferCapNeedBonusSkill");
        skillEffectsMap.put(2364, "carrierMinmatarSkillLevel4");
        skillEffectsMap.put(2402, "skillSuperWeaponDmgBonus");
        skillEffectsMap.put(2411, "cloneVatMaxJumpCloneBonusSkillLevel");
        skillEffectsMap.put(2426, "skillInterdictorEliteBonus1SkillLevel");
        skillEffectsMap.put(2427, "skillInterdictorEliteBonus2SkillLevel");
        skillEffectsMap.put(2429, "advancedDroneInterfacingMaxGroupDCUSkillLevel");
        skillEffectsMap.put(2431, "energyManagementSkillBoostCapacitorCapacityBonus");
        skillEffectsMap.put(2432, "energyManagementCapacitorBonusPostPercentCapacityLocationShipGroupCapacitorCapacityBonus");
        skillEffectsMap.put(2434, "fightersDmgBonusSkills");
        skillEffectsMap.put(2437, "miningYieldGangBonusFixed");
        skillEffectsMap.put(2451, "commandShipSkillMultiplier1");
        skillEffectsMap.put(2452, "commandShipSkillMultiplier2");
        skillEffectsMap.put(2456, "miningUpgradeCPUPenaltyReductionModulesRequiringMiningUpgradePercent");
        skillEffectsMap.put(2491, "ewSkillEcmBurstRangeBonus");
        skillEffectsMap.put(2492, "ewSkillEcmBurstCapNeedBonus");
        skillEffectsMap.put(2580, "neurotoxinRecoverySkillBoostChanceBonus");
        skillEffectsMap.put(2589, "modifyBoosterEffectChanceWithBoosterChanceBonusPostPercent");
        skillEffectsMap.put(2590, "naniteControlSkillBoostBoosterAttributeModifier");
        skillEffectsMap.put(2656, "scanStrengthBonusSkillLevelIncrease");
        skillEffectsMap.put(2724, "rigDrawbackBonusEffect");
        skillEffectsMap.put(2725, "rigDrawbackSkillEffect");
        skillEffectsMap.put(2727, "gasCloudHarvestingMaxGroupSkillLevel");
        skillEffectsMap.put(2760, "boosterModifyBoosterArmorPenalties");
        skillEffectsMap.put(2763, "boosterModifyBoosterShieldPenalty");
        skillEffectsMap.put(2766, "boosterModifyBoosterMaxVelocityAndCapacitorPenalty");
        skillEffectsMap.put(2776, "boosterModifyBoosterMissilePenalty");
        skillEffectsMap.put(2778, "boosterModifyBoosterTurretPenalty");
        skillEffectsMap.put(2820, "maxScanDeviationSelfSkillModifier");
        skillEffectsMap.put(2846, "skillBoostTrackingSpeedBonus");
        skillEffectsMap.put(2847, "trackingSpeedBonusPassiveRequiringGunneryTrackingSpeedBonus");
        skillEffectsMap.put(2866, "biologyTimeBonusFixed");
        skillEffectsMap.put(2918, "posStructureControlAmountBonus");
        skillEffectsMap.put(2967, "skillTriageModuleConsumptionQuantityBonus");
        skillEffectsMap.put(2974, "skillRemoteECMDurationBonusSkillLevel");
        skillEffectsMap.put(2979, "skillRemoteHullRepairSystemsCapNeedBonus");
        skillEffectsMap.put(2980, "skillCapitalRemoteHullRepairSystemsCapNeedBonus");
        skillEffectsMap.put(2982, "skillRemoteECMDurationBonus");
        skillEffectsMap.put(3036, "skillBombDeploymentModuleReactivationDelayBonus");
        skillEffectsMap.put(3195, "thermodynamicsSkillLevel");
        skillEffectsMap.put(3196, "thermodynamicsSkillDamageBonus");
        skillEffectsMap.put(3209, "droneDurabilityShieldCapBonus2");
        skillEffectsMap.put(3210, "droneDurabilityArmorHPBonus2");
        skillEffectsMap.put(3254, "tacticalShieldManipulationBonus2");
        skillEffectsMap.put(3262, "oreCapitalShipSkillMultiplier1");
        skillEffectsMap.put(3263, "oreCapitalShipSkillMultiplier2");
        skillEffectsMap.put(3264, "skillIndustrialReconfigurationConsumptionQuantityBonus");
        skillEffectsMap.put(3311, "oreCapitalShipSkillMultiplier3");
        skillEffectsMap.put(3313, "cloneVatMaxJumpCloneBonusSkillNew");
        skillEffectsMap.put(3327, "oreCapitalShipSkillMultiplier4");
        skillEffectsMap.put(3344, "heavyInterdictorsSkillMultiplier1");
        skillEffectsMap.put(3345, "heavyInterdictorsSkillMultiplier2");
        skillEffectsMap.put(3358, "electronicAttackShipsSkillMultiplier1");
        skillEffectsMap.put(3359, "electronicAttackShipsSkillMultiplier2");
        skillEffectsMap.put(3404, "blackOpsSkillMultiplier1");
        skillEffectsMap.put(3405, "blackOpsSkillMultiplier2");
        skillEffectsMap.put(3408, "violatorsSkillMultiplier1");
        skillEffectsMap.put(3409, "violatorsSkillMultiplier2");
        skillEffectsMap.put(3519, "weaponUpgradesCpuNeedBonusPostPercentCpuLocationShipModulesRequiringBombLauncher");
        skillEffectsMap.put(3520, "skillAdvancedWeaponUpgradesPowerNeedBonusBombLaunchers");
        skillEffectsMap.put(3521, "skillNaniteOperationRepairCost");
        skillEffectsMap.put(3522, "skillNaniteOperationRepairCostLevel");
        skillEffectsMap.put(3524, "skillNaniteInterfacingRepairTimeLevel");
        skillEffectsMap.put(3526, "cynosuralTheoryConsumptionBonus");
        skillEffectsMap.put(3527, "skillConsumptionQuantityBonusPercentageSkillLevel");
        skillEffectsMap.put(3532, "skillJumpDriveConsumptionAmountBonusPercentage");
        skillEffectsMap.put(3561, "ewSkillTrackingDisruptionTrackingSpeedBonus");
        skillEffectsMap.put(3586, "ewSkillSignalSuppressionScanResolutionBonus");
        skillEffectsMap.put(3591, "ewSkillSignalSuppressionMaxTargetRangeBonus");
        skillEffectsMap.put(3595, "jumpFreightersSkillMultiplier1");
        skillEffectsMap.put(3596, "jumpFreightersSkillMultiplier2");
        skillEffectsMap.put(3608, "squadronCommandHidden");
        skillEffectsMap.put(3719, "skirmishWarfareAgilityBonus");
        skillEffectsMap.put(3723, "skillNaniteInterfacingRepairTime2");
        skillEffectsMap.put(3741, "zColinSkillOrcaCargoBonusLvl");
        skillEffectsMap.put(3743, "zColinSkillOrcaForemanBonusLvl");
        skillEffectsMap.put(3753, "eliteIndustrialExtenderSkillBonus");
        skillEffectsMap.put(3755, "zColinSkillTransportCovertCloakMod");
        skillEffectsMap.put(3832, "subsystemSkillLevelAmarrDefensive");
        skillEffectsMap.put(3833, "subsystemSkillLevelCaldariDefensive");
        skillEffectsMap.put(3834, "subsystemSkillLevelGallenteDefensive");
        skillEffectsMap.put(3835, "subsystemSkillLevelMinmatarDefensive");
        skillEffectsMap.put(3836, "subsystemSkillLevelMinmatarElectronic");
        skillEffectsMap.put(3837, "subsystemSkillLevelGallenteElectronic");
        skillEffectsMap.put(3838, "subsystemSkillLevelCaldariElectronic");
        skillEffectsMap.put(3839, "subsystemSkillLevelAmarrElectronic");
        skillEffectsMap.put(3840, "subsystemSkillLevelAmarrEngineering");
        skillEffectsMap.put(3841, "subsystemSkillLevelCaldariEngineering");
        skillEffectsMap.put(3842, "subsystemSkillLevelGallenteEngineering");
        skillEffectsMap.put(3843, "subsystemSkillLevelMinmatarEngineering");
        skillEffectsMap.put(3844, "subsystemSkillLevelMinmatarOffensive");
        skillEffectsMap.put(3845, "subsystemSkillLevelGallenteOffensive");
        skillEffectsMap.put(3846, "subsystemSkillLevelCaldariOffensive");
        skillEffectsMap.put(3847, "subsystemSkillLevelAmarrOffensive");
        skillEffectsMap.put(3848, "subsystemSkillLevelAmarrPropulsion");
        skillEffectsMap.put(3849, "subsystemSkillLevelCaldariPropulsion");
        skillEffectsMap.put(3850, "subsystemSkillLevelGallentePropulsion");
        skillEffectsMap.put(3851, "subsystemSkillLevelMinmatarPropulsion");
        skillEffectsMap.put(4161, "baseMaxScanDeviationModifierRequiringAstrometrics");
        skillEffectsMap.put(4162, "baseSensorStrengthModifierRequiringAstrometrics");
        skillEffectsMap.put(4191, "strategicCruiserAmarrSkillLevel1");
        skillEffectsMap.put(4192, "strategicCruiserCaldariSkillLevel1");
        skillEffectsMap.put(4193, "strategicCruiserGallenteSkillLevel1");
        skillEffectsMap.put(4194, "strategicCruiserMinmatarSkillLevel1");
        skillEffectsMap.put(4195, "subsystemSkillLevelAmarrDefensive2");
        skillEffectsMap.put(4196, "subsystemSkillLevelCaldariDefensive2");
        skillEffectsMap.put(4197, "subsystemSkillLevelGallenteDefensive2");
        skillEffectsMap.put(4198, "subsystemSkillLevelMinmatarDefensive2");
        skillEffectsMap.put(4199, "subsystemSkillLevelMinmatarElectronic2");
        skillEffectsMap.put(4200, "subsystemSkillLevelGallenteElectronic2");
        skillEffectsMap.put(4201, "subsystemSkillLevelCaldariElectronic2");
        skillEffectsMap.put(4202, "subsystemSkillLevelAmarrElectronic2");
        skillEffectsMap.put(4203, "subsystemSkillLevelAmarrEngineering2");
        skillEffectsMap.put(4204, "subsystemSkillLevelCaldariEngineering2");
        skillEffectsMap.put(4205, "subsystemSkillLevelGallenteEngineering2");
        skillEffectsMap.put(4206, "subsystemSkillLevelMinmatarEngineering2");
        skillEffectsMap.put(4207, "subsystemSkillLevelMinmatarOffensive2");
        skillEffectsMap.put(4208, "subsystemSkillLevelGallenteOffensive2");
        skillEffectsMap.put(4209, "subsystemSkillLevelCaldariOffensive2");
        skillEffectsMap.put(4210, "subsystemSkillLevelAmarrOffensive2");
        skillEffectsMap.put(4211, "subsystemSkillLevelAmarrPropulsion2");
        skillEffectsMap.put(4212, "subsystemSkillLevelCaldariPropulsion2");
        skillEffectsMap.put(4213, "subsystemSkillLevelGallentePropulsion2");
        skillEffectsMap.put(4214, "subsystemSkillLevelMinmatarPropulsion2");
        skillEffectsMap.put(4338, "subsystemSkillLevelAmarrOffensive3");
        skillEffectsMap.put(4339, "subsystemSkillLevelCaldariOffensive3");
        skillEffectsMap.put(4341, "subsystemSkillLevelGallenteOffensive3");
        skillEffectsMap.put(4350, "subsystemSkillLevelMinmatarOffensive3");
        skillEffectsMap.put(4357, "reconShipMissileVelocitySkillMultiplier");
        skillEffectsMap.put(4430, "dreadnoughtBoostShipBonusM3");
        skillEffectsMap.put(4480, "covertOpsSkillLevelPreMulEliteBonusCoverOpsShip3");
        skillEffectsMap.put(4533, "titanAmarrSkillLevel5");
        skillEffectsMap.put(4536, "titanGallenteSkillLevel5");
        skillEffectsMap.put(4537, "titanMinmatarSkillLevel5");
        skillEffectsMap.put(4539, "titanCaldariSkillLevel5");
        skillEffectsMap.put(4554, "dreadnoughtBoostShipBonusC3");
        skillEffectsMap.put(4555, "capitalLauncherSkillCruiseCitadelEmDamage1");
        skillEffectsMap.put(4556, "capitalLauncherSkillCruiseCitadelExplosiveDamage1");
        skillEffectsMap.put(4557, "capitalLauncherSkillCruiseCitadelKineticDamage1");
        skillEffectsMap.put(4558, "capitalLauncherSkillCruiseCitadelThermalDamage1");
        skillEffectsMap.put(4598, "amarrFrigateSkillLevelPreMulShipBonus3AFShip");
        skillEffectsMap.put(4599, "caldariFrigateSkillLevelPreMulShipBonus3CFShip");
        skillEffectsMap.put(4600, "gallenteFrigateSkillLevelPreMulShipBonus3GFShip");
        skillEffectsMap.put(4601, "minmatarFrigateSkillLevelPreMulShipBonus3MFShip");
        skillEffectsMap.put(4657, "skillFighterBombersDmgBonus");
        skillEffectsMap.put(4665, "oreIndustrialSkillLevelPostMulShipBonusOreIndustrial1Ship");
        skillEffectsMap.put(4666, "oreIndustrialSkillLevelPostMulShipBonusOreIndustrial2Ship");
        skillEffectsMap.put(4801, "salvagingAccessDifficultyBonusMultiplier");
        skillEffectsMap.put(4802, "hackingAccessDifficultyBonusMultiplier");
        skillEffectsMap.put(4803, "archaeologyAccessDifficultyBonusMultiplier");
        skillEffectsMap.put(4804, "dataMiningSkillBoostAccessDifficultyBonusAbsolutePercent");
        skillEffectsMap.put(4805, "ewSkillEcmBurstFalloffBonus");
        skillEffectsMap.put(4813, "skillConsumptionQuantityBonusPercentSkillLevel");
        skillEffectsMap.put(4814, "jumpPortalConsumptionBonusPercentSkill");
        skillEffectsMap.put(4923, "skillMJDdurationBonus");
        skillEffectsMap.put(4945, "skillTargetBreakerDurationBonus2");
        skillEffectsMap.put(4946, "skillTargetBreakerCapNeedBonus2");
        skillEffectsMap.put(4976, "skillReactiveArmorHardenerDurationBonus");
        skillEffectsMap.put(4984, "skillWarAllyCostModifier");
        skillEffectsMap.put(4985, "relationsAllyCostSkillBoost");
        skillEffectsMap.put(5052, "baseDefenderAllyCostCharAssignment");
        skillEffectsMap.put(5137, "miningFrigateSkillLevelPostMulShipBonusORE1frig");
        skillEffectsMap.put(5138, "miningFrigateSkillLevelPostMulShipBonusORE2frig");
        skillEffectsMap.put(5162, "skillReactiveArmorHardenerCapNeedBonus");
        skillEffectsMap.put(5168, "droneSalvageBonus");
        skillEffectsMap.put(5170, "droneSalvageSkillBonus");
        skillEffectsMap.put(5172, "sensorIntegritySkillLevelPreMulSensorStrengthBonusSelf");
        skillEffectsMap.put(5180, "sensorCompensationSensorStrengthBonusGravimetric");
        skillEffectsMap.put(5181, "sensorCompensationSensorStrengthBonusLadar");
        skillEffectsMap.put(5182, "sensorCompensationSensorStrengthBonusMagnetometric");
        skillEffectsMap.put(5183, "sensorCompensationSensorStrengthBonusRadar");
        skillEffectsMap.put(5200, "armorUpgradesSkillBoostMassPenaltyReduction");
        skillEffectsMap.put(5201, "armorUpgradesMassPenaltyReductionBonus");
        skillEffectsMap.put(5276, "destroyerSkillLevelPreMulShipBonusAD1Ship");
        skillEffectsMap.put(5277, "destroyerSkillLevelPreMulShipBonusAD2Ship");
        skillEffectsMap.put(5278, "destroyerSkillLevelPreMulShipBonusCD1Ship");
        skillEffectsMap.put(5279, "destroyerSkillLevelPreMulShipBonusCD2Ship");
        skillEffectsMap.put(5280, "destroyerSkillLevelPreMulShipBonusGD1Ship");
        skillEffectsMap.put(5281, "destroyerSkillLevelPreMulShipBonusGD2Ship");
        skillEffectsMap.put(5282, "destroyerSkillLevelPreMulShipBonusMD1Ship");
        skillEffectsMap.put(5283, "destroyerSkillLevelPreMulShipBonusMD2Ship");
        skillEffectsMap.put(5284, "battlecruiserSkillLevelPreMulShipBonusABC1Ship");
        skillEffectsMap.put(5285, "battlecruiserSkillLevelPreMulShipBonusABC2Ship");
        skillEffectsMap.put(5286, "battlecruiserSkillLevelPreMulShipBonusCBC1Ship");
        skillEffectsMap.put(5287, "battlecruiserSkillLevelPreMulShipBonusCBC2Ship");
        skillEffectsMap.put(5288, "battlecruiserSkillLevelPreMulShipBonusGBC1Ship");
        skillEffectsMap.put(5289, "battlecruiserSkillLevelPreMulShipBonusGBC2Ship");
        skillEffectsMap.put(5290, "battlecruiserSkillLevelPreMulShipBonusMBC1Ship");
        skillEffectsMap.put(5291, "battlecruiserSkillLevelPreMulShipBonusMBC2Ship");
        skillEffectsMap.put(5372, "damageCloudChanceReductionFixed");
        skillEffectsMap.put(5373, "deepCoreMiningSkillBoostDamageCloudChanceReductionMul");
        skillEffectsMap.put(5433, "hackingSkillVirusBonus");
        skillEffectsMap.put(5435, "dataMiningSkillLevelVirusCoherenceBonusModifier2");
        skillEffectsMap.put(5437, "archaeologySkillVirusBonus");
        skillEffectsMap.put(5527, "skillJumpCloneCooldownReduction");
        skillEffectsMap.put(5543, "armoredSquadronCommand");
        skillEffectsMap.put(5544, "informationSquadronCommand");
        skillEffectsMap.put(5545, "informationSquadronCommandHidden");
        skillEffectsMap.put(5547, "siegeSquadronCommand");
        skillEffectsMap.put(5548, "skirmishSquadronCommand");
        skillEffectsMap.put(5549, "miningSquadronCommand");
        skillEffectsMap.put(5571, "commandShipSkillMultiplier3");
        skillEffectsMap.put(5607, "capacitorEmissionSystemskill");
        skillEffectsMap.put(5614, "piCustomsOfficeTaxReduction");
        skillEffectsMap.put(5615, "piTaxReductionModifierSkillModifier");
        skillEffectsMap.put(5769, "repairDroneHullBonusBonus");
        skillEffectsMap.put(5793, "ewSkillTrackingDisruptionRangeDisruptionBonus");
        skillEffectsMap.put(5850, "expeditionFrigateSkillLevelPostMulEliteBonusExpedition1");
        skillEffectsMap.put(5851, "expeditionFrigateSkillLevelPostMulEliteBonusExpedition2");
        skillEffectsMap.put(5903, "advancedIndustryManufacturingTimeBonusPostPercent");
        skillEffectsMap.put(5906, "advancedIndustryCopyTimeBonusPostPercent");
        skillEffectsMap.put(5907, "advancedIndustryInventionTimeBonusPostPercent");
        skillEffectsMap.put(5908, "advancedIndustrySkillBoostAdvancedIndustrySkillIndustryJobTimeBonus");
        skillEffectsMap.put(5909, "advancedIndustryManufactureTimeResearchTimeBonusPostPercent");
        skillEffectsMap.put(5910, "advancedIndustryMaterialResearchTimeBonusPostPercent");
        skillEffectsMap.put(5996, "freighterO1SkillLevel");
        skillEffectsMap.put(5997, "freighterO2SkillLevel");
        skillEffectsMap.put(6003, "tacticalDestroyerAmarrSkillLevel1");
        skillEffectsMap.put(6004, "tacticalDestroyerAmarrSkillLevel2");
        skillEffectsMap.put(6005, "tacticalDestroyerAmarrSkillLevel3");
        skillEffectsMap.put(6022, "reconShipSkillMultiplier3");
        skillEffectsMap.put(6033, "tacticalDestroyerMinmatarSkillLevel1");
        skillEffectsMap.put(6034, "tacticalDestroyerMinmatarSkillLevel2");
        skillEffectsMap.put(6035, "tacticalDestroyerMinmatarSkillLevel3");
        skillEffectsMap.put(6044, "gallenteCruiserSkillLevelPreMulShipBonusGC3Ship");
        skillEffectsMap.put(6078, "tacticalDestroyerCaldariSkillLevel1");
        skillEffectsMap.put(6079, "tacticalDestroyerCaldariSkillLevel2");
        skillEffectsMap.put(6080, "tacticalDestroyerCaldariSkillLevel3");
        skillEffectsMap.put(6145, "tacticalDestroyerGallenteSkillLevel1");
        skillEffectsMap.put(6146, "tacticalDestroyerGallenteSkillLevel2");
        skillEffectsMap.put(6147, "tacticalDestroyerGallenteSkillLevel3");
    }

    public static int toIntValue(double value) {
        return Double.valueOf(Math.ceil(value)).intValue();
    }
}
