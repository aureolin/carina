/**
 * Project Carina - EVE Fitting Engine Android Library
 *
 * Copyright (c) 2015 Aureolin
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package co.aureolin.labs.carina.util;

import android.database.Cursor;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

/**
 * Created by Akinwale on 13/10/2015.
 */
public final class Helper {
    private static final DecimalFormat DECIMAL_FORMAT = new DecimalFormat("#0.00");

    public static final DateFormat HUMAN_DATE_FORMAT = new SimpleDateFormat("d MMM yyyy h:mm a");

    public static final String ISO_DATE_FORMAT_STR = "yyyy-MM-dd HH:mm:ss";

    private static final DateFormat PARSE_DATE_FORMAT = new SimpleDateFormat(ISO_DATE_FORMAT_STR);
    static {
        PARSE_DATE_FORMAT.setLenient(false);
        PARSE_DATE_FORMAT.setTimeZone(TimeZone.getTimeZone("UTC"));
    }

    public static String formatDecimal(double value) {
        return DECIMAL_FORMAT.format(value);
    }

    public static void closeCursor(Cursor cursor) {
        if (cursor != null) {
            cursor.close();
        }
    }

    public static int parseInt(String value, int defaultValue) {
        if (value == null || value.trim().length() == 0) {
            return defaultValue;
        }

        try {
            return Integer.parseInt(value);
        } catch (NumberFormatException e) {
            return defaultValue;
        }
    }

    public static long parseLong(String value, long defaultValue) {
        if (value == null || value.trim().length() == 0) {
            return defaultValue;
        }

        try {
            return Long.parseLong(value);
        } catch (NumberFormatException e) {
            return defaultValue;
        }
    }

    public static double parseDouble(String value, double defaultValue) {
        if (value == null || value.trim().length() == 0) {
            return defaultValue;
        }

        try {
            return Double.parseDouble(value);
        } catch (NumberFormatException e) {
            return defaultValue;
        }
    }

    public static Date parseIsoDate(String value, Date defaultValue) {
        if (value == null || value.trim().length() == 0) {
            return defaultValue;
        }

        try {
            return PARSE_DATE_FORMAT.parse(value);
        } catch (ParseException e) {
            return defaultValue;
        }
    }

    public static boolean isEmpty(String string) {
        return (string == null || string.trim().length() == 0);
    }

    public static String getCommaSeparatedValues(long[] array) {
        if (array == null || array.length == 0) {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        String delim = "";
        for (int i = 0; i < array.length; i++) {
            sb.append(delim).append(String.valueOf(array[i]));
            delim = ",";
        }
        return sb.toString();
    }

    public static String getReadableTimeDifference(Date startDate, Date endDate) {
        return getReadableTimeDifference(startDate, endDate, false);
    }

    public static String getReadableTime(long diffSeconds, boolean withSeconds) {
        if (diffSeconds < 0) {
            return "";
        }

        int numDays = Long.valueOf(diffSeconds / (60 * 60 * 24)).intValue();
        diffSeconds = diffSeconds % (60 * 60 * 24);
        int numHours = Long.valueOf(diffSeconds / (60 * 60)).intValue();
        diffSeconds = diffSeconds % (60 * 60);
        int minutes = Long.valueOf(diffSeconds / 60).intValue();
        diffSeconds = diffSeconds % 60;

        if (numDays == 0 && numHours == 0 && minutes == 0 && withSeconds) {
            return String.format("%ds", diffSeconds);
        } else if (numDays == 0 && numHours == 0) {
            return (withSeconds) ? String.format("%dm %ds", minutes, diffSeconds)
                    : String.format("%dm", minutes);
        } else if (numDays == 0 && numHours != 0) {
            return (withSeconds) ? String.format("%dh %dm %ds", numHours, minutes, diffSeconds)
                    : String.format("%dh %dm", numHours, minutes);
        }

        return (withSeconds) ? String.format("%dd %dh %dm %ds", numDays, numHours, minutes, diffSeconds)
                : String.format("%dd %dh %dm", numDays, numHours, minutes);
    }

    public static String getReadableTimeDifference(Date startDate, Date endDate, boolean withSeconds) {
        long startTs = startDate.getTime();
        long endTs = endDate.getTime();
        long diffSeconds = (int) ((endTs - startTs) / 1000);
        return getReadableTime(diffSeconds, withSeconds);
    }

    public static String romanNumeralForValue(int value) {
        if (value == 1) {
            return "I";
        }
        if (value == 2) {
            return "II";
        }
        if (value == 3) {
            return "III";
        }
        if (value == 4) {
            return "IV";
        }
        if (value == 5) {
            return "V";
        }
        return "";
    }

    public static long[] toPrimitive(List<Long> list) {
        long[] res = new long[list.size()];
        for (int i = 0; i < list.size(); i++) {
            res[i] = list.get(i);
        }
        return res;
    }
}
