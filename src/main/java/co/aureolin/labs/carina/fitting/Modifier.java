/**
 * Project Carina - EVE Fitting Engine Android Library
 *
 * Copyright (c) 2015 Aureolin
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package co.aureolin.labs.carina.fitting;

/**
 * Created by Akinwale on 11/10/2015.
 */
public class Modifier {
    private String domain;

    private String function;

    private long modifiedAttributeId;

    private long modifyingAttributeId;

    private int operator;

    private long skillTypeId;

    public Modifier() {

    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getFunction() {
        return function;
    }

    public void setFunction(String function) {
        this.function = function;
    }

    public long getModifiedAttributeId() {
        return modifiedAttributeId;
    }

    public void setModifiedAttributeId(long modifiedAttributeId) {
        this.modifiedAttributeId = modifiedAttributeId;
    }

    public long getModifyingAttributeId() {
        return modifyingAttributeId;
    }

    public void setModifyingAttributeId(long modifyingAttributeId) {
        this.modifyingAttributeId = modifyingAttributeId;
    }

    public int getOperator() {
        return operator;
    }

    public void setOperator(int operator) {
        this.operator = operator;
    }

    public long getSkillTypeId() {
        return skillTypeId;
    }

    public void setSkillTypeId(long skillTypeId) {
        this.skillTypeId = skillTypeId;
    }

    public static Modifier fromModifierInfo() {
        Modifier modifier = new Modifier();

        return modifier;
    }
}
