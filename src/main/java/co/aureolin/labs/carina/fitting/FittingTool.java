/**
 * Project Carina - EVE Fitting Engine Android Library
 *
 * Copyright (c) 2015 Aureolin
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package co.aureolin.labs.carina.fitting;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import co.aureolin.labs.carina.fitting.Expression.Result;
import co.aureolin.labs.carina.fitting.Expression.ValueType;
import co.aureolin.labs.carina.fitting.exceptions.CannotFitModuleException;
import co.aureolin.labs.carina.fitting.exceptions.CannotUnfitModuleException;
import co.aureolin.labs.carina.fitting.exceptions.ExpressionEvaluationFailedException;
import co.aureolin.labs.carina.fitting.exceptions.InvalidOperatorException;
import co.aureolin.labs.carina.fitting.exceptions.ModuleGroupMaxFittingReachedException;
import co.aureolin.labs.carina.fitting.exceptions.SubSystemSlotOccupiedException;
import co.aureolin.labs.carina.model.Character;
import co.aureolin.labs.carina.model.fitting.Charge;
import co.aureolin.labs.carina.model.fitting.FittingAttribute;
import co.aureolin.labs.carina.model.fitting.FittingCharacter;
import co.aureolin.labs.carina.model.fitting.FittingModel;
import co.aureolin.labs.carina.model.fitting.FittingSkill;
import co.aureolin.labs.carina.model.fitting.Module;
import co.aureolin.labs.carina.model.fitting.Ship;
import co.aureolin.labs.carina.util.CarinaSdeHelper;
import co.aureolin.labs.carina.util.FittingHelper;
import co.aureolin.labs.carina.util.Helper;

/**
 * Created by Akinwale on 11/10/2015.
 */
public class FittingTool {
    private static final String TAG = FittingTool.class.getCanonicalName();

    private static final String CURRENT_SHIP_EXPR_PREFIX = "CurrentShip->";

    private static final String SQL_LOAD_TYPE =
            "SELECT Type.Id, Type.Name, Grp.Id, Grp.Name, Category.Name FROM EN_Types Type "
          + "JOIN EN_Groups Grp ON Grp.Id = Type.GroupId "
          + "JOIN EN_Categories Category ON Category.Id = Grp.CategoryId "
          + "WHERE Type.Id = %d";

    private static final String SQL_LOAD_EFFECTS_FOR_TYPES_FORMAT =
        "SELECT EffectId, EffectName, modifierInfo, preExpression, postExpression, TypeId " +
        "FROM EN_TypeEffects WHERE TypeId IN (%s) AND EffectId NOT IN (%s)";

    private static final String SQL_LOAD_ATTRIBUTES =
            "SELECT AttributeId, AttributeName, AttributeValue, UnitName, UnitDisplayName " +
            "    FROM EN_TypeAttributes WHERE TypeId = %d";

    public static final long skillLevelAttributeId = 280;

    public static final long cpuOutputBonus2AttributeId = 424;

    public static Map<Long, Ship> shipCache = new HashMap<Long, Ship>();

    public static Map<Long, Module> moduleCache = new HashMap<Long, Module>();

    public static Map<Long, Charge> chargeCache = new HashMap<Long, Charge>();

    public static Map<Long, FittingSkill> skillCache = new HashMap<Long, FittingSkill>();

    public static Result evaluate(Expression expr, Expression arg1, Expression arg2, FittingModel src) {
        Log.d(TAG, String.format("Evaluating Expression: %s", expr.toString()));
        switch (expr.getOperand()) {
            case Dogma.Operand.Add: // Simple addition (1)
                if (arg1 != null && arg2 != null) {
                    Result addResult1 = evaluate(arg1, arg1.getArgument1(), arg1.getArgument2(), src);
                    Result addResult2 = evaluate(arg2, arg2.getArgument1(), arg2.getArgument2(), src);
                    double addResultValue = addResult1.getDoubleValue() + addResult2.getDoubleValue();

                    return new Result(String.valueOf(addResultValue), ValueType.DOUBLE);
                }
                break;

            case Dogma.Operand.AddItemMod: // Add Item Modifier - AIM (6)
                if (arg1 != null && arg2 != null) {
                    Result aimResult1 = evaluate(arg1, arg1.getArgument1(), arg1.getArgument2(), src);
                    Result aimResult2 = evaluate(arg2, arg2.getArgument1(), arg2.getArgument2(), src);
                    double aimValue1 = aimResult1.getDoubleValue();
                    double aimValue2 = aimResult2.getDoubleValue();

                    // Get the affected attribute
                    long attributeId = getModifiedAttributeForItemMod(arg1, src);
                    FittingModel modifiedModel = getModifiedModelForItemMod(arg1, src);
                    Dogma.Operator operator = aimResult1.getOperator();
                    operator.setAffectedAttributeId(attributeId);
                    operator.setSrc(src);
                    operator.setModel(modifiedModel);
                    operator.setRemoving(false);

                    double aimNewValue = operator.apply(aimValue1, aimValue2);
                    //updateAffectedValue(arg1, src, aimNewValue, false);

                    return new Result(String.valueOf(aimNewValue), ValueType.DOUBLE);
                }

            case Dogma.Operand.And: // Logical AND (10)
                if (arg1 != null && arg2 != null) {
                    Result andResult1 = evaluate(arg1, arg1.getArgument1(), arg1.getArgument2(), src);
                    if (!andResult1.getBooleanValue() && !expr.getExpressionName().contains("SkillCheck")) { // TODO: Skill check for later?
                        // No need to evaluate second expression if the first returns false
                        return new Result(Result.FALSE, ValueType.BOOLEAN);
                    }

                    if (expr.getExpressionName().contains("SkillCheck")) {
                        // Always return true for skill check
                        andResult1.setValue(Result.TRUE);
                    }
                    Result andResult2 = evaluate(arg2, arg2.getArgument1(), arg2.getArgument2(), src);
                    return new Result((andResult1.getBooleanValue() && andResult2.getBooleanValue()) ?
                            Result.TRUE : Result.FALSE, ValueType.BOOLEAN
                    );
                }
                break;

            case Dogma.Operand.ItemAttribute: // (12) Join target items and attribute into target definition
                if (arg1 != null && arg2 != null) {
                    Result itemAttributeDomain = evaluate(arg1, arg1.getArgument1(), arg1.getArgument2(), src);
                    FittingModel attributeModel = getAttributeModel(itemAttributeDomain.getIntValue(), src);
                    if (attributeModel != null) {
                        return evaluate(arg2, arg2.getArgument1(), arg2.getArgument2(), attributeModel);
                    }
                }
                break;

            case Dogma.Operand.Attack: // Attack (13)
                if (arg1 != null) {
                    return evaluate(arg1, arg1.getArgument1(), arg1.getArgument2(), src);
                }
                break;

            case Dogma.Operand.Splice: // Splice (17)
                if (arg1 != null && arg2 != null) {
                    Result spliceResult1 = evaluate(arg1, arg1.getArgument1(), arg1.getArgument2(), src);
                    Result spliceResult2 = evaluate(arg2, arg2.getArgument1(), arg2.getArgument2(), src);
                    return new Result(true, spliceResult1, spliceResult2);
                }
                break;

            case Dogma.Operand.DecrementByAttribute: // Decrement by attribute (18)
                if (arg1 != null && arg2 != null) {
                    Result decResult1 = evaluate(arg1, arg1.getArgument1(), arg1.getArgument2(), src);
                    Result decResult2 = evaluate(arg2, arg2.getArgument1(), arg2.getArgument2(), src);
                    double decResultValue = decResult1.getDoubleValue() + decResult2.getDoubleValue();
                    return new Result(String.valueOf(decResultValue), ValueType.DOUBLE);
                }
                break;

            case Dogma.Operand.DefineOperator: // Define operator (21)
                Result result = new Result();
                result.setOperator(new Dogma.Operator(expr.getExpressionValue()));
                return result;

            case Dogma.Operand.DefineAttribute: // Define attribute (22)
                long attributeId = expr.getExpressionAttributeId();
                if (src.hasAttribute(attributeId)) {
                    return new Result(String.valueOf(
                            src.getAffectedValue(attributeId)), ValueType.DOUBLE);
                }
                break;

            case Dogma.Operand.DefineBoolean: // Define boolean (23)
                return new Result("1".equals(expr.getExpressionValue()) ?
                        Result.TRUE : Result.FALSE, ValueType.BOOLEAN);

            case Dogma.Operand.DefineLocation: // Define location (24)
                return new Result(String.valueOf(
                        Dogma.Domain.fromExpressionValue(expr.getExpressionValue())), ValueType.INT);

            case Dogma.Operand.DefineInteger: // Define integer (27)
                return new Result(expr.getExpressionValue(), ValueType.INT);

            case Dogma.Operand.OperatorTarget: // Join operator and target (31)
                if (arg1 != null && arg2 != null) {
                    // Result containing operator
                    Result opTargetResult1 = evaluate(arg1, arg1.getArgument1(), arg1.getArgument2(), src);
                    // Argument 2
                    Result opTargetResult2 = evaluate(arg2, arg2.getArgument1(), arg2.getArgument2(), src);
                    opTargetResult2.setOperator(opTargetResult1.getOperator());
                    return opTargetResult2;
                }
                break;

            case Dogma.Operand.Eq: // Equals (33)
                if (arg1 != null && arg2 != null) {
                    Result gtResult1 = evaluate(arg1, arg1.getArgument1(), arg1.getArgument2(), src);
                    Result gtResult2 = evaluate(arg2, arg2.getArgument1(), arg2.getArgument2(), src);
                    return new Result((gtResult1.getDoubleValue() == gtResult2.getDoubleValue()) ?
                            Result.TRUE : Result.FALSE, ValueType.BOOLEAN);
                }
                break;

            case Dogma.Operand.ItemAttributeCondition: // Item Attribute Condition (35)
                if (arg1 != null && arg2 != null) {
                    Result locationResult = evaluate(arg1, arg1.getArgument1(), arg1.getArgument2(), src);
                    FittingModel attributeModel = getAttributeModel(locationResult.getIntValue(), src);
                    if (attributeModel != null) {
                        return evaluate(arg2, arg2.getArgument1(), arg2.getArgument2(), attributeModel);
                    }
                }
                break;

            case Dogma.Operand.Gt: // Greater than (38)
                if (arg1 != null && arg2 != null) {
                    Result gtResult1 = evaluate(arg1, arg1.getArgument1(), arg1.getArgument2(), src);
                    Result gtResult2 = evaluate(arg2, arg2.getArgument1(), arg2.getArgument2(), src);
                    return new Result((gtResult1.getDoubleValue() > gtResult2.getDoubleValue()) ?
                            Result.TRUE : Result.FALSE, ValueType.BOOLEAN);
                }
                break;

            case Dogma.Operand.Gte: // Greater than or equal to (39)
                if (arg1 != null && arg2 != null) {
                    Result gteResult1 = evaluate(arg1, arg1.getArgument1(), arg1.getArgument2(), src);
                    Result gteResult2 = evaluate(arg2, arg2.getArgument1(), arg2.getArgument2(), src);
                    return new Result((gteResult1.getDoubleValue() >= gteResult2.getDoubleValue()) ?
                            Result.TRUE : Result.FALSE, ValueType.BOOLEAN);
                }
                break;

            case Dogma.Operand.IfThen: // If... Then (41)
                if (arg1 != null && arg2 != null) {
                    Result ifResult = evaluate(arg1, arg1.getArgument1(), arg1.getArgument2(), src);
                    Result thenResult = evaluate(arg2, arg2.getArgument1(), arg2.getArgument2(), src);
                    if (ifResult.getBooleanValue()) {
                        return thenResult;
                    }

                    // If condition evaluated to false, return an empty result
                    return emptyResult();
                }
                break;

            case Dogma.Operand.IncrementByAttributeValue: // Increment attribute by another (42)
                if (arg1 != null && arg2 != null) {
                    Result incResult1 = evaluate(arg1, arg1.getArgument1(), arg1.getArgument2(), src);
                    Result incResult2 = evaluate(arg2, arg2.getArgument1(), arg2.getArgument2(), src);
                    double incResultValue = incResult1.getDoubleValue() + incResult2.getDoubleValue();
                    return new Result(String.valueOf(incResultValue), ValueType.DOUBLE);
                }
                break;

            case Dogma.Operand.Or: // Logical OR (52)
                if (arg1 != null && arg2 != null) {
                    Result orResult1 = evaluate(arg1, arg1.getArgument1(), arg1.getArgument2(), src);
                    if (orResult1.getBooleanValue()) {
                        // No need to evaluate second expression if the first returns true
                        return new Result(Result.TRUE, ValueType.BOOLEAN);
                    }

                    Result orResult2 = evaluate(arg2, arg2.getArgument1(), arg2.getArgument2(), src);
                    return new Result((orResult1.getBooleanValue() && orResult2.getBooleanValue()) ?
                            Result.TRUE : Result.FALSE, ValueType.BOOLEAN);
                }
                break;

            case Dogma.Operand.RemoveItemMod: // Remove Item Modifier - RIM (58)
                if (arg1 != null && arg2 != null) {
                    Result rimResult1 = evaluate(arg1, arg1.getArgument1(), arg1.getArgument2(), src);
                    Result rimResult2 = evaluate(arg2, arg2.getArgument1(), arg2.getArgument2(), src);
                    double rimValue1 = rimResult1.getDoubleValue();
                    double rimValue2 = rimResult2.getDoubleValue();

                    long rimAttributeId = getModifiedAttributeForItemMod(arg1, src);
                    FittingModel modifiedModel = getModifiedModelForItemMod(arg1, src);
                    Dogma.Operator operator = rimResult1.getOperator();
                    operator.setAffectedAttributeId(rimAttributeId);
                    operator.setSrc(src);
                    operator.setModel(modifiedModel);
                    operator.setRemoving(true);

                    double rimNewValue = rimResult1.getOperator().apply(rimValue1, -rimValue2);
                    //updateAffectedValue(arg1, src, rmNewValue, true);

                    return new Result(String.valueOf(rimNewValue), ValueType.DOUBLE);
                }
                break;

            default: // Return an empty result if operand is not recognised
                return emptyResult();
        }

        throw new ExpressionEvaluationFailedException("Expression evaluation failed.");
    }

    public static Result emptyResult() {
        Result result = new Result();
        result.setEmpty(true);
        return result;
    }

    public static FittingModel getAttributeModel(int domain, FittingModel model) {
        if (domain == Dogma.Domain.Self) {
            return model;
        }

        if (domain == Dogma.Domain.Ship && model.getFittingItemType() != FittingModel.TYPE_SHIP) {
            if (model.getCurrentShip() == null) {
                Log.d(TAG, "Module not fitted to ship.");
                return null;
                //throw new IllegalStateException("Module not fitted to ship.");
            }
            return model.getCurrentShip();
        }

        // TODO: handle other domains necessary?
        return model;
    }

    public static long getModifiedAttributeForItemMod(Expression expr, FittingModel src) {
        String attributeName = null;
        long modifiedAttributeId = 0;
        do {
            Expression[] args = new Expression[]{expr.getArgument1(), expr.getArgument2()};
            for (int i = 0; i < args.length; i++) {
                Expression arg = args[i];
                if (arg != null && arg.getOperand() == Dogma.Operand.ItemAttribute) {
                    attributeName = (arg.getArgument2() != null) ?
                            arg.getArgument2().getExpressionName() : null;
                    if (FittingHelper.fittingAttributesMap.containsValue(attributeName)) {
                        modifiedAttributeId = FittingHelper.fittingAttributesMap.inverse().get(attributeName);
                        break;
                    }
                }
            }

            if (modifiedAttributeId != 0) {
                break;
            }

            expr = expr.getParentExpression();
        } while (expr != null);

        return modifiedAttributeId;
    }

    public static FittingModel getModifiedModelForItemMod(Expression expr, FittingModel src) {
        int domain;
        FittingModel modifiedModel = null;
        do {
            if (expr.getOperand() == Dogma.Operand.OperatorTarget) {
                Expression domainExpr = expr.getArgument2().getArgument1();
                domain = Dogma.Domain.fromExpressionValue(domainExpr.getExpressionValue());
                modifiedModel = getAttributeModel(domain, src);
                if (modifiedModel != null) {
                    break;
                }
            }
            expr = expr.getParentExpression();
        } while (expr != null);

        return modifiedModel;
    }

    public static void updateAffectedValue(
            Expression expr, FittingModel src, double newValue, boolean removeModifier) {
        // Traverse parent expression until we find the modified item attribute
        Expression parent = expr;
        String attributeName = null;
        long modifiedAttributeId = 0;
        int domain = 0;
        FittingModel modifiedModel = null;
        do {
            if (modifiedAttributeId == 0) {
                Expression[] args = new Expression[]{parent.getArgument1(), parent.getArgument2()};
                for (int i = 0; i < args.length; i++) {
                    Expression arg = args[i];
                    if (arg != null && arg.getOperand() == Dogma.Operand.ItemAttribute) {
                        attributeName = (arg.getArgument2() != null) ?
                                arg.getArgument2().getExpressionName() : null;
                        if (FittingHelper.fittingAttributesMap.containsValue(attributeName)) {
                            modifiedAttributeId =
                                    FittingHelper.fittingAttributesMap.inverse().get(attributeName);
                            break;
                        }
                    }
                }
            }

            if (parent.getOperand() == Dogma.Operand.OperatorTarget) {
                Expression domainExpr = parent.getArgument2().getArgument1();
                domain = Dogma.Domain.fromExpressionValue(domainExpr.getExpressionValue());
                modifiedModel = getAttributeModel(domain, src);
            }
            parent = parent.getParentExpression();
        } while (parent != null);

        if (modifiedModel != null && modifiedAttributeId > 0) {
            if (!modifiedModel.hasAttribute(modifiedAttributeId)) {
                modifiedModel.insertValue(modifiedAttributeId, newValue);
            } else {
                modifiedModel.setAffectedValue(modifiedAttributeId, newValue);
            }
        }
    }

    public static Ship loadShip(long id, SQLiteDatabase db) {
        return (Ship) loadType(id, FittingModel.TYPE_SHIP, db);
    }

    public static Charge loadCharge(long id, SQLiteDatabase db) {
        return (Charge) loadType(id, FittingModel.TYPE_CHARGE, db);
    }

    public static FittingModel loadType(long id, int modelType, SQLiteDatabase db) {
        FittingModel type = null;
        String sql = String.format(SQL_LOAD_TYPE, id);
        Cursor cursor = null;
        try {
            cursor = db.rawQuery(sql, null);
            if (cursor.moveToNext()) {
                switch (modelType) {
                    case FittingModel.TYPE_SHIP:
                        type = new Ship();
                        break;
                    case FittingModel.TYPE_MODULE:
                    case FittingModel.TYPE_RIG:
                        type = new Module();
                        break;
                    case FittingModel.TYPE_CHARGE:
                        type = new Charge();
                        break;
                }
                type.setUniqueId(new Random().nextLong());
                type.setId(cursor.getLong(0));
                type.setName(cursor.getString(1));
                type.setGroupId(cursor.getLong(2));
                type.setGroupName(cursor.getString(3));
                type.setCategoryName(cursor.getString(4));
                type.setEffects(loadEffectsForType(id, db));
                type.setAttributes(loadAttributesForType(id, db));
            }
        } finally {
            Helper.closeCursor(cursor);
        }

        if (type != null && modelType == FittingModel.TYPE_SHIP) {
            type.setAffectingSkillIds(loadAffectingSkillIdsForShip(type, db));
        }

        return type;
    }

    public static Module loadModule(long id, SQLiteDatabase db) {
        Module module = null;
        if (moduleCache.containsKey(id)) {
            Module cachedModule = moduleCache.get(id);

            module = new Module();
            module.setId(cachedModule.getId());
            module.setName(cachedModule.getName());
            module.setGroupId(cachedModule.getGroupId());
            module.setGroupName(cachedModule.getGroupName());
            module.setCategoryName(cachedModule.getCategoryName());
            module.setEffects(new HashMap<Long, Effect>(cachedModule.getEffects()));
            module.setAttributes(new HashMap<Long, FittingAttribute>(cachedModule.getAttributes()));
        } else {
            module = (Module) loadType(id, FittingModel.TYPE_MODULE, db);
            moduleCache.put(id, module);
        }

        // Generate a new unique ID for the module
        module.setUniqueId(new Random().nextLong());
        return module;
    }

    public static Map<Long, Map<Long, Effect>> loadAllEffectsForTypes(long[] typeIds, SQLiteDatabase db) {
        Map<Long, Map<Long, Effect>> allTypeEffects = new HashMap<Long, Map<Long, Effect>>();

        // First load all required expressions
        CarinaSdeHelper.loadExpressionsForTypes(typeIds, db);

        // Then load the effects
        String typeIdList = Helper.getCommaSeparatedValues(typeIds);
        String sql = String.format(SQL_LOAD_EFFECTS_FOR_TYPES_FORMAT,
                typeIdList,
                "132" // exclude only "skillEffect"
                /*CarinaSdeHelper.getEffectCacheIdList(Arrays.asList(new Long[] { 132L })*)*/ // exclude "skillEffect"
        );
        Cursor cursor = null;
        try {
            cursor = db.rawQuery(sql, null);
            while (cursor.moveToNext()) {
                Effect effect = Effect.fromCursor(cursor);
                long typeId = cursor.getLong(5);
                long effectId = effect.getId();
                if (!allTypeEffects.containsKey(typeId)) {
                    allTypeEffects.put(typeId, new HashMap<Long, Effect>());
                }
                allTypeEffects.get(typeId).put(effectId, effect);

                // Also, cache the effect
                if (!CarinaSdeHelper.effectCache.containsKey(effectId)) {
                    CarinaSdeHelper.effectCache.put(effectId, effect);
                }
            }
        } finally {
            Helper.closeCursor(cursor);
        }

        return allTypeEffects;
    }

    public static Map<Long, Effect> loadEffectsForType(long typeId, SQLiteDatabase db) {
        Map<Long, Map<Long, Effect>> allTypeEffects = loadAllEffectsForTypes(new long[] { typeId }, db);
        return allTypeEffects.containsKey(typeId) ? allTypeEffects.get(typeId) : new HashMap<Long, Effect>();
    }

    public static Map<Long, FittingAttribute> loadAttributesForType(long typeId, SQLiteDatabase db) {
        Map<Long, FittingAttribute> attributes = new HashMap<Long, FittingAttribute>();
        String sql = String.format(SQL_LOAD_ATTRIBUTES, typeId);
        Cursor cursor = null;
        try {
            cursor = db.rawQuery(sql, null);
            while (cursor.moveToNext()) {
                FittingAttribute attribute = FittingAttribute.fromCursor(cursor);
                attributes.put(attribute.getId(), attribute);
            }
        } finally {
            Helper.closeCursor(cursor);
        }

        return attributes;
    }

    public static FittingCharacter loadCharacter(
            co.aureolin.labs.carina.model.Character character, SQLiteDatabase db) {
        FittingCharacter fitCharacter = FittingCharacter.fromCharacterModel(character);

        List<Long> skillTypeIdList = new ArrayList<Long>(fitCharacter.getSkills().keySet());
        long[] skillTypeIds = Helper.toPrimitive(skillTypeIdList);
        Map<Long, Map<Long, Effect>> allLoadedEffects = loadAllEffectsForTypes(skillTypeIds, db);

        // Load skill effects and attributes
        for (Map.Entry<Long, FittingSkill> entry : fitCharacter.getSkills().entrySet()) {
            long skillId = entry.getKey();
            int skillLevel = entry.getValue().getLevel();

            Map<Long, FittingAttribute> skillAttributes = skillCache.containsKey(skillId) ?
                    skillCache.get(skillId).getAttributes() : loadAttributesForType(entry.getKey(), db);
            if (skillAttributes.containsKey(skillLevelAttributeId)) {
                skillAttributes.get(skillLevelAttributeId).setValue(skillLevel);
                skillAttributes.get(skillLevelAttributeId).setAffectedValue(skillLevel);
            }
            if (skillAttributes.containsKey(cpuOutputBonus2AttributeId)) {
                FittingAttribute attribute = skillAttributes.get(cpuOutputBonus2AttributeId);
                attribute.setValue(attribute.getValue());
                attribute.setAffectedValue(skillLevel * attribute.getAffectedValue());
            }
            entry.getValue().setAttributes(skillAttributes);

            Map<Long, Effect> skillEffects = allLoadedEffects.containsKey(skillId) ?
                    allLoadedEffects.get(skillId) : null;
            if (skillEffects != null) {
                entry.getValue().setEffects(skillEffects);
            }

            if (!skillCache.containsKey(skillId)) {
                skillCache.put(skillId, entry.getValue());
            }
        }

        return fitCharacter;
    }

    public static void unfitModuleFromShip(Module module, Ship ship) {
        if (ship == null || module == null) {
            throw new CannotUnfitModuleException(module, ship);
        }

        if (ship.removeModule(module)) {
            for (Map.Entry<Long, Effect> entry : module.getEffects().entrySet()) {
                Effect effect = entry.getValue();
                if (module.isSubsystem() /* Process all subsystem post expressions */
                    || "online".equals(effect.getName())) {
                    if (FittingHelper.fittingEffectsMap.containsValue(effect.getName())) {
                        Log.d(TAG, "Applying effect: " + effect.getName());
                        try {
                            Expression expr = effect.getPostExpression();
                            if (expr == null && effect.getPostExpressionId() > 0) {
                                expr = CarinaSdeHelper.getCachedExpression(effect.getPostExpressionId());
                                effect.setPostExpression(expr);
                            }
                            if (expr != null) {
                                evaluate(expr, expr.getArgument1(), expr.getArgument2(), module);
                            }
                        } catch (InvalidOperatorException | ExpressionEvaluationFailedException ex) {
                            Log.e(TAG, ex.getMessage(), ex);
                        }
                    }
                    break;
                }
            }

            module.setCurrentShip(null);
        } else {
            throw new CannotUnfitModuleException(module, ship);
        }
    }

    public static void setChargeForModule(Charge charge, Module module) {
        if (!module.canUseAmmo()) {
            return;
        }

        module.setCurrentCharge(charge);
        for (Map.Entry<Long, Effect> entry : charge.getEffects().entrySet()) {
            Effect effect = entry.getValue();
            Log.d(TAG, "Checking effect: " + effect.getName());
            if (FittingHelper.fittingEffectsMap.containsValue(effect.getName())) {
                // Run the effect's pre-expression when the module is first fitted
                Log.d(TAG, "Applying effect: " + effect.getName());
                try {
                    Expression expr = effect.getPreExpression();
                    if (expr == null && effect.getPreExpressionId() > 0) {
                        expr = CarinaSdeHelper.getCachedExpression(effect.getPreExpressionId());
                        effect.setPreExpression(expr);
                    }
                    if (expr != null) {
                        evaluate(expr, expr.getArgument1(), expr.getArgument2(), charge);
                    }
                } catch (InvalidOperatorException | ExpressionEvaluationFailedException ex) {
                    Log.e(TAG, ex.getMessage(), ex);
                }
            }
        }
    }

    public static void fitModuleToShip(Module module, Ship ship) {
        // Check where this module will fit
        if (module.hasMaxFitting() && ship.moduleHasReachedMaxFitting(module)) {
            throw new ModuleGroupMaxFittingReachedException(module, ship);
        }

        if (module.canFitHighSlot() && ship.getRemainingHighSlots() > 0) {
            if (module.canFitLauncherSlot() && ship.getRemainingLauncherHardpoints() > 0) {
                ship.addLauncherSlotModule(module);
            } else if (module.canFitTurretSlot() && ship.getRemainingTurretHardpoints() > 0) {
                ship.addTurretSlotModule(module);
            } else {
                ship.addHighSlotModule(module);
            }
        } else if (module.canFitMediumSlot() && ship.getRemainingMediumSlots() > 0) {
            ship.addMediumSlotModule(module);
        } else if (module.canFitLowSlot() && ship.getRemainingLowSlots() > 0) {
            ship.addLowSlotModule(module);
        } else if (module.canFitRigSlot() && ship.getRemainingRigSlots() > 0
                && module.getRigSize() == ship.getRigSize()) {
            ship.addRigSlotModule(module);
        } else if (module.canFitSubsystemSlot()) {
            if (!ship.canFitSubsystem(module)) {
                throw new SubSystemSlotOccupiedException(module, ship);
            }
            ship.addSubsystemSlotModule(module);
        } else {
            // TODO: Add more information? Number of remaining slots, etc.
            throw new CannotFitModuleException(module, ship);
        }

        module.setCurrentShip(ship);
        // Apply the "online" and other effects required
        for (Map.Entry<Long, Effect> entry : module.getEffects().entrySet()) {
            Effect effect = entry.getValue();
            Log.d(TAG, "Checking effect: " + effect.getName());
            if (FittingHelper.fittingEffectsMap.containsValue(effect.getName())) {
                // Run the effect's pre-expression when the module is first fitted
                Log.d(TAG, "Applying effect: " + effect.getName());
                try {
                    Expression expr = effect.getPreExpression();
                    if (expr == null && effect.getPreExpressionId() > 0) {
                        expr = CarinaSdeHelper.getCachedExpression(effect.getPreExpressionId());
                        effect.setPreExpression(expr);
                    }
                    if (expr != null) {
                        evaluate(expr, expr.getArgument1(), expr.getArgument2(), module);
                    }
                } catch (InvalidOperatorException | ExpressionEvaluationFailedException ex) {
                    Log.e(TAG, ex.getMessage(), ex);
                }
            }
        }
    }

    public static void applyCharacterSkills(FittingModel model) {
        FittingCharacter character = model.getCurrentCharacter();
        if (character != null) {
            for (FittingSkill skill : character.getSkills().values()) {
                FittingModel currentShip = (model instanceof Ship) ? model : model.getCurrentShip();
                if (!currentShip.isAffectedBySkill(skill.getId())) {
                    continue;
                }

                if (skill.getCurrentShip() == null) {
                    skill.setCurrentShip(currentShip);
                }
                Log.d(TAG, "Applying skill: " + skill.getId() + "; " + skill.getName());

                for (Effect effect : skill.getEffects().values()) {
                    Log.d(TAG, "Skill Effect: " + effect.getId() + ";" + effect.getName() + ";" + effect.getPreExpressionId());
                    try {
                        Expression expr = effect.getPreExpression();
                        if (expr == null && effect.getPreExpressionId() > 0) {
                            expr = CarinaSdeHelper.getCachedExpression(effect.getPreExpressionId());
                            effect.setPreExpression(expr);
                        }
                        if (expr != null) {
                            evaluate(expr, expr.getArgument1(), expr.getArgument2(), skill);
                        }
                    } catch (InvalidOperatorException | ExpressionEvaluationFailedException ex) {
                        Log.e(TAG, ex.getMessage(), ex);
                    }
                }
            }
        }
    }

    public static void simpleTest(Character character, SQLiteDatabase db) {
        FittingCharacter fitChar = loadCharacter(character, db);
        Ship ship = loadShip(603, db); // Merlin
        ship.setCurrentCharacter(fitChar);
        //applyCharacterSkills(ship);

        //Module mse2 = loadModule(3831, db); // Medium Shield Extender II
        Module aif2 = loadModule(2281, db); // Adaptive Invulnerability Field II
        Log.d(TAG, "Module 1 loaded");
        Module aif2_2 = loadModule(2281, db);
        Log.d(TAG, "Module 2 loaded");
        Module aif2_3 = loadModule(2281, db);
        Log.d(TAG, "Module 3 loaded");
        Module aif2_4 = loadModule(2281, db);
        Log.d(TAG, "Module 4 loaded");

        //fitModuleToShip(mse2, ship);
        fitModuleToShip(aif2, ship);
        fitModuleToShip(aif2_2, ship);
        fitModuleToShip(aif2_3, ship);
        fitModuleToShip(aif2_4, ship);

        Log.d(TAG, String.format("CPU: %.2f / %.2f", ship.getCpuLoad(), ship.getCpuOutput()));
        Log.d(TAG, String.format("PG: %.2f / %.2f", ship.getPowerLoad(), ship.getPowergridOutput()));
        Log.d(TAG, String.format("Shield HP: %d", ship.getShieldHitpoints()));

        Log.d(TAG, String.format("Shield EM Resist: %.2f%%, Thermal Resist: %.2f%%, Kinetic Resist: %.2f%%, Explosive Resist: %.2f%%",
                ship.getShieldEMDamageResistance(),
                ship.getShieldThermalDamageResistance(),
                ship.getShieldKineticDamageResistance(),
                ship.getShieldExplosiveDamageResistance()));

        Log.d(TAG, String.format("Velocity: %d, Scan Resolution: %d",
                ship.getMaximumVelocity(), ship.getScanResolution()));
    }

    /*public static List<Long> loadAffectingSkillIdsForType(FittingModel model, SQLiteDatabase db) {

    }*/

    public static List<Long> loadAffectingSkillIdsForShip(FittingModel model, SQLiteDatabase db) {
        String sql = "SELECT SkillEffect.TypeId, Expr.expressionName FROM dgmExpressions Expr "
            + "JOIN EN_SkillEffects SkillEffect ON SkillEffect.preExpression = Expr.expressionID";
        List<Long> skillIds = new ArrayList<Long>();
        Cursor cursor = null;
        try {
            cursor = db.rawQuery(sql, null);
            while (cursor.moveToNext()) {
                String exprName = cursor.getString(1).trim();
                int startIndex = exprName.indexOf(CURRENT_SHIP_EXPR_PREFIX);
                if (startIndex == -1) {
                    continue;
                }

                long skillId = cursor.getLong(0);
                String attributeName = exprName.substring(
                        startIndex + CURRENT_SHIP_EXPR_PREFIX.length(),
                        exprName.indexOf(")", startIndex));
                if (model.hasAttribute(attributeName)) {
                    skillIds.add(skillId);
                }
            }
        } finally {
             Helper.closeCursor(cursor);
        }

        return skillIds;
    }
}
