package co.aureolin.labs.carina.fitting.exceptions;

/**
 * Created by Akinwale on 12/10/2015.
 */
public class InvalidOperatorException extends RuntimeException {
    public InvalidOperatorException() {
        super();
    }

    public InvalidOperatorException(String message) {
        super(message);
    }

    public InvalidOperatorException(String message, Throwable cause) {
        super(message, cause);
    }
}
