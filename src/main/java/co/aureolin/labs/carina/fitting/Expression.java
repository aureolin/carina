/**
 * Project Carina - EVE Fitting Engine Android Library
 *
 * Copyright (c) 2015 Aureolin
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package co.aureolin.labs.carina.fitting;

import android.database.Cursor;

import co.aureolin.labs.carina.model.fitting.FittingModel;

/**
 * Created by Akinwale on 11/10/2015.
 */
public class Expression {

    private long id;

    private long argument1Id;

    private long argument2Id;

    private int operand;

    private Expression argument1;

    private Expression argument2;

    private Expression parentExpression;

    private String expressionName;

    private String expressionValue;

    private long expressionTypeId;

    private long expressionGroupId;

    private long expressionAttributeId;

    public Expression() {

    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getOperand() {
        return operand;
    }

    public void setOperand(int operand) {
        this.operand = operand;
    }

    public long getArgument1Id() {
        return argument1Id;
    }

    public void setArgument1Id(long argument1Id) {
        this.argument1Id = argument1Id;
    }

    public long getArgument2Id() {
        return argument2Id;
    }

    public void setArgument2Id(long argument2Id) {
        this.argument2Id = argument2Id;
    }

    public Expression getParentExpression() {
        return parentExpression;
    }

    public void setParentExpression(Expression parentExpression) {
        this.parentExpression = parentExpression;
    }

    public Expression getArgument1() {
        return argument1;
    }

    public void setArgument1(Expression argument1) {
        this.argument1 = argument1;
    }

    public Expression getArgument2() {
        return argument2;
    }

    public void setArgument2(Expression argument2) {
        this.argument2 = argument2;
    }

    public String getExpressionName() {
        return expressionName;
    }

    public void setExpressionName(String expressionName) {
        this.expressionName = expressionName;
    }

    public String getExpressionValue() {
        return expressionValue;
    }

    public void setExpressionValue(String expressionValue) {
        this.expressionValue = expressionValue;
    }

    public long getExpressionTypeId() {
        return expressionTypeId;
    }

    public void setExpressionTypeId(long expressionTypeId) {
        this.expressionTypeId = expressionTypeId;
    }

    public long getExpressionGroupId() {
        return expressionGroupId;
    }

    public void setExpressionGroupId(long expressionGroupId) {
        this.expressionGroupId = expressionGroupId;
    }

    public long getExpressionAttributeId() {
        return expressionAttributeId;
    }

    public void setExpressionAttributeId(long expressionAttributeId) {
        this.expressionAttributeId = expressionAttributeId;
    }

    @Override
    public String toString() {
        return new StringBuilder()
                .append("{Name:").append(expressionName)
                .append(", Value: ").append(expressionValue)
                .append(", Operand: ").append(operand)
                .append(", Parent: ").append(parentExpression != null ? parentExpression.getId() : "null")
                .append(", Arg1: ").append(argument1 != null ? argument1.getId() : "null")
                .append(", Arg2: ").append(argument2 != null ? argument2.getId() : "null")
                .append("}\n").toString();
    }

    public static class ValueType {
        public static final int INT = 1;

        public static final int DOUBLE = 2;

        public static final int BOOLEAN = 3;
    }

    public static class Result {
        public static final String TRUE = "True";

        public static final String FALSE = "False";

        private FittingModel target; // Maintain a reference to the src item?

        private boolean empty;

        private String value;

        private int valueType;

        private Dogma.Operator operator;

        private boolean spliceResult;

        private Result result1;

        private Result result2;

        public Result() {

        }

        public Result(String value, int valueType) {
            this.value = value;
            this.valueType = valueType;
        }

        public Result(boolean spliceResult, Result result1, Result result2) {
            this.spliceResult = spliceResult;
            this.result1 = result1;
            this.result2 = result2;
        }

        public int getValueType() {
            return valueType;
        }

        public void setValueType(int valueType) {
            this.valueType = valueType;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        public Dogma.Operator getOperator() {
            return operator;
        }

        public void setOperator(Dogma.Operator operator) {
            this.operator = operator;
        }

        public FittingModel getTarget() {
            return target;
        }

        public void setTarget(FittingModel target) {
            this.target = target;
        }

        public int getIntValue() {
            try {
                return Integer.valueOf(value);
            } catch (NumberFormatException e) {
                return 0;
            }
        }

        public double getDoubleValue() {
            try {
                return Double.valueOf(value);
            } catch (NumberFormatException e) {
                return 0;
            }
        }

        public boolean getBooleanValue() {
            return Boolean.valueOf(value);
        }

        public boolean isEmpty() {
            return empty;
        }

        public void setEmpty(boolean empty) {
            this.empty = empty;
        }

        public String toString() {
            return String.format(
                    "{ Value: %s, ValueType: %s, Empty: %s, Splice: %s, Result1: %s, Result2: %s }\n",
                    value,
                    (valueType == ValueType.BOOLEAN) ? "Boolean" : (valueType == ValueType.INT) ? "Integer" : "Double",
                    empty,
                    spliceResult,
                    result1,
                    result2
            );
        }
    }

    public static Expression fromCursor(Cursor cursor) {
        Expression expression = new Expression();
        expression.setId(cursor.getLong(0));
        expression.setOperand(cursor.getInt(1));
        expression.setExpressionValue(cursor.getString(2));
        expression.setExpressionName(cursor.getString(3));
        expression.setExpressionTypeId(cursor.getLong(4));
        expression.setExpressionGroupId(cursor.getLong(5));
        expression.setExpressionAttributeId(cursor.getLong(6));

        return expression;
    }
}
