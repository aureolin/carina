package co.aureolin.labs.carina.fitting.exceptions;

/**
 * Created by Akinwale on 12/10/2015.
 */
public class ExpressionEvaluationFailedException extends RuntimeException {
    public ExpressionEvaluationFailedException() {
        super();
    }

    public ExpressionEvaluationFailedException(String message) {
        super(message);
    }

    public ExpressionEvaluationFailedException(String message, Throwable cause) {
        super(message, cause);
    }
}
