package co.aureolin.labs.carina.fitting.exceptions;

import co.aureolin.labs.carina.model.fitting.Module;
import co.aureolin.labs.carina.model.fitting.Ship;

/**
 * Created by Akinwale on 27/10/2015.
 */
public class CannotFitModuleException extends RuntimeException {
    private int slotType;

    private Module module;

    private Ship ship;

    public CannotFitModuleException() {
        super();
    }

    public CannotFitModuleException(Module module, Ship ship) {
        this.module = module;
        this.ship = ship;
    }

    public CannotFitModuleException(String message) {
        super(message);
    }

    public CannotFitModuleException(String message, Throwable cause) {
        super(message, cause);
    }
}
