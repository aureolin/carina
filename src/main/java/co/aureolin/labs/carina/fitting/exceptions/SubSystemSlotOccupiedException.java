package co.aureolin.labs.carina.fitting.exceptions;

import co.aureolin.labs.carina.model.fitting.Module;
import co.aureolin.labs.carina.model.fitting.Ship;

/**
 * Created by Akinwale on 19/12/2015.
 */
public class SubSystemSlotOccupiedException extends CannotFitModuleException  {
    public SubSystemSlotOccupiedException() {
        super();
    }

    public SubSystemSlotOccupiedException(Module module, Ship ship) {
        super(module, ship);
    }

    public SubSystemSlotOccupiedException(String message) {
        super(message);
    }

    public SubSystemSlotOccupiedException(String message, Throwable cause) {
        super(message, cause);
    }
}
