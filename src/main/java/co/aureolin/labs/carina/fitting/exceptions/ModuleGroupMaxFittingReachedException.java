package co.aureolin.labs.carina.fitting.exceptions;

import co.aureolin.labs.carina.model.fitting.Module;
import co.aureolin.labs.carina.model.fitting.Ship;

/**
 * Created by Akinwale on 18/12/2015.
 */
public class ModuleGroupMaxFittingReachedException extends CannotFitModuleException  {
    public ModuleGroupMaxFittingReachedException() {
        super();
    }

    public ModuleGroupMaxFittingReachedException(Module module, Ship ship) {
        super(module, ship);
    }

    public ModuleGroupMaxFittingReachedException(String message) {
        super(message);
    }

    public ModuleGroupMaxFittingReachedException(String message, Throwable cause) {
        super(message, cause);
    }
}
