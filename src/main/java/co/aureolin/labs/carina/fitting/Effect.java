/**
 * Project Carina - EVE Fitting Engine Android Library
 *
 * Copyright (c) 2015 Aureolin
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package co.aureolin.labs.carina.fitting;

import android.database.Cursor;

/**
 * Created by Akinwale on 11/10/2015.
 */
public class Effect {
    private long id;

    private String name;

    private long preExpressionId;

    private long postExpressionId;

    private Expression preExpression;

    private Expression postExpression;

    private Expression.Result preExpressionResult;

    private Expression.Result postExpressionResult;

    private String modifierInfo;

    public Effect() {

    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getPreExpressionId() {
        return preExpressionId;
    }

    public void setPreExpressionId(long preExpressionId) {
        this.preExpressionId = preExpressionId;
    }

    public long getPostExpressionId() {
        return postExpressionId;
    }

    public void setPostExpressionId(long postExpressionId) {
        this.postExpressionId = postExpressionId;
    }

    public Expression getPreExpression() {
        return preExpression;
    }

    public void setPreExpression(Expression preExpression) {
        this.preExpression = preExpression;
    }

    public Expression getPostExpression() {
        return postExpression;
    }

    public void setPostExpression(Expression postExpression) {
        this.postExpression = postExpression;
    }

    public Expression.Result getPreExpressionResult() {
        return preExpressionResult;
    }

    public void setPreExpressionResult(Expression.Result preExpressionResult) {
        this.preExpressionResult = preExpressionResult;
    }

    public Expression.Result getPostExpressionResult() {
        return postExpressionResult;
    }

    public void setPostExpressionResult(Expression.Result postExpressionResult) {
        this.postExpressionResult = postExpressionResult;
    }

    public String getModifierInfo() {
        return modifierInfo;
    }

    public void setModifierInfo(String modifierInfo) {
        this.modifierInfo = modifierInfo;
    }

    public static Effect fromCursor(Cursor cursor) {
        Effect effect = new Effect();
        effect.setId(cursor.getLong(0));
        effect.setName(cursor.getString(1));
        effect.setModifierInfo(cursor.getString(2));
        effect.setPreExpressionId(cursor.getLong(3));
        effect.setPostExpressionId(cursor.getLong(4));
        return effect;
    }
}
