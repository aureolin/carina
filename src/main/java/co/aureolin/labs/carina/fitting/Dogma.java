/**
 * Project Carina - EVE Fitting Engine Android Library
 *
 * Copyright (c) 2015 Aureolin
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package co.aureolin.labs.carina.fitting;

import android.util.Log;

import java.util.Arrays;
import java.util.List;

import co.aureolin.labs.carina.fitting.exceptions.InvalidOperatorException;
import co.aureolin.labs.carina.model.fitting.FittingModel;
import co.aureolin.labs.carina.model.fitting.Module;
import co.aureolin.labs.carina.model.fitting.Ship;
import co.aureolin.labs.carina.util.FittingHelper;

/**
 * Created by Akinwale on 04/10/2015.
 */
public class Dogma {
    private static final String TAG = "Dogma";

    public static class Domain {
        public static int Self = 1;

        public static int Char = 2;

        public static int Ship = 3;

        public static int Target = 4;

        public static int Area = 5;

        public static int Other = 6;

        public static int fromExpressionValue(String value) {
            if ("Self".equals(value)) {
                return Self;
            }
            if ("Char".equals(value)) {
                return Char;
            }
            if ("Ship".equals(value)) {
                return Ship;
            }
            if ("Target".equals(value)) {
                return Target;
            }
            if ("Area".equals(value)) {
                return Area;
            }
            if ("Other".equals(value)) {
                return Other;
            }

            return -1;
        }
    }

    public static class Operator {
        private static final String PreAssign = "PreAssign";

        private static final String PreMul = "PreMul";

        private static final String PreDiv = "PreDiv";

        private static final String ModAdd = "ModAdd";

        private static final String ModSub = "ModSub";

        private static final String PostMul = "PostMul";

        private static final String PostDiv = "PostDiv";

        private static final String PostPercent = "PostPercent";

        private static final String PreAssignment = "PreAssignment";

        private static final String PostAssignment = "PostAssignment";

        private static final List<String> nonPenalisedOperations = Arrays.asList(new String[] {
            PreAssign, ModAdd, ModSub, PreAssignment, PostAssignment
        });

        private static final List<String> penalisedOperations = Arrays.asList(new String[] {
            PreMul, PreDiv, PostMul, PostDiv, PostPercent
        });

        private String name;

        private FittingModel src;

        private FittingModel model;

        private boolean removing;

        private long affectedAttributeId;

        private boolean canPenaliseModule;

        private int modulePenaltyIndex;

        public Operator(String name) {
            this(name, null, null, 0, false);
        }

        public Operator(String name,
                        FittingModel src,
                        FittingModel model,
                        long affectedAttributeId,
                        boolean removing) {
            this.name = name;
            this.src = src;
            this.model = model;
            this.affectedAttributeId = affectedAttributeId;
            this.removing = removing;
        }

        public String getName() {
            return name;
        }

        public FittingModel getModel() {
            return model;
        }

        public void setModel(FittingModel model) {
            this.model = model;
        }

        public FittingModel getSrc() {
            return src;
        }

        public void setSrc(FittingModel src) {
            this.src = src;
        }

        public long getAffectedAttributeId() {
            return affectedAttributeId;
        }

        public void setAffectedAttributeId(long affectedAttributeId) {
            this.affectedAttributeId = affectedAttributeId;
        }

        public boolean isRemoving() {
            return removing;
        }

        public void setRemoving(boolean removing) {
            this.removing = removing;
        }

        // Penalties galore
        private void preApply() {
            if (model != null
                    && (src instanceof Module)
                    && (model instanceof Ship)
                    && affectedAttributeId > 0
                    && FittingHelper.penalisableAttributeIds.contains(
                        Long.valueOf(affectedAttributeId).intValue())) {
                Ship ship = (Ship) model;
                if (ship != null) {
                    if (removing) {
                        ship.decrementPenalisedAttributeCount(affectedAttributeId);
                    } else {
                        ship.incrementPenalisedAttributeCount(affectedAttributeId);
                    }

                    modulePenaltyIndex = ship.getPenalisedAttributeCount(affectedAttributeId) - 1;
                    if (modulePenaltyIndex < 0) {
                        modulePenaltyIndex = 0;
                    }
                }
            }
        }

        private void postApply(double result) {
            if (model != null && affectedAttributeId > 0) {
                Log.d(TAG, String.format("Setting affected value of Attribute ID: %d to %.2f",
                    affectedAttributeId, result));
                model.setAffectedValue(affectedAttributeId, result);
            }
        }

        // TODO: Where to include stacking penalty calculations?
        public double apply(double arg1, double arg2) {
            if (!penalisedOperations.contains(name) && !nonPenalisedOperations.contains(name)) {
                throw new InvalidOperatorException("Invalid operation");
            }

            Log.d(TAG, String.format("Op=%s, Arg1=%.2f, Arg2=%.2f", name, arg1, arg2));
            preApply();

            if (penalisedOperations.contains(name)) {
                // use the modulePenaltyIndex value to get the penalty constant
                double penaltyConstant =
                    (modulePenaltyIndex < FittingHelper.penalisationConstants.length) ?
                    FittingHelper.penalisationConstants[modulePenaltyIndex] : 0;
                arg2 = arg2 * penaltyConstant;
                Log.d(TAG, String.format(
                        "New Arg2=%.2f, PenaltyConstant=%.8f", arg2, penaltyConstant));
            }

            double result = 0;
            if (PreAssign.equals(name)) {
                result = arg1;
            }

            if (PreMul.equals(name)) {
                result = arg1 * arg2;
            }

            if (PreDiv.equals(name)) {
                result = arg1 / arg2;
            }

            if (ModAdd.equals(name)) {
                result = arg1 + arg2;
            }

            if (ModSub.equals(name)) {
                result = arg1 - arg2;
            }

            if (PostMul.equals(name)) {
                result = arg1 * arg2;
            }

            if (PostDiv.equals(name)) {
                result = arg1 / arg2;
            }

            if (PostPercent.equals(name)) {
                result = arg1 * ((arg2 / 100.0) + 1);
            }

            if (PreAssignment.equals(name)) {
                result = arg1;
            }

            if (PostAssignment.equals(name)) {
                result = arg1;
            }

            postApply(result);
            return result;
        }
    }

    public static class ModuleState {
        public static final int Active = 1;

        public static final int Offline = 2;

        public static final int Online = 3;

        public static final int Overload = 4;
    }

    public static class ModuleSlot {
        public static final int High = 1;

        public static final int Launcher = 2;

        public static final int Low = 3;

        public static final int Medium = 4;

        public static final int Rig = 5;

        public static final int Subsystem = 6;

        public static final int Turret = 7;
    }

    public static class Operand {
        // Add two numbers to return a result
        public static final int Add = 1;

        public static final int AddGangGroupMod = 2;

        public static final int AddGangItemMod = 3;

        public static final int AddGangOwnSkillReqMod = 4;

        public static final int AddGangSkillReqMod = 5;

        public static final int AddItemMod = 6;

        public static final int AddLocGroupMod = 7;

        public static final int AddLocMod = 8;

        public static final int AddLocSkillReqMod = 9;

        // Logical AND operator
        public static final int And = 10;

        public static final int AddOwnSkillReqMod = 11;

        public static final int ItemAttribute = 12;

        public static final int Attack = 13;

        public static final int CargoScan = 14;

        public static final int CheatTeleDock = 15;

        public static final int CheatTeleGate = 16;

        // Executes two statements
        // Format: expression1; expression2
        public static final int Splice = 17;

        // Decrease attribute value by value of another attribute
        public static final int DecrementByAttribute = 18;

        public static final int AoeDecloak = 19;

        // Define operator, text in expressionValue field
        public static final int DefineOperator = 21;

        // Define operator, text in expressionValue field
        public static final int DefineAttribute = 22;

        // Define boolean constant, boolean in expressionValue field
        public static final int DefineBoolean = 23;

        // Define a domain (Char, Self, Ship)
        public static final int DefineLocation = 24;

        public static final int DefineGroup = 26;

        public static final int DefineInteger = 27;

        public static final int DefineType = 29;

        public static final int EcmBurst = 30;

        public static final int OperatorTarget = 31;

        public static final int AreaOfEffectDamage = 32;

        public static final int Eq = 33;

        public static final int GroupAttribute = 34;

        public static final int ItemAttributeCondition = 35;

        public static final int GetArg1Type = 36;

        public static final int Gt = 38;

        public static final int Gte = 39;

        public static final int GenericAttribute = 40;

        public static final int IfThen = 41;

        public static final int IncrementByAttributeValue = 42;

        public static final int MissileLaunch = 44;

        public static final int DefenderLaunch = 45;

        public static final int FofLaunch = 47;

        public static final int LocationGroup = 48; // domain.group

        public static final int LocationSkillReq = 49; // domain[skill req]

        public static final int Mine = 50;

        public static final int Or = 52; // Logical OR

        public static final int PowerBooster = 53;

        public static final int RmGangGroupMod = 54;

        public static final int RmGangItemMod = 55;

        public static final int RmGangOwnSkillReqMod = 56;

        public static final int RmGangSkillReqMod = 57;

        public static final int RemoveItemMod = 58;

        public static final int RmLocGroupMod = 59;

        public static final int RmLocMod = 60;

        public static final int RmLocSkillReqMod = 61;

        public static final int RmOwnSkillReqMod = 62;

        public static final int SkillReqAttr = 64;

        public static final int Assign = 65;

        public static final int ShipScan = 66;

        public static final int Subtract = 68;

        public static final int SurveyScan = 69;

        public static final int TargetHostile = 70;

        public static final int TargetSilent = 71;

        public static final int ToolTargetSkills = 72;

        public static final int UserError = 73;

        public static final int VerifyTargetGroup = 74;
    }
}
