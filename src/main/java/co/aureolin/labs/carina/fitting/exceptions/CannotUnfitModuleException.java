package co.aureolin.labs.carina.fitting.exceptions;

import co.aureolin.labs.carina.model.fitting.Module;
import co.aureolin.labs.carina.model.fitting.Ship;

/**
 * Created by Akinwale on 18/12/2015.
 */
public class CannotUnfitModuleException extends RuntimeException {
    private int slotType;

    private Module module;

    private Ship ship;

    public CannotUnfitModuleException() {
        super();
    }

    public CannotUnfitModuleException(Module module, Ship ship) {
        this.module = module;
        this.ship = ship;
    }

    public CannotUnfitModuleException(String message) {
        super(message);
    }

    public CannotUnfitModuleException(String message, Throwable cause) {
        super(message, cause);
    }
}

